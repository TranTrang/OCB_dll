﻿using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.register
{
    public partial class CheckUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string phone = Request["phoneuser"];
            string email = Request["email"];
            UserManager UM = new UserManager();
            if (UM.GetByPhone(phone) != null)
            {
                Response.Write(2);
                return;

            }
            if (UM.GetByEmail(email) != null)
            {
                Response.Write(1);
                return;
            }


            Response.Write(0);
        }
    }
}