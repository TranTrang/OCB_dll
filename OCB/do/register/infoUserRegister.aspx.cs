﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.register
{
    public partial class infoUserRegister : System.Web.UI.Page
    {

        public UserTBx user = new UserTBx();
        public List<CardSurveyValueTBx> listCardSurveyValue = new List<CardSurveyValueTBx>();
        public SurveyItemValueManager SIM = new SurveyItemValueManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            //load thong tin user da dk

            int userID = Convert.ToInt32(Request["userid"]);
            if (userID > 0)
            {
                UserManager UM = new UserManager();
                user = UM.GetByID(userID);

                CardSurveyValueManager CSVM = new CardSurveyValueManager();
                listCardSurveyValue = CSVM.GetListByUserID(userID).OrderBy(n => n.SurveyTBx.step).ThenBy(n => n.SurveyID).ToList();

                // customize answers
                CardSurveyValueTBx[] arrBirth = new CardSurveyValueTBx[3];
                CardSurveyValueTBx[] arrProfileDate = new CardSurveyValueTBx[3];
                for (int i = 0; i < listCardSurveyValue.Count; i++)
                {
                    // birthday
                    if (listCardSurveyValue[i].SurveyID == 17) { arrBirth[0] = listCardSurveyValue[i]; }
                    if (listCardSurveyValue[i].SurveyID == 49) { arrBirth[1] = listCardSurveyValue[i]; listCardSurveyValue.RemoveAt(i); i--; }
                    if (listCardSurveyValue[i].SurveyID == 16) { arrBirth[2] = listCardSurveyValue[i]; listCardSurveyValue.RemoveAt(i); i--; }

                    // profile day
                    if (listCardSurveyValue[i].SurveyID == 51) { arrProfileDate[0] = listCardSurveyValue[i]; }
                    if (listCardSurveyValue[i].SurveyID == 54) { arrProfileDate[1] = listCardSurveyValue[i]; listCardSurveyValue.RemoveAt(i); i--; }
                    if (listCardSurveyValue[i].SurveyID == 50) { arrProfileDate[2] = listCardSurveyValue[i]; listCardSurveyValue.RemoveAt(i); i--; }
                }

                //birth day
                if (arrBirth[0] != null && arrBirth[1] != null && arrBirth[2] != null)
                {
                    if (arrBirth[0].TextValue.Length == 1) arrBirth[0].TextValue = "0" + arrBirth[0].TextValue;
                    if (arrBirth[1].TextValue.Length == 1) arrBirth[1].TextValue = "0" + arrBirth[1].TextValue;
                    CardSurveyValueTBx valueBirthDay = listCardSurveyValue.Where(m => m.SurveyID == 17).First();
                    valueBirthDay.TextValue = arrBirth[0].TextValue + "/" + arrBirth[1].TextValue + "/" + arrBirth[2].TextValue;
                    valueBirthDay.SurveyTBx.Code = "Ngày Tháng Năm Sinh";
                }

                // profile day
                if (arrProfileDate[0] != null && arrProfileDate[1] != null && arrProfileDate[2] != null)
                {
                    if (arrProfileDate[0].TextValue.Length == 1) arrProfileDate[0].TextValue = "0" + arrProfileDate[0].TextValue;
                    if (arrProfileDate[1].TextValue.Length == 1) arrProfileDate[1].TextValue = "0" + arrProfileDate[1].TextValue;
                    CardSurveyValueTBx valueProfileDay = listCardSurveyValue.Where(m => m.SurveyID == 51).First();
                    valueProfileDay.TextValue = arrProfileDate[0].TextValue + "/" + arrProfileDate[1].TextValue + "/" + arrProfileDate[2].TextValue;
                    valueProfileDay.SurveyTBx.Code = "Ngày cấp giấy tờ";
                }

                foreach (var item in listCardSurveyValue)
                {
                    int n;
                    bool isNumeric = int.TryParse(item.TextValue, out n);
                    if (isNumeric)
                    {
                        SurveyItemValueTBx survey = SIM.GetBySurveyIDAndValueID((int)item.SurveyID, n);
                        if (survey != null)
                        {
                            item.TextValue = survey.Value;
                        }
                    }
                }
            }
        }
    }
}