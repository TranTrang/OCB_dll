﻿using Newtonsoft.Json;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.register
{
    public partial class CheckPhone : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string phone = Request["phone"];
                UserManager UM = new UserManager();
                if (UM.GetByPhone(phone) != null)
                {

                    Response.Write(JsonConvert.SerializeObject(new
                    {
                        success = 1
                    }));
                }
                else
                {
                    Response.Write(JsonConvert.SerializeObject(new
                    {
                        success = 0
                    }));
                }

            }
            catch (Exception ex)
            {
                Response.Write(JsonConvert.SerializeObject(new
                {
                    success = -1,
                    error = ex
                }));
            }
        }
    }
}