﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="infoUserRegister.aspx.cs" Inherits="OCB.register.infoUserRegister" %>


<div class="row page-title-container">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <h4 class="page-title">USER SURVEY (<%=user.Email %>)</h4>
    </div>
</div>
<div class="row" id="table">
    <%if (listCardSurveyValue.Count == 0)
        { %>
    <div class="col-md-12 col-xs-12 col-lg-12">
        <p>No result found.</p>
    </div>
    <%}
        else
        { %>
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="pagination-table" id="paging-table">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>

                        <th>Câu hỏi khảo sát của ngân hàng</th>
                        <th>Câu trả lời của khách hàng</th>
                      <%--  <th class="hidden-xs">Thời gian</th>--%>
                    </tr>
                </thead>
                <tbody class="pagination-body">
                    <%for (int i = 0; i < listCardSurveyValue.Count; i++)
                        {%>
                    <tr>

                        <%if (listCardSurveyValue[i].SurveyID != 0 && listCardSurveyValue[i].SurveyID != null)
                            {%>
                        <td><%=listCardSurveyValue[i].SurveyTBx.Code %></td>
                        <%}
                            else
                            {%>
                        <td>Not value</td>
                        <%} %>
                        <td><%=listCardSurveyValue[i].TextValue %></td>
                        <%--<td class="hidden-xs"><%=listCardSurveyValue[i].CreatedDate %></td>--%>
                    </tr>
                    <%} %>
                </tbody>
            </table>
            <div id="pagination"></div>
        </div>
    </div>
    <%} %>
</div>
