﻿using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace OCB.register
{
    public partial class CheckEmail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string email = Request["email"];
                UserManager UM = new UserManager();
                if (UM.GetByEmail(email) != null)
                {

                    Response.Write(JsonConvert.SerializeObject(new
                    {
                        success = 1
                    }));
                }
                else
                {
                    Response.Write(JsonConvert.SerializeObject(new
                    {
                        success = 0
                    }));
                }

            }
            catch (Exception ex)
            {
                Response.Write(JsonConvert.SerializeObject(new
                {
                    success = -1,
                    error = ex
                }));
            }
        }
    }
}