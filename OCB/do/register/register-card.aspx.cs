﻿using Newtonsoft.Json;
using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.register
{
    public partial class register_card : System.Web.UI.Page
    {
        public List<UserTBx> listUser = new List<UserTBx>();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                UserManager UM = new UserManager();
                UserTBx user = new UserTBx();
                CardSurveyValueManager CSVM = new CardSurveyValueManager();
                ProvinceManager PM = new ProvinceManager();
                ProvinceTBx provincetb = new ProvinceTBx();
                TownTBx towntb = new TownTBx();
                TownManager TM = new TownManager();
                string item = Request["item"];
                string[] listinfo = item.Split('*');
                int step = Convert.ToInt32(Request["step"]);
                for (int i = 0; i < listinfo.Length; i++)
                {
                    if (listinfo[i] != "")
                    {
                        List<UserJSON> DTC = JsonConvert.DeserializeObject<List<UserJSON>>(listinfo[i]);
                        foreach (var userJson in DTC)
                        {
                            switch (Convert.ToInt32(userJson.Key))
                            {
                                case 5://Phone
                                    if (UM.GetByPhone(userJson.Value) != null)
                                    {
                                        Response.Write(2);
                                        return;

                                    }
                                    user.Phone = userJson.Value;
                                    break;
                                case 6:// Email
                                    if (UM.GetByEmail(userJson.Value) != null)
                                    {
                                        Response.Write(3);
                                        return;

                                    }

                                    user.Email = userJson.Value;
                                    break;
                                case 43://fullname
                                    user.FullName = userJson.Value;
                                    break;
                                case 27://address
                                    user.Address = userJson.Value;
                                    break;
                            }
                        }
                    }
                }
                user.Status = 1;
                user.RegisterStatus = 0;
                user.IsDeleted = false;
                user.CreatedDate = DateTime.Now;
                UM.Add(user);
                Response.Write(user.ID);
                if (user.ID == 0)
                {
                    Response.Write("Không thể save User");
                    return;
                }

                for (int i = 0; i < listinfo.Length; i++)
                {
                    if (listinfo[i] == "")
                    {

                    }
                    else
                    {
                        List<UserJSON> DTC2 = JsonConvert.DeserializeObject<List<UserJSON>>(listinfo[i]);
                        foreach (var userJson in DTC2)
                        {
                            CardSurveyValueTBx cardsurveyValue = new CardSurveyValueTBx();
                            int key = Convert.ToInt32(userJson.Key);
                            if (userJson.Value != "" && userJson.Value != null)
                            {
                                if (key == 7 || key == 52 || key == 63 || key == 105 || key == 84 || key == 104)
                                {
                                    provincetb = PM.GetByID(Convert.ToInt32(userJson.Value));
                                    cardsurveyValue.TextValue = provincetb.Name;
                                }
                                else if (key == 8 || key == 97 || key == 106 || key == 101)
                                {
                                    towntb = TM.GetByID(Convert.ToInt32(userJson.Value));
                                    cardsurveyValue.TextValue = towntb.Name;
                                }
                                else
                                {
                                    cardsurveyValue.TextValue = userJson.Value;
                                }
                                cardsurveyValue.UserID = user.ID;
                                cardsurveyValue.CardSurveyID = 3;
                                cardsurveyValue.CreatedDate = DateTime.UtcNow;
                                cardsurveyValue.IsDeleted = false;
                                cardsurveyValue.SurveyID = Convert.ToInt32(userJson.Key);
                                CSVM.Add(cardsurveyValue);
                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }

        }

    }
}