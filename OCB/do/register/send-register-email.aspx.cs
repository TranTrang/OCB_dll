﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.register
{
    public partial class send_register_email : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string username = "";
                string phone = "";
                string email = "";
                string item = Request["item"];
                string[] listinfo = item.Split('*');
                int step = Convert.ToInt32(Request["step"]);
                for (int i = 0; i < 4; i++)
                {
                    List<UserJSON> DTC = JsonConvert.DeserializeObject<List<UserJSON>>(listinfo[i]);
                    foreach (var userJson in DTC)
                    {
                        switch (Convert.ToInt32(userJson.Key))
                        {
                            case 5:
                                phone = userJson.Value;
                                break;
                            case 6:
                                email = userJson.Value;
                                break;
                            case 43:
                                username = userJson.Value;
                                break;

                        }

                    }
                }
                string mailSubject = "Thông báo đăng ký mở thẻ - OCB";
                string mailBody = "<table>";
                mailBody += "<tr><td>Họ và tên: </td><td>" + username + "</td></tr>";
                mailBody += "<tr><td>Email: </td><td>" + email + "</td></tr>";
                mailBody += "<tr><td>Số điện thoại: </td><td>" + phone + "</td></tr>";
                mailBody += "</table>";
                string mailBodyUser = "<table>";
                mailBodyUser += "<tr><td>Họ và tên: </td><td>" + username + "</td></tr>";
                mailBodyUser += "<tr><td>Email: </td><td>" + email + "</td></tr>";
                mailBodyUser += "<tr><td>Số điện thoại: </td><td>" + phone + "</td></tr>";

                mailBodyUser += "</table>";

                // Send email to booktour
                List<string> listClientEmail = new List<string>();
                listClientEmail.Add("trantrangit.cntp@gmail.com");

                UTIL_SENDMAIL.SendMail("", listClientEmail, true, mailSubject, mailBody, "noreply@ocb.com.vn", "Đăng ký mở thẻ từ OCB");

                // Send email to user
                mailSubject = "Chào mừng bạn đến OCB ";
                mailBodyUser += "Cảm ơn bạn đã đăng ký, chúng tôi sẽ gửi thông báo đến bạn trong vòng 24h.";

                UTIL_SENDMAIL.SendMail(email, null, false, mailSubject, mailBodyUser, "noreply@ocb.com.vn", "Đăng ký mở thẻ từ OCB ");

                Response.Write(1);
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }
    }
}