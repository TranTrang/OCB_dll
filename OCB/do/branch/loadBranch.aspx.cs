﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.branch
{
    public partial class loadBranch : System.Web.UI.Page
    {
        public List<BranchTBx> listBranch = new List<BranchTBx>();
        string option = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            int idProvince = Convert.ToInt32(Request["provinceid"]);
            BranchManager BM = new BranchManager();
            listBranch = BM.GetListByProvinceID(idProvince);
            option += "<ul><li data-id='' onclick='Select(this)'>Chọn danh sách chi nhánh</li>";
            for (int i = 0; i < listBranch.Count; i++)
            {
                option += "<li data-id='" + listBranch[i].ID + "' onclick='Select(this)'>" + listBranch[i].NameVN + "</li>";
            }
            option += "</ul>";
            Response.Write(option);
        }
    }
}