﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.town
{
    public partial class loadTown : System.Web.UI.Page
    {
        public List<TownTBx> listTown = new List<TownTBx>();
        string chuoi = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            int idProvince = Convert.ToInt32(Request["provinceid"]);
            TownManager TM = new TownManager();
            listTown = TM.GetByProvinceID(idProvince);
            chuoi += "<ul><li data-id='' onclick='Select(this)'>Quận/Huyện</li>";
            for (int i = 0; i < listTown.Count; i++)
            {
                chuoi += "<li data-id='" + listTown[i].ID + "' onclick='Select(this)'>" + listTown[i].Name + "</li>";
            }
            chuoi += "</ul>";
            Response.Write(chuoi);
        }
    }
}