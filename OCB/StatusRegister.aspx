﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="StatusRegister.aspx.cs" Inherits="OCB.StatusRegister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <header class="header-page bg-green-dark">
        <div class="container-fluid">
            <div class="row  box-logo-pass-menu">
                <div class="col-xs-12 col-sm-5 col-md-5">
                    <div class="row m-row-pass">
                        <div class="col-xs-2 hidden-sm hidden-md hidden-lg">
                            <a href="#">
                                <div class="icon-bar-mobile">
                                    <i class="fas fa-bars"></i>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-7 col-sm-5 col-md-5 col-lg-5">
                            <a href="#">
                                <div class="img-logo">
                                    <img src="/images/OCB/logo.png" class="img-responsive" />
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7">
                            <a href="#">
                                <div class="img-pass">
                                    <img src="/images/OCB/pass.png" class="img-responsive" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 list-menu">
                    <nav>
                        <ul class="list-menu-parent">
                            <li><a href="#">OCB OMMI</a></li>
                            <li><a href="#">Thẻ tín dụng</a></li>
                            <li><a href="#">Vay</a></li>
                            <li><a href="#">Ưu đãi</a></li>
                            <li><a href="#">Mr.Tài chính</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="/my-account">My account</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="box-thank text-center">
                    <h3 class="color-green-dark">Trạng thái hồ sơ</h3>
                    <div class="form hidden-sm hidden-md hidden-lg text-left step-status">
                        <h4><strong>Mở thẻ tín dụng</strong></h4>
                        <p>Hoàn tất thông tin  <i class="fas fa-check-circle check"></i></p>
                        <p>Hoàn tất hồ sơ  <i class="fas fa-check-circle check"></i></p>
                        <p>
                            Thẩm định
                            <br />
                            <a href="#" class="color-orange"><small>Đang thực hiện</small></a>
                        </p>
                        <p>Đã gửi thẻ đi</p>
                        <p>Đã nhận thẻ</p>
                    </div>
                    <div class="form hidden-xs">
                        <h4>Mở thẻ tín dụng</h4>
                        <div class="box-status">
                            <div class="line"></div>
                            <div class="content-status">
                                <div class="item-status">
                                    <i class="fas fa-check-circle check"></i>
                                    <h5>Hoàn tất thông tin</h5>
                                </div>
                                <div class="item-status">
                                    <i class="fas fa-check-circle check"></i>
                                    <h5>Hoàn tất hồ sơ</h5>
                                </div>
                                <div class="item-status circle-lg">
                                    <i class="far fa-dot-circle"></i>
                                    <h5>Thẩm định</h5>
                                </div>
                                <div class="item-status">
                                    <i class="far fa-dot-circle circle-small"></i>
                                    <h5>Đã gửi thẻ đi</h5>
                                </div>
                                <div class="item-status">
                                    <i class="far fa-dot-circle circle-small"></i>
                                    <h5>Đã nhận thẻ</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
