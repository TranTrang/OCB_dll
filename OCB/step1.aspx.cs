﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB
{
    public partial class step1 : System.Web.UI.Page
    {
        public List<ProvinceTBx> listProvince = new List<ProvinceTBx>();
        public List<TownTBx> listTown = new List<TownTBx>();
        public SurveyManager SM = new SurveyManager();
        public SurveyItemValueManager SIV = new SurveyItemValueManager();
        public List<SurveyTBx> listSurvey = new List<SurveyTBx>();
        public List<SurveyTBx> listSurveyStep = new List<SurveyTBx>();
        public List<SurveyItemValueTBx> listOption = new List<SurveyItemValueTBx>();
        public int step = 1;
        protected void Page_Load(object sender, EventArgs e)
        {
            ProvinceManager PM = new ProvinceManager();
            listProvince = PM.GetList();
            listSurveyStep = SM.GetListStep(step);
        }
    }
}