﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="AddPage.aspx.cs" Inherits="cp_page_page_AddPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page" class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-page" class="breadcrumbs__element">Page</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Add new Page</span></li>
                </ul>
            </div>
        </div>
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">ADD NEW PAGE</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Name:* </label>
                <input id="txtName" type="text" class="required" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Website: </label>
                <input id="txtWebsite" type="text" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Position: </label>
                <input id="txtPosition" type="number" value="0"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Metadata: </label>
                <textarea id="txtMetadata" class="tinymce" ></textarea>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <button class="btn btn-primary" onclick="Submit()">Submit</button>
                <button class="btn btn-danger" onclick="Cancel()">Cancel</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(12);
        });

        function Cancel() {
            location.href = "/cp-page";
        }

        function Submit() {
            var name = $('#txtName').val();
            var website = $('#txtWebsite').val();
            var position = $('#txtPosition').val();
            var metadata = tinymce.get('txtMetadata').getContent();

            // check exception
            CheckFieldEmpty('page');
            var error = "";
            error += CheckFieldError();

            if (error != "") {
                alertify.alert('Error', error);
            } else {
                ShowLoading();
                $.post(LOCAL_DO_URL + 'page/add-page.aspx', {
                    name: name,
                    website: website,
                    position: position,
                    metadata: metadata,

                }, function (data) {
                    HideLoading();

                    if (data != 1) {
                        alertify.alert('Error', data);
                    } else {
                        alertify.success('Successful!');
                        location.href = "/cp-page";
                    }
                });
            }
        }
    </script>
</asp:Content>
