﻿using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_page_page_EmailCustomizer : BaseAdminPage
{
    public EmailData emailData = new EmailData();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        PageManager PM = new PageManager();
        emailData = Newtonsoft.Json.JsonConvert.DeserializeObject<EmailData>(PM.GetByID(1).Metadata);
    }
}