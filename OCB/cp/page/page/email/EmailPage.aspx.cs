﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_page_email_EmailPage : BaseAdminPage
{
    public List<PageTBx> listEmailPage = new List<PageTBx>();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        PageManager PM = new PageManager();
        listEmailPage = PM.GetListEmailPage();
    }
}