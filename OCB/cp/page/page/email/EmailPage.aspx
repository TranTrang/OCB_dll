﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="EmailPage.aspx.cs" Inherits="cp_page_page_email_EmailPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">Customize Email</h4>
            </div>
        </div>
        <div class="row page-control-container">
            <div class="col-md-6 col-lg-6 col-xs-6">
                <%--<a href="/cp-add-card" class="btn btn-info">Add new Card</a>--%>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-6">
                <div class="search-box-container">
                    <%--<input type="text" placeholder="Search..." id="txtSearchInput" />
                    <span><i class="fas fa-search"></i></span>--%>
                </div>
            </div>
        </div>
        <div class="row" id="table">
            <%if (listEmailPage.Count == 0) { %>
                <div class="col-md-12 col-xs-12 col-lg-12"><p>No result found.</p></div>
            <%} else { %>
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <div class="pagination-table" id="paging-table">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="pagination-body">
                                <%foreach (var emailPage in listEmailPage) {%>
                                    <tr>
                                        <td><%=emailPage.ID %></td>
                                        <td><%=emailPage.Name %></td>
                                        <td>
                                            <a href="/cp-email-customize/pageID-<%=emailPage.ID %>" class="btn btn-primary">Edit</a>
                                            <%--<button class="btn btn-danger" onclick="Delete(<%=card.ID %>)">Delete</button>--%>
                                        </td>
                                    </tr>
                                <%} %>
                            </tbody>
                        </table>
                        <div id="pagination"></div>
                    </div>
                </div>
            <%} %>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(10);

            Pagination(20, 'paging-table');
        });

        //function Delete(id) {
        //    alertify.confirm('Are you sure you want to DELETE this?', function () {
        //        ShowLoading();

        //        $.post(LOCAL_DO_URL + 'card/delete-card.aspx', {
        //            cardID: id,

        //        }, function (data) {
        //            HideLoading();

        //            if (data == 1) {
        //                alertify.success('Deleted!');

        //                // refresh page
        //                location.reload();
        //            } else {
        //                alertify.alert(data);
        //            }
        //        });
        //    });
        //}
    </script>
</asp:Content>



