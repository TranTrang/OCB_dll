﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeBehind="EmailCustomizer.aspx.cs" Inherits="OCB.cp.page.page.email.EmailCustomizer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="page" class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <%--<li class="breadcrumbs__item"><a href="/cp-card" class="breadcrumbs__element">Card</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Edit Card</span></li>--%>
                </ul>
            </div>
        </div>
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">Customize email - <%=page.Name.Split('_')[1] %></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Send From Email:* </label>
                <input id="txtSendFromEmail" type="text" class="required" value="<%=emailData.sendFromEmail %>" />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Title:* </label>
                <input id="txtTitle" type="text" class="required" value="<%=emailData.title %>" />
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Subject:* </label>
                <input id="txtSubject" type="text" class="required" value="<%=emailData.subject %>" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>HTML Content:* </label>
                <textarea id="txtBody" class="tinymce required" placeholder="Description"><%=emailData.body %></textarea>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <button class="btn btn-primary" onclick="Submit()">Submit</button>
                <button class="btn btn-danger" onclick="Cancel()">Cancel</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(10);
        });

        function Cancel() {
            location.href = "/cp-email-page";
        }

        function Submit() {
            var sendFromEmail = $('#txtSendFromEmail').val();
            var title = $('#txtTitle').val();
            var subject = $('#txtSubject').val();
            var body = tinymce.get('txtBody').getContent();

            // check exception
            CheckFieldEmpty('page');
            var error = "";
            error += CheckFieldError();

            if (error != "") {
                alertify.alert('Error', error);
            } else {
                ShowLoading();
                $.post(LOCAL_DO_URL + 'page/customize-email.aspx', {
                    pageID: <%=page.ID %>,
                    sendFromEmail: sendFromEmail,
                    title: title,
                    subject: subject,
                    body: body,

                }, function (data) {
                    HideLoading();

                    if (data != 1) {
                        alertify.alert('Error', data);
                    } else {
                        alertify.success('Successful!');
                        location.href = "/cp-email-page";
                    }
                });
            }
        }
    </script>
</asp:Content>
