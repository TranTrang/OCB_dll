﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.cp.page.page.email
{
    public partial class EmailCustomizer : BaseAdminPage
    {

        public PageTBx page = new PageTBx();
        public EmailData emailData = new EmailData();

        protected void Page_Load(object sender, EventArgs e)
        {
            InitPage();
        }

        protected override void ActionAdminPage()
        {
            int pageID = Convert.ToInt32(Page.RouteData.Values["pageID"]);

            PageManager PM = new PageManager();
            page = PM.GetByID(pageID);

            emailData = Newtonsoft.Json.JsonConvert.DeserializeObject<EmailData>(page.Metadata);
        }
    }
}