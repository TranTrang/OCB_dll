﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_page_EditPage : BaseAdminPage
{
    public PageTBx page = new PageTBx();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        int pageID = Convert.ToInt32(Page.RouteData.Values["pageID"]);

        PageManager PM = new PageManager();
        page = PM.GetByID(pageID);
    }
}