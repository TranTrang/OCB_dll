﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_page_page_Page : BaseAdminPage
{
    public List<PageTBx> listEditPage = new List<PageTBx>();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        PageManager PM = new PageManager();
        listEditPage = PM.GetListEditPage();
        listEditPage.Reverse();
    }
}