﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="Page.aspx.cs" Inherits="cp_page_page_page_Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">PAGE</h4>
            </div>
        </div>
        <div class="row page-control-container">
            <div class="col-md-6 col-lg-6 col-xs-6">
                <a href="/cp-add-page" class="btn btn-info">Add new Page</a>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-6">
                <div class="search-box-container">
                    <%--<input type="text" placeholder="Search..." id="txtSearchInput" />
                    <span><i class="fas fa-search"></i></span>--%>
                </div>
            </div>
        </div>
        <div class="row" id="table">
            <%if (listEditPage.Count == 0) { %>
                <div class="col-md-12 col-xs-12 col-lg-12"><p>No result found.</p></div>
            <%} else { %>
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <div class="pagination-table" id="paging-table">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Website</th>
                                    <th>Position</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="pagination-body">
                                <%foreach (var page in listEditPage) {%>
                                    <tr>
                                        <td><%=page.ID %></td>
                                        <td><%=page.Name %></td>
                                        <td><%=page.Website %></td>
                                        <td><%=page.Postion %></td>
                                        <td>
                                            <a href="/cp-edit-page/pageID-<%=page.ID %>" class="btn btn-primary">Edit</a>
                                            <button class="btn btn-danger" onclick="Delete(<%=page.ID %>)">Delete</button>
                                        </td>
                                    </tr>
                                <%} %>
                            </tbody>
                        </table>
                        <div id="pagination"></div>
                    </div>
                </div>
            <%} %>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(12);

            Pagination(50, 'paging-table');
        });

        function Delete(id) {
            alertify.confirm('Are you sure you want to DELETE this?', function () {
                ShowLoading();

                $.post(LOCAL_DO_URL + 'page/delete-page.aspx', {
                    pageID: id,

                }, function (data) {
                    HideLoading();

                    if (data == 1) {
                        alertify.success('Deleted!');

                        // refresh page
                        location.reload();
                    } else {
                        alertify.alert(data);
                    }
                });
            });
        }
    </script>
</asp:Content>

