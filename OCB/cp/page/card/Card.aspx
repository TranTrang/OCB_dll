﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="Card.aspx.cs" Inherits="cp_page_card_Card" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">CARD</h4>
            </div>
        </div>
        <div class="row page-control-container">
            <div class="col-md-6 col-lg-6 col-xs-6">
                <a href="/cp-add-card" class="btn btn-info">Add new Card</a>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-6">
                <div class="search-box-container">
                    <%--<input type="text" placeholder="Search..." id="txtSearchInput" />
                    <span><i class="fas fa-search"></i></span>--%>
                </div>
            </div>
        </div>
        <div class="row" id="table">
            <%if (listCard.Count == 0) { %>
                <div class="col-md-12 col-xs-12 col-lg-12"><p>No result found.</p></div>
            <%} else { %>
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <div class="pagination-table" id="paging-table">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Credit Limit</th>
                                    <th>Created Date</th>
                                    <th>Publish</th>
                                    <%--<th>Detail</th>--%>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="pagination-body">
                                <%foreach (var card in listCard) {%>
                                    <tr>
                                        <td><%=card.ID %></td>
                                        <td><%=card.Name %></td>
                                        <td><%=card.Type %></td>
                                        <td><%=card.CreditLimit %></td>
                                        <td><%=card.CreatedDate %></td>
                                        <td><input id="cbPublish" type="checkbox" onclick="TooglePublish(<%=card.ID %>, this)" <%if (Convert.ToBoolean(card.IsPublish)) {%>checked="checked" <%}%>></td>
                                        <%--<td>
                                            <a href="/cp-edit-card-survey/cardID-<%=card.ID %>" class="btn btn-primary">Card Survey</a>
                                        </td>--%>
                                        <td>
                                            <a href="/cp-edit-card/id-<%=card.ID %>" class="btn btn-primary">Edit</a>
                                            <button class="btn btn-danger" onclick="Delete(<%=card.ID %>)">Delete</button>
                                        </td>
                                    </tr>
                                <%} %>
                            </tbody>
                        </table>
                        <div id="pagination"></div>
                    </div>
                </div>
            <%} %>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(3);

            Pagination(20, 'paging-table');
        });

        function TooglePublish(id, cbID) {
            var isPublish = $(cbID).prop('checked')

            var strWarning = "";
            if (isPublish) {
                strWarning = "Are you sure you want to PUBLISH this card?";
            }
            else {
                strWarning = "Are you sure you want to UNPUBLISH this card?";
            }

            alertify.confirm(strWarning, function () {
                ShowLoading();
                $.post(LOCAL_DO_URL + 'card/toogle-publish-card.aspx', {
                    cardID: id,
                    isPublish: isPublish,

                }, function (data) {
                    HideLoading();

                    if (data == 1) {
                        alertify.success('Successful!');
                    } else {
                        alertify.alert(data);

                        if (isPublish) {
                            $(cbID).prop('checked', false);
                        } else {
                            $(cbID).prop('checked', true);
                        }
                    }
                });
            }, function () { // player select cancel
                if (isPublish) {
                    $(cbID).prop('checked', false);
                } else {
                    $(cbID).prop('checked', true);
                }
            });
        }

        function Delete(id) {
            alertify.confirm('Are you sure you want to DELETE this?', function () {
                ShowLoading();

                $.post(LOCAL_DO_URL + 'card/delete-card.aspx', {
                    cardID: id,

                }, function (data) {
                    HideLoading();

                    if (data == 1) {
                        alertify.success('Deleted!');

                        // refresh page
                        location.reload();
                    } else {
                        alertify.alert(data);
                    }
                });
            });
        }
    </script>
</asp:Content>

