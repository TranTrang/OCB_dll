﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="AddCard.aspx.cs" Inherits="cp_page_card_AddCard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page" class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-card" class="breadcrumbs__element">Card</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Add new Card</span></li>
                </ul>
            </div>
        </div>
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">ADD NEW CARD</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Name:* </label>
                <input id="txtName" type="text" class="required" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Type:* </label>
                <input id="txtType" type="text" class="required" />
            </div>
        </div>
        <hr /> <!-- ========================================================================================== -->
                <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Credit Limit: </label>
                <input id="txtCreditLimit" type="text" value="0" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Chargeback Card Fee: </label>
                <input id="txtChargebackCardFee" type="text" value="0" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Day Trading Limit: </label>
                <input id="txtDayTradingLimit" type="text" value="0" />
            </div>
        </div>
        <hr /> <!-- ========================================================================================== -->
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Description: </label>
                <textarea id="txtDesc" class="tinymce" placeholder="Description"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Utilities: </label>
                <textarea id="txtUtilities" class="tinymce" placeholder="Description"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Features: </label>
                <textarea id="txtFeatures" class="tinymce" placeholder="Description"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Highlights: </label>
                <textarea id="txtHighlights" class="tinymce" placeholder="Description"></textarea>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <button class="btn btn-primary" onclick="Submit()">Submit</button>
                <button class="btn btn-danger" onclick="Cancel()">Cancel</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(4);
        });

        function Cancel() {
            location.href = "/cp-card";
        }

        function Submit() {
            var name = $('#txtName').val();
            var type = $('#txtType').val();

            var creditLimit = $('#txtCreditLimit').val();
            var chargebackCardFee = $('#txtChargebackCardFee').val();
            var dayTradingLimit = $('#txtDayTradingLimit').val();

            var desc = tinymce.get('txtDesc').getContent();
            var utilities = tinymce.get('txtUtilities').getContent();
            var features = tinymce.get('txtFeatures').getContent();
            var highlights = tinymce.get('txtHighlights').getContent();

            // check exception
            CheckFieldEmpty('page');
            var error = "";
            error += CheckFieldError();

            if (error != "") {
                alertify.alert('Error', error);
            } else {
                ShowLoading();
                $.post(LOCAL_DO_URL + 'card/add-card.aspx', {
                    name: name,
                    type: type,

                    creditLimit: creditLimit,
                    chargebackCardFee: chargebackCardFee,
                    dayTradingLimit: dayTradingLimit,

                    desc: desc,
                    utilities: utilities,
                    features: features,
                    highlights: highlights,

                }, function (data) {
                    HideLoading();

                    if (data != 1) {
                        alertify.alert('Error', data);
                    } else {
                        alertify.success('Successful!');
                        location.href = "/cp-card";
                    }
                });
            }
        }
    </script>
</asp:Content>