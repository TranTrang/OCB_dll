﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_card_Card : BaseAdminPage
{
    public List<CardTBx> listCard = new List<CardTBx>();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        CardManager CM = new CardManager();
        listCard = CM.GetList();
        listCard.Reverse();
    }
}