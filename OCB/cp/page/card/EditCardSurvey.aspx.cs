﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_card_EditCardSurvey : BaseAdminPage
{
    public CardTBx card = new CardTBx();
    public List<SurveyTBx> listAllSurvey = new List<SurveyTBx>();
    public List<CardSurveyTBx> listCardSurvey = new List<CardSurveyTBx>();

    public int currentIndex = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        int cardID = Convert.ToInt32(Page.RouteData.Values["cardID"]);

        CardManager CM = new CardManager();
        card = CM.GetByID(cardID);

        //
        SurveyManager SM = new SurveyManager();
        listAllSurvey = SM.GetList();

        CardSurveyManager CSM = new CardSurveyManager();
        listCardSurvey = CSM.GetListByCardID(cardID).OrderBy(e => e.SurveyID).ToList();
    }

    protected bool IsCheckedThisSurvey(int surveyID)
    {
        if (surveyID == listCardSurvey[currentIndex].SurveyID)
        {
            currentIndex++;
            return true;
        }
        else return false;
    }
}