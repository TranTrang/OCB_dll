﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="CardImage.aspx.cs" Inherits="cp_page_card_CardImage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-card" class="breadcrumbs__element">Card</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Card Image</span></li>
                </ul>
            </div>
        </div>
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">CARD - <%=card.Name %></h4>
            </div>
        </div>
        <div class="row page-control-container">
            <div class="col-md-6 col-lg-6 col-xs-6">
                <a onclick="CreateCardImage()" class="btn btn-info">Create new Card Image</a>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-6">
                <div class="search-box-container">
                </div>
            </div>
        </div>
        <div class="row" id="table">
            <%if (listCardImage.Count == 0) { %>
                <div class="col-md-12 col-xs-12 col-lg-12"><p>No result found.</p></div>
            <%} else { %>
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <div class="pagination-table" id="paging-table">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th width="150px">Image</th>
                                    <th width="200px">Action</th>
                                </tr>
                            </thead>
                            <tbody class="pagination-body">
                                <%foreach (var cardImage in listCardImage) {%>
                                    <tr>
                                        <td><%=cardImage.ID %></td>
                                        <td><%=cardImage.Name %></td>
                                        <td>
                                            <%if (System.IO.File.Exists(Server.MapPath("~/upload/card/" + cardImage.Name))) {%>
                                                <img src="/upload/card/<%=cardImage.Name %>" class="center-block" />
                                            <%} else { %>
                                                <img src="/images/index_v2/slider-demo.png" class="center-block" />
                                            <%} %>
                                        </td>
                                        <td>
                                            <a href="/cp-edit-card-image/cardImageID-<%=cardImage.ID %>" class="btn btn-primary">Edit Image</a>
                                            <button class="btn btn-danger" onclick="Delete(<%=cardImage.ID %>)">Delete</button>
                                        </td>
                                    </tr>
                                <%} %>
                            </tbody>
                        </table>
                        <div id="pagination"></div>
                    </div>
                </div>
            <%} %>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(3);

            Pagination(20, 'paging-table');
        });

        function CreateCardImage() {
            alertify.confirm('Do you want to CREATE a new Card Image?', function () {
                ShowLoading();
                $.post(LOCAL_DO_URL + 'card/card-image/add-card-image.aspx', {
                    cardID: <%=card.ID %>,

                }, function (data) {
                    HideLoading();

                    if (data == 1) {
                        alertify.success('Successful!');
                        location.reload();
                    } else {
                        alertify.alert(data);
                    }
                });
            });
        }

        function Delete(cardImageID) {
            alertify.confirm('Are you sure you want to DELETE this?', function () {
                ShowLoading();

                $.post(LOCAL_DO_URL + 'card/card-image/delete-card-image.aspx', {
                    cardImageID: cardImageID,

                }, function (data) {
                    HideLoading();

                    if (data == 1) {
                        alertify.success('Deleted!');
                        location.reload();
                    } else {
                        alertify.alert(data);
                    }
                });
            });
        }
    </script>
</asp:Content>

