﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.cp.page.card.card_image
{
    public partial class EditCardImage : BaseAdminPage
    {
        public CardImageTBx cardImage = new CardImageTBx();

        protected void Page_Load(object sender, EventArgs e)
        {
            InitPage();
        }

        protected override void ActionAdminPage()
        {
            int cardImageID = Convert.ToInt32(Page.RouteData.Values["cardImageID"]);

            CardImageManager CIM = new CardImageManager();
            cardImage = CIM.GetByID(cardImageID);

            if (System.IO.File.Exists(Server.MapPath("~/upload/card/" + cardImage.Name)))
            {
                imgAvatar.Src = "/upload/card/" + cardImage.Name + "?date=" + DateTime.UtcNow;
            }
            else
            {
                imgAvatar.Src = "/images/index_v2/slider-demo.png";
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (fileUpload.HasFile)
            {
                try
                {
                    // ==================== Check exception ==================== //
                    // wrong image format
                    if (fileUpload.PostedFile.ContentType != "image/jpeg" && fileUpload.PostedFile.ContentType != "image/png")
                    {
                        lblStatus.Text = "Upload status: Only accept JPEG or PNG file.";
                        return;
                    }
                    // file size is too large
                    if (fileUpload.PostedFile.ContentLength > 3072000)
                    {
                        lblStatus.Text = "Upload status: The file has to be less than 3MB.";
                        return;
                    }
                    // ==================== Check exception ==================== //

                    //get extension name of filename
                    string extension = System.IO.Path.GetExtension(fileUpload.PostedFile.FileName);
                    string name = "card_" + cardImage.CardID.ToString() + "_" + cardImage.ID.ToString() + extension;

                    // save image
                    fileUpload.SaveAs(Server.MapPath("~/upload/card/" + name));
                    CardImageManager CIM = new CardImageManager();
                    CardImageTBx cardImg = CIM.GetByID(cardImage.ID);
                    cardImg.Name = name;
                    CIM.Save();

                    //// create thumbnail
                    //string thumbnail_name = "user_" + userID + "_thumbnail" + extension;
                    //UTIL.CreateThumbnail(300, Server.MapPath("~/upload/user/" + name), Server.MapPath("~/upload/user/" + thumbnail_name));

                    // show image
                    imgAvatar.Src = "/upload/card/" + name + "?date=" + DateTime.Now;
                    lblStatus.Text = "Upload status: File uploaded!";
                }
                catch (Exception ex)
                {
                    lblStatus.Text = "Upload status: The file could not be uploaded. The following error occurred: " + ex.Message;
                }
            }
        }
    }
}