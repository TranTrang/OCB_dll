﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_card_CardImage : BaseAdminPage
{
    public CardTBx card = new CardTBx();
    public List<CardImageTBx> listCardImage = new List<CardImageTBx>();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        int cardID = Convert.ToInt32(Page.RouteData.Values["cardID"]);

        CardManager CM = new CardManager();
        card = CM.GetByID(cardID);

        CardImageManager CIM = new CardImageManager();
        listCardImage = CIM.GetListByCardID(cardID);
        listCardImage.Reverse();
    }
}