﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeBehind="EditCardImage.aspx.cs" Inherits="OCB.cp.page.card.card_image.EditCardImage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-card-image/cardID-<%=cardImage.CardID %>" class="breadcrumbs__element">Card Image</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Add Card Image</span></li>
                </ul>
            </div>
        </div>
        <form class="row" runat="server">
            <div class="col-sm-6 col-md-6 col-xs-12">
                <h3 class="box-title">Upload Image</h3>
                <img id="imgAvatar" runat="server" class="width-auto center-block" style="width: 400px" /><br />
            </div>
            <div class="col-sm-6 col-md-6 col-xs-12">
                <h3>
                    <br />
                </h3>
                <asp:FileUpload ID="fileUpload" runat="server" CssClass="btn btn-info" /><br />
                <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btn btn-primary" OnClick="btnUpload_Click" /><br />
                <br />
                <asp:Label ID="lblStatus" runat="server" Text="Upload status: "></asp:Label>
            </div>
        </form>
        <br />
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <button class="btn btn-danger" onclick="Cancel()">Cancel</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(3);
        });

        function Cancel() {
            location.href = "/cp-card-image/cardID-<%=cardImage.CardID %>";
        }
    </script>
</asp:Content>
