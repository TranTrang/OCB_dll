﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="EditCardSurvey.aspx.cs" Inherits="cp_page_card_EditCardSurvey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page" class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-card" class="breadcrumbs__element">Card</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Edit Card Survey</span></li>
                </ul>
            </div>
        </div>
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">EDIT SURVEY of CARD (<%=card.Name %>)</h4>
            </div>
        </div>
        <%for (int i = 0; i < listAllSurvey.Count; i++){%>
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xs-12 ">
                    <%if (listCardSurvey.Count > 0 && currentIndex < listCardSurvey.Count && IsCheckedThisSurvey(listAllSurvey[i].ID)) {%>
                        <input id="survey<%=listAllSurvey[i].ID %>" type="checkbox" checked="checked" />
                    <%} else {%>
                        <input id="survey<%=listAllSurvey[i].ID %>" type="checkbox" />
                    <%} %>
                    <label><%=listAllSurvey[i].Text %> </label>
                </div>
            </div>
        <%} %>
        <br />
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <button class="btn btn-primary" onclick="Submit()">Submit</button>
                <button class="btn btn-danger" onclick="Cancel()">Cancel</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(3);
        });

        function Cancel() {
            location.href = "/cp-card";
        }

        function Submit() {
            var strSurvey = "";
            <%for (int i = 0; i < listAllSurvey.Count; i++) {%>
                var value = $('#survey<%=listAllSurvey[i].ID %>').prop('checked');
                if (value == true) {
                    strSurvey += <%=listAllSurvey[i].ID.ToString() %> + "@";
                }
                
            <%} %>
            strSurvey = strSurvey.slice(0, -1); // remove last "@"

            // check exception
            CheckFieldEmpty('page');
            var error = "";
            error += CheckFieldError();

            if (error != "") {
                alertify.alert('Error', error);
            } else {
                ShowLoading();
                $.post(LOCAL_DO_URL + 'card/edit-card-survey.aspx', {
                    cardID: <%=card.ID %>,
                    strSurvey: strSurvey,

                }, function (data) {
                    HideLoading();

                    if (data != 1) {
                        alertify.alert('Error', data);
                    } else {
                        alertify.success('Successful!');
                        location.href = "/cp-card";
                    }
                });
            }
        }
    </script>
</asp:Content>