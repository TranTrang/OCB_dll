﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeBehind="Country.aspx.cs" Inherits="OCB.cp.page.country.Country" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">COUNTRY</h4>
            </div>
        </div>
        <div class="row page-control-container">
            <div class="col-md-3 col-lg-2 col-xs-6">
                <a href="/cp-add-country" style="width: 100%" class="btn btn-info">Add new country</a>
            </div>
            <form runat="server">
                <div class="col-md-3 col-lg-2 col-xs-6">
                    <asp:Button CssClass="btn btn-primary" ID="ExportCountry" Text="Export Country" runat="server" OnClick="ExportCountry_Click" Width="100%" />
                </div>
                <div class="col-md-3 col-lg-2 col-xs-6">
                    <asp:Button ID="btnImport" runat="server" Text="Import Country" CssClass="btn btn-danger" OnClick="btnImport_Click" Width="100%" />
                </div>
                <div class="col-md-3 col-lg-2 col-xs-6">
                    <asp:FileUpload ID="fileUpload" runat="server" CssClass="btn btn-info" Width="100%" /><br />
                </div>
            </form>
            <div class="col-md-12 col-lg-4 col-xs-12">
                <div class="search-box-container">
                    <input type="text" placeholder="Search country..." id="txtSearchInput" />
                    <span><i class="fas fa-search"></i></span>
                </div>
            </div>

        </div>
        <div class="row" id="table">
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(6);

            $('#txtSearchInput').keydown(function (e) {
                if (e.keyCode == 13) {
                    var strSearch = $(this).val();

                    Search(strSearch, "country/search-country.aspx");
                }
            });

            Search("", "country/search-country.aspx");
        });

        function Delete(id) {
            alertify.confirm('Are you sure you want to DELETE this?', function () {
                ShowLoading();

                $.post(LOCAL_DO_URL + 'country/delete-country.aspx', {
                    id: id,
                }, function (data) {
                    HideLoading();

                    if (data == 1) {
                        alertify.success('Deleted!');

                        // refresh page
                        location.reload();
                    } else {
                        alertify.alert(data);
                    }
                });
            });
        }
    </script>
</asp:Content>
