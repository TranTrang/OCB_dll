﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="AddCountry.aspx.cs" Inherits="cp_page_country_AddCountry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page" class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-country" class="breadcrumbs__element">Country</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Add new country</span></li>
                </ul>
            </div>
        </div>
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">ADD NEW COUNTRY</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Name:* </label>
                <input id="txtName" type="text" class="required" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Code:* </label>
                <input id="txtCode" type="text" class="required" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Order: </label>
                <input id="txtOrder" type="number" class="required" value="0" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <button class="btn btn-primary" onclick="Submit()">Submit</button>
                <button class="btn btn-danger" onclick="Cancel()">Cancel</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(6);
        });

        function Cancel() {
            location.href = "/cp-country";
        }

        function Submit() {
            var name = $('#txtName').val();
            var code = $('#txtCode').val();
            var order = $('#txtOrder').val();

            // check exception
            CheckFieldEmpty('page');
            var error = "";
            error += CheckFieldError();

            if (error != "") {
                alertify.alert('Error', error);
            } else {
                ShowLoading();
                $.post(LOCAL_DO_URL + 'country/add-country.aspx', {
                    name: name,
                    code: code,
                    order: order,

                }, function (data) {
                    HideLoading();
                    if (data == 0) {
                        alertify.error('Country is existed');
                    } else if (data == 1) {
                        alertify.success('Successful!');
                        location.href = "/cp-country";
                    } else {
                        alertify.alert('Error', data);
                    }
                });
            }
        }
    </script>
</asp:Content>

