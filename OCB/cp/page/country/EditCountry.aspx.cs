﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_country_EditCountry : BaseAdminDo
{
    protected CountryTBx country;
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int id = Convert.ToInt32(Page.RouteData.Values["countryID"]);
        CountryManager CM = new CountryManager();
        country = CM.GetByID(id);
    }

}