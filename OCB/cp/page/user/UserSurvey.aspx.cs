﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_user_UserSurvey : BaseAdminPage
{
    public UserTBx user = new UserTBx();
    public List<CardSurveyValueTBx> listCardSurveyValue = new List<CardSurveyValueTBx>();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        int userID = Convert.ToInt32(Page.RouteData.Values["userID"]);

        UserManager UM = new UserManager();
        user = UM.GetByID(userID);

        CardSurveyValueManager CSVM = new CardSurveyValueManager();
        listCardSurveyValue = CSVM.GetListByUserID(userID).OrderBy(e => e.SurveyTBx.step).ThenBy(e => e.SurveyID).ToList();

        // customize answers
        CardSurveyValueTBx[] arrBirth = new CardSurveyValueTBx[3];
        CardSurveyValueTBx[] arrProfileDate = new CardSurveyValueTBx[3];
        for (int i = 0; i < listCardSurveyValue.Count; i++)
        {
            // birthday
            if (listCardSurveyValue[i].SurveyID == 17) { arrBirth[0] = listCardSurveyValue[i]; }
            if (listCardSurveyValue[i].SurveyID == 49) { arrBirth[1] = listCardSurveyValue[i]; listCardSurveyValue.RemoveAt(i); i--; }
            if (listCardSurveyValue[i].SurveyID == 16) { arrBirth[2] = listCardSurveyValue[i]; listCardSurveyValue.RemoveAt(i); i--; }

            // profile day
            if (listCardSurveyValue[i].SurveyID == 25) { arrProfileDate[0] = listCardSurveyValue[i]; }
            if (listCardSurveyValue[i].SurveyID == 54) { arrProfileDate[1] = listCardSurveyValue[i]; listCardSurveyValue.RemoveAt(i); i--; }
            if (listCardSurveyValue[i].SurveyID == 50) { arrProfileDate[2] = listCardSurveyValue[i]; listCardSurveyValue.RemoveAt(i); i--; }
        }

        //birth day
        if (arrBirth[0] != null && arrBirth[1] != null && arrBirth[2] != null)
        {
            if (arrBirth[0].TextValue.Length == 1) arrBirth[0].TextValue = "0" + arrBirth[0].TextValue;
            if (arrBirth[1].TextValue.Length == 1) arrBirth[1].TextValue = "0" + arrBirth[1].TextValue;
            CardSurveyValueTBx valueBirthDay = listCardSurveyValue.Where(e => e.SurveyID == 17).First();
            valueBirthDay.TextValue = arrBirth[0].TextValue + "/" + arrBirth[1].TextValue + "/" + arrBirth[2].TextValue;
            valueBirthDay.SurveyTBx.Code = "Ngày Tháng Năm Sinh";
        }

        // profile day
        if (arrProfileDate[0] != null && arrProfileDate[1] != null && arrProfileDate[2] != null)
        {
            if (arrProfileDate[0].TextValue.Length == 1) arrProfileDate[0].TextValue = "0" + arrProfileDate[0].TextValue;
            if (arrProfileDate[1].TextValue.Length == 1) arrProfileDate[1].TextValue = "0" + arrProfileDate[1].TextValue;
            CardSurveyValueTBx valueProfileDay = listCardSurveyValue.Where(e => e.SurveyID == 25).First();
            valueProfileDay.TextValue = arrProfileDate[0].TextValue + "/" + arrProfileDate[1].TextValue + "/" + arrProfileDate[2].TextValue;
            valueProfileDay.SurveyTBx.Code = "Ngày cấp giấy tờ";
        }
    }

    public DateTime GetLocalTime(DateTime dateTime)
    {
        DateTime utcDateTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
        return utcDateTime.ToLocalTime();
    }
}