﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_user_EditUser : BaseAdminPage
{
    public UserTBx user;
    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        int id = Convert.ToInt32(Page.RouteData.Values["id"]);

        UserManager UM = new UserManager();
        user = UM.GetByID(id);
    }
}