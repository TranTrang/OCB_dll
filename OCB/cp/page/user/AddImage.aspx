﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeBehind="AddImage.aspx.cs" Inherits="OCB.cp.page.user.AddImage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-user" class="breadcrumbs__element">Users</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Add user image</span></li>
                </ul>
            </div>
        </div>
        <form class="row" runat="server">
            <div class="col-sm-6 col-md-6 col-xs-12">
                <h3 class="box-title">Upload Image</h3>
                <img id="imgAvatar" runat="server" class="width-auto center-block" /><br />
            </div>
            <div class="col-sm-6 col-md-6 col-xs-12">
                <h3>
                    <br />
                </h3>
                <asp:FileUpload ID="fileUpload" runat="server" CssClass="btn btn-info" /><br />
                <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btn btn-primary" OnClick="btnUpload_Click" /><br /><br />
                <asp:Label ID="lblStatus" runat="server" Text="Upload status: "></asp:Label>
            </div>
        </form>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <button class="btn btn-danger" onclick="Cancel()">Cancel</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(2);
        });

        function Cancel() {
            location.href = "/cp-user";
        }
    </script>
</asp:Content>
