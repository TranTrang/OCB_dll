﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="User.aspx.cs" Inherits="cp_page_user_User" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">USERS</h4>
            </div>
        </div>
        <div class="row page-control-container">
            <div class="col-md-6 col-lg-6 col-xs-6">
                <%--<a href="/cp-add-user" class="btn btn-info">Add new user</a>--%>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-6">
                <div class="search-box-container">
                    <input type="text" placeholder="Search Phone..." id="txtSearchInput" />
                    <span><i class="fas fa-search"></i></span>
                </div>
            </div>
        </div>
        <div class="row" id="table">
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(1);

            $('#txtSearchInput').keydown(function (e) {
                if (e.keyCode == 13) {
                    var strSearch = $(this).val();

                    Search(strSearch, "user/search-user.aspx");
                }
            });

            Search("", "user/search-user.aspx");
        });

        function Delete(id) {
            alertify.confirm('Are you sure you want to DELETE this?', function () {
                ShowLoading();

                $.post(LOCAL_DO_URL + 'user/delete-user.aspx', {
                    id: id,
                }, function (data) {
                    HideLoading();

                    if (data == 1) {
                        alertify.success('Deleted!');

                        // refresh page
                        location.reload();
                    } else {
                        alertify.alert(data);
                    }
                });
            });
        }
    </script>
</asp:Content>

