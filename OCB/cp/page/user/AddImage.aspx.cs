﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.cp.page.user
{
    public partial class AddImage : BaseAdminPage
    {
        public int userID;
        protected void Page_Load(object sender, EventArgs e)
        {
            InitPage();
        }

        protected override void ActionAdminPage()
        {
            userID = Convert.ToInt32(Page.RouteData.Values["id"]);

            if (!System.IO.File.Exists(Server.MapPath("~/upload/user/user_" + userID + ".jpg")))
            {
                imgAvatar.Src = "/cp/images/default-avatar.jpg";
            }
            else
            {
                imgAvatar.Src = "/upload/user/user_" + userID + ".jpg?date=" + DateTime.UtcNow;
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (fileUpload.HasFile)
            {
                try
                {
                    // ==================== Check exception ==================== //
                    // wrong image format
                    if (fileUpload.PostedFile.ContentType != "image/jpeg")
                    {
                        lblStatus.Text = "Upload status: Only accept JPEG file.";
                        return;
                    }
                    // file size is too large
                    if (fileUpload.PostedFile.ContentLength > 2048000)
                    {
                        // file should less than 2MB to load web quickly
                        lblStatus.Text = "Upload status: The file has to be less than 2MB.";
                        return;
                    }
                    // ==================== Check exception ==================== //

                    //get extension name of filename
                    string extension = System.IO.Path.GetExtension(fileUpload.PostedFile.FileName);
                    string name = "user_" + userID + extension;

                    // save image
                    fileUpload.SaveAs(Server.MapPath("~/upload/user/" + name));

                    // create thumbnail
                    string thumbnail_name = "user_" + userID + "_thumbnail" + extension;
                    UTIL.CreateThumbnail(300, Server.MapPath("~/upload/user/" + name), Server.MapPath("~/upload/user/" + thumbnail_name));

                    // show image
                    imgAvatar.Src = "/upload/user/" + name + "?date=" + DateTime.Now;
                    lblStatus.Text = "Upload status: File uploaded!";
                }
                catch (Exception ex)
                {
                    lblStatus.Text = "Upload status: The file could not be uploaded. The following error occurred: " + ex.Message;
                }
            }
        }
    }
}