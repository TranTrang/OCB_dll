﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_user_profile_image_UserProfileImage : BaseAdminPage
{
    public UserTBx user = new UserTBx();
    public List<UserProfileImageTBx> listProfileImg = new List<UserProfileImageTBx>();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        int userID = Convert.ToInt32(Page.RouteData.Values["userID"]);

        UserManager UM = new UserManager();
        user = UM.GetByID(userID);

        UserProfileImageManager UPIM = new UserProfileImageManager();
        listProfileImg = UPIM.GetListByPhoneNumber(user.Phone);
    }
}