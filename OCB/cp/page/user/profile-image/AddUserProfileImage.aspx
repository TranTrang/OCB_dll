﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeBehind="AddUserProfileImage.aspx.cs" Inherits="OCB.cp.page.user.profile_image.AddUserProfileImage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <form class="container-fluid" runat="server">
        <div class="row m-bottom-10">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-user-profile-image/userID-<%=user.ID %>" class="breadcrumbs__element">Users</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Add user image</span></li>
                </ul>
            </div>
        </div>
        <div class="row m-bottom-10">
            <div class="col-sm-4 col-md-4 col-xs-6">
                <asp:DropDownList ID="ddlUploadSurvey" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlUploadSurvey_SelectedIndexChanged"></asp:DropDownList>
            </div>
            <div class="col-sm-4 col-md-4 col-xs-6">
                <asp:DropDownList ID="ddlSurveyChild" runat="server" AutoPostBack="false" CssClass="form-control"></asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6 col-xs-12">
                <h3>Image Preview</h3>
                <img id="imgUserProfile" runat="server" class="table-image" />
            </div>
            <div class="col-sm-6 col-md-6 col-xs-12">
                <h3>Upload Image</h3>
                <asp:FileUpload ID="fileUpload" runat="server" CssClass="btn btn-info" /><br />
                <asp:Label ID="lblStatus" runat="server" Text="Upload status: "></asp:Label>
                <br />
                <br />
                <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btn btn-primary" OnClick="btnUpload_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-danger" OnClick="btnCancel_Click" />
            </div>
        </div>
    </form>
</asp:Content>
