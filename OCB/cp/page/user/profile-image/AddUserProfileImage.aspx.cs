﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.cp.page.user.profile_image
{
    public partial class AddUserProfileImage : BaseAdminPage
    {
        public UserTBx user = new UserTBx();
        public List<SurveyTBx> listUploadSurvey = new List<SurveyTBx>();
        public List<SurveyItemValueTBx> listTempUploadSurveyIteml = new List<SurveyItemValueTBx>();

        private string phoneNumber;

        public string PhoneNumber
        {
            get
            {
                if (ViewState["phoneNumber"] == null)
                {
                    ViewState["phoneNumber"] = 0;
                }

                phoneNumber = ViewState["phoneNumber"].ToString();

                return phoneNumber;
            }
            set
            {
                phoneNumber = value;
                ViewState["phoneNumber"] = phoneNumber;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitPage();
            }
        }

        protected override void ActionAdminPage()
        {
            int userID = Convert.ToInt32(Page.RouteData.Values["userID"]);

            UserManager UM = new UserManager();
            user = UM.GetByID(userID);
            if (user.ID == 0)
            {
                Response.Write("User not exist!");
                return;
            }

            PhoneNumber = user.Phone;

            // ============================== Get dropdownlist survey ==============================
            SurveyManager SM = new SurveyManager();
            listUploadSurvey = SM.GetListSurveysUploadImage();

            ddlUploadSurvey.DataSource = listUploadSurvey;
            ddlUploadSurvey.DataTextField = "Text";
            ddlUploadSurvey.DataValueField = "ID";
            ddlUploadSurvey.DataBind();
            ddlUploadSurvey.Items[0].Selected = true;
            // ============================== Get dropdownlist survey ==============================

            BindSurveyItem(Convert.ToInt32(ddlUploadSurvey.SelectedValue));
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (fileUpload.HasFile)
            {
                try
                {
                    // ==================== Check exception ==================== //
                    // wrong image format
                    if (fileUpload.PostedFile.ContentType != "image/jpeg")
                    {
                        lblStatus.Text = "Upload status: Only accept JPEG file.";
                        return;
                    }
                    // file size is too large
                    if (fileUpload.PostedFile.ContentLength > 2048000)
                    {
                        // file should less than 2MB to load web quickly
                        lblStatus.Text = "Upload status: The file has to be less than 2MB.";
                        return;
                    }
                    // ==================== Check exception ==================== //

                    //get extension name of filename
                    string extension = System.IO.Path.GetExtension(fileUpload.PostedFile.FileName);

                    string type = ddlSurveyChild.SelectedItem.Text;

                    string name = PhoneNumber + UTIL.FormatLink(type) + "-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extension;

                    // save image
                    fileUpload.SaveAs(Server.MapPath("~/upload/userInfo/" + name));

                    // save database
                    UserProfileImageManager UPIM = new UserProfileImageManager();
                    UserProfileImageTBx userProfileImage = new UserProfileImageTBx();

                    userProfileImage.IsDeleted = false;
                    userProfileImage.Type = type;
                    userProfileImage.LinkURL = name;
                    userProfileImage.UserPhoneNumber = PhoneNumber;

                    UPIM.Add(userProfileImage);

                    // show image
                    imgUserProfile.Src = "/upload/userInfo/" + name + "?date=" + DateTime.Now;
                    lblStatus.Text = "Upload status: File uploaded!";
                }
                catch (Exception ex)
                {
                    lblStatus.Text = "Upload status: The file could not be uploaded. The following error occurred: " + ex.Message;
                }
            }
        }

        protected List<SurveyItemValueTBx> GetListSurveyItem(int surveyID)
        {
            SurveyItemValueManager SIVM = new SurveyItemValueManager();
            return SIVM.GetListBySurveyID(surveyID);
        }

        private void BindSurveyItem(int surveyID)
        {
            // ============================== Get dropdownlist survey child ==============================
            List<SurveyItemValueTBx> listSurveyChild = GetListSurveyItem(surveyID);
            ddlSurveyChild.DataSource = listSurveyChild;
            ddlSurveyChild.DataTextField = "Value";
            ddlSurveyChild.DataValueField = "ID";
            ddlSurveyChild.DataBind();
            // ============================== Get dropdownlist survey child ==============================
        }

        protected void ddlUploadSurvey_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSurveyItem(Convert.ToInt32(ddlUploadSurvey.SelectedValue));
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            int userID = Convert.ToInt32(Page.RouteData.Values["userID"]);

            Response.Redirect("/cp-user-profile-image/userID-" + userID);
        }
    }
}