﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="UserProfileImage.aspx.cs" Inherits="cp_page_user_profile_image_UserProfileImage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">Profile - <%=user.Email %></h4>
            </div>
        </div>
        <div class="row page-control-container">
            <div class="col-md-6 col-lg-6 col-xs-6">
                <a href="/cp-add-user-profile-image/userID-<%=user.ID %>" class="btn btn-info">Upload New Profile</a>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-6">
                <div class="search-box-container">
                </div>
            </div>
        </div>
        <div class="row" id="table">
            <%if (listProfileImg.Count == 0) { %>
                <div class="col-md-12 col-xs-12 col-lg-12"><p>No result found.</p></div>
            <%} else { %>
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <div class="pagination-table" id="paging-table">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Type</th>
                                    <th>Name</th>
                                    <th width="150px">Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="pagination-body">
                                <%foreach (var profile in listProfileImg) {%>
                                    <tr>
                                        <td><%=profile.ID %></td>
                                        <td><%=profile.Type %></td>
                                        <td><%=profile.LinkURL %></td>
                                        <td>
                                            <%if (System.IO.File.Exists(Server.MapPath("~/upload/userInfo/" + profile.LinkURL))) {%>
                                                <img src="/upload/userInfo/<%=profile.LinkURL %>" class="center-block" />
                                            <%} else { %>
                                                <img src="/cp/images/default-image.jpg" class="center-block" />
                                            <%} %>
                                        </td>
                                        <td>
                                            <%--<a href="/cp-edit-/id-<%=profile.ID %>" class="btn btn-primary">Edit</a>--%>
                                            <button class="btn btn-danger" onclick="Delete(<%=profile.ID %>)">Delete</button>
                                        </td>
                                    </tr>
                                <%} %>
                            </tbody>
                        </table>
                        <div id="pagination"></div>
                    </div>
                </div>
            <%} %>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(10);

            Pagination(50, 'paging-table');
        });

        function Delete(id) {
            alertify.confirm('Are you sure you want to DELETE this?', function () {
                ShowLoading();

                $.post(LOCAL_DO_URL + 'user/profile-image/delete-profile-image.aspx', {
                    profileImageID: id,

                }, function (data) {
                    HideLoading();

                    if (data == 1) {
                        alertify.success('Deleted!');
                        location.reload();
                    } else {
                        alertify.alert(data);
                    }
                });
            });
        }
    </script>
</asp:Content>




