﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="EditUser.aspx.cs" Inherits="cp_page_user_EditUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-user" class="breadcrumbs__element">Users</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Edit user</span></li>
                </ul>
            </div>
        </div>
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">EDIT USER</h4>
            </div>
        </div>
       <%-- <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Full name: </label>
                <input type="text" class="required" placeholder="Your full name" id="txtFullName" value="<%=user.Fullname %>" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-6 col-xs-6 form">
                <label>Email: </label>
                <input type="text" class="required" placeholder="Your email" id="txtEmail" value="<%=user.Email %>" />
            </div>
            <div class="col-md-6 col-lg-6 col-xs-6 form">
                <label>Phone: </label>
                <input type="text" class="required" placeholder="Your phone number" id="txtPhone" value="<%=user.Phone %>" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Short description: </label>
                <textarea class="required" placeholder="Short description" id="txtShortDesc"><%=user.ShortDesc %></textarea>
            </div>
        </div>--%>
      <%--  <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Description: </label>
                <textarea class="tinymce required" placeholder="Description" id="txtDesc"><%=user.Desc %></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <button class="btn btn-primary" onclick="Submit()">Submit</button>
                <button class="btn btn-danger" onclick="Cancel()">Cancel</button>
            </div>
        </div>--%>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(2);
        });

        function Cancel() {
            location.href = "/cp-user";
        }

        function Submit() {
            var id = '<%=user.ID%>';

            var fullName = $('#txtFullName').val();
            var email = $('#txtEmail').val();
            var phone = $('#txtPhone').val();

            var shortDesc = $('#txtShortDesc').val();
            var desc = tinymce.get('txtDesc').getContent();
            
            // check exception
            CheckFieldEmpty();
            var error = CheckEmailValid(email);
            error += CheckFieldError();

            if (error != "") { // has error
                alertify.alert('Error', error);
            } else {
                ShowLoading();

                // post data to server
                // $.post( url [, data ] [, success ] [, dataType ] )
                $.post(LOCAL_URL + 'user/edit-user.aspx', {
                    id: id,
                    fullName: fullName,
                    email: email,
                    phone: phone,

                    shortDesc: shortDesc,
                    desc: desc,
                }, function (data) {
                    HideLoading();

                    if (data != 1) {
                        alertify.alert('Error', data);
                    } else {
                        alertify.success('Successful!');
                        location.href = "/cp-user";
                    }
                });
            }
        }
    </script>
</asp:Content>

