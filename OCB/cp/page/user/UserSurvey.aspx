﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="UserSurvey.aspx.cs" Inherits="cp_page_user_UserSurvey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-user" class="breadcrumbs__element">User</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">User Survey</span></li>
                </ul>
            </div>
        </div>
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">USER SURVEY (<%=user.Email %>) - <%=(GetLocalTime(Convert.ToDateTime(listCardSurveyValue[0].CreatedDate))).ToString("dd/MM/yyyy hh:mm:ss") %></h4>
            </div>
        </div>
        <div class="row" id="table">
            <%if (listCardSurveyValue.Count == 0) { %>
                <div class="col-md-12 col-xs-12 col-lg-12"><p>No result found.</p></div>
            <%} else { %>
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <div class="pagination-table" id="paging-table">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Step</th>
                                    <th>Survey ID</th>
                                    <th>Survey</th>
                                    <th>Answer</th>
                                </tr>
                            </thead>
                            <tbody class="pagination-body">
                                <%for (int i = 0; i < listCardSurveyValue.Count; i++) {%>
                                    <tr>
                                        <td><%=listCardSurveyValue[i].SurveyTBx.step %></td>
                                        <td><%=listCardSurveyValue[i].SurveyTBx.ID %></td>
                                        <%if (listCardSurveyValue[i].SurveyID != 0 && listCardSurveyValue[i].SurveyID != null) {%>
                                            <td><%=listCardSurveyValue[i].SurveyTBx.Code %></td>
                                        <%}else {%>
                                            <td>Not value</td>
                                        <%} %>
                                        <td><%=listCardSurveyValue[i].TextValue %></td>
                                    </tr>
                                <%} %>
                            </tbody>
                        </table>
                        <div id="pagination"></div>
                    </div>
                </div>
            <%} %>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(1);
        });
    </script>
</asp:Content>

