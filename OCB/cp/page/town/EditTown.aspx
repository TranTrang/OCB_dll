﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="EditTown.aspx.cs" Inherits="cp_page_town_EditTown" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page" class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-town" class="breadcrumbs__element">Town</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Add new town</span></li>
                </ul>
            </div>
        </div>
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">EDIT TOWN</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Province:* </label>
                <select id="txtProvince" class="required">
                    <%foreach (var province in listProvince)
                        {
                            if (province.ID == town.ProvinceID)
                            { %>
                    <option value="<%=province.ID %>" selected><%=province.Name %></option>
                    <%}
                        else
                        { %>
                    <option value="<%=province.ID %>"><%=province.Name %></option>
                    <%}
                        } %>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Name:* </label>
                <input id="txtName" type="text" class="required" value="<%=town.Name %>" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Code:* </label>
                <input id="txtCode" type="text" class="required" value="<%=town.TownID %>" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Order: </label>
                <input id="txtOrder" type="number" class="required" value="<%=town.OrderBy %>" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <button class="btn btn-primary" onclick="Submit()">Submit</button>
                <button class="btn btn-danger" onclick="Cancel()">Cancel</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(8);
        });

        function Cancel() {
            location.href = "/cp-town";
        }

        function Submit() {
            var id = <%=town.ID %>;
            var name = $('#txtName').val();
            var code = $('#txtCode').val();
            var province = $("#txtProvince").val();
            var order = $('#txtOrder').val();

            // check exception
            CheckFieldEmpty('page');
            var error = "";
            error += CheckFieldError();

            if (error != "") {
                alertify.alert('Error', error);
            } else {
                ShowLoading();
                $.post(LOCAL_DO_URL + 'town/edit-town.aspx', {
                    id: id,
                    name: name,
                    code: code,
                    province: province,
                    order: order,

                }, function (data) {
                    HideLoading();
                    if (data == 1) {
                        alertify.success('Successful!');
                        location.href = "/cp-town";
                    } else {
                        alertify.alert('Error', data);
                    }
                });
            }
        }
    </script>
</asp:Content>

