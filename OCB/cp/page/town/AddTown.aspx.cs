﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_town_AddTown : BaseAdminDo
{
    protected List<ProvinceTBx> listProvince;
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        ProvinceManager PM = new ProvinceManager();
        listProvince = PM.GetList();
    }
}