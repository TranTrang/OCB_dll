﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_town_EditTown : BaseAdminDo
{
    protected List<ProvinceTBx> listProvince;
    protected TownTBx town;
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        ProvinceManager PM = new ProvinceManager();
        TownManager TM = new TownManager();
        listProvince = PM.GetList();
        int id = Convert.ToInt32(Page.RouteData.Values["townID"]);
        town = TM.GetByID(id);
    }
}