﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_survey_survey_item_value_SurveyItemValue : BaseAdminPage
{
    public SurveyTBx survey = new SurveyTBx();
    public List<SurveyItemValueTBx> listValue = new List<SurveyItemValueTBx>();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        int surveyID = Convert.ToInt32(Page.RouteData.Values["surveyID"]);

        SurveyManager SM = new SurveyManager();
        survey = SM.GetByID(surveyID);

        SurveyItemValueManager SIVM = new SurveyItemValueManager();
        listValue = SIVM.GetListBySurveyID(surveyID);
    }
}