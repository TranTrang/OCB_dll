﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="EditSurveyItemValue.aspx.cs" Inherits="cp_page_survey_survey_item_value_EditSurveyItemValue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page" class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-survey" class="breadcrumbs__element">Survey</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Add new Survey</span></li>
                </ul>
            </div>
        </div>
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">ADD NEW ITEM VALUE OF SURVEY - <%=itemValue.SurveyTBx.Code %></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Value:* </label>
                <input id="txtValue" type="text" class="required" value="<%=itemValue.Value %>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Order By:* </label>
                <input id="txtOrderBy" type="number" class="required" value="<%=itemValue.OrderBy %>" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>T24ID:* </label>
                <input id="txtT24ID" type="text" class="required" value="<%=itemValue.T24ID %>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Group: </label>
                <input id="txtGroup" type="text" class="required" value="<%=itemValue.Group %>" />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <button class="btn btn-primary" onclick="Submit()">Submit</button>
                <button class="btn btn-danger" onclick="Cancel()">Cancel</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(4);
        });

        function Cancel() {
            location.href = "/cp-survey-item-value/surveyID-<%=itemValue.SurveyTBx.ID %>";
        }

        function Submit() {
            var value = $('#txtValue').val();
            var orderBy = $('#txtOrderBy').val();
            var T24ID = $('#txtT24ID').val();
            var group = $('#txtGroup').val();

            // check exception
            CheckFieldEmpty('page');
            var error = "";
            error += CheckFieldError();

            if (error != "") {
                alertify.alert('Error', error);
            } else {
                ShowLoading();
                $.post(LOCAL_DO_URL + 'survey/survey-item-value/edit-survey-item-value.aspx', {
                    surveyItemValueID: <%=itemValue.ID %>,
                    value: value,
                    orderBy: orderBy,
                    T24ID: T24ID,
                    group: group,

                }, function (data) {
                    HideLoading();

                    if (data != 1) {
                        alertify.alert('Error', data);
                    } else {
                        alertify.success('Successful!');
                        location.href = "/cp-survey-item-value/surveyID-<%=itemValue.SurveyTBx.ID %>";
                    }
                });
            }
        }
    </script>
</asp:Content>


