﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="SurveyItemValue.aspx.cs" Inherits="cp_page_survey_survey_item_value_SurveyItemValue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">Item Value Of Survey - <%=survey.Code %></h4>
            </div>
        </div>
        <div class="row page-control-container">
            <div class="col-md-6 col-lg-6 col-xs-6">
                <a href="/cp-add-survey-item-value/surveyID-<%=survey.ID %>" class="btn btn-info">Add new Item Value</a>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-6">
                <div class="search-box-container">
                    <%--<input type="text" placeholder="Search..." id="txtSearchInput" />
                    <span><i class="fas fa-search"></i></span>--%>
                </div>
            </div>
        </div>
        <div class="row" id="table">
            <%if (listValue.Count == 0) { %>
                <div class="col-md-12 col-xs-12 col-lg-12"><p>No result found.</p></div>
            <%} else { %>
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <div class="pagination-table" id="paging-table">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>value</th>
                                    <th>OrderBy</th>
                                    <th>T24ID</th>
                                    <th>Group</th>
                                    <th>Manage</th>
                                </tr>
                            </thead>
                            <tbody class="pagination-body">
                                <%foreach (var value in listValue) {%>
                                    <tr>
                                        <td><%=value.ID %></td>
                                        <td><%=value.Value %></td>
                                        <td><%=value.OrderBy %></td>
                                        <td><%=value.T24ID %></td>
                                        <td><%=value.Group %></td>
                                        <td>
                                            <a href="/cp-edit-survey-item-value/surveyItemValueID-<%=value.ID %>" class="btn btn-primary">Edit</a>
                                            <button class="btn btn-danger" onclick="Delete(<%=value.ID %>)">Delete</button>
                                        </td>
                                    </tr>
                                <%} %>
                            </tbody>
                        </table>
                        <div id="pagination"></div>
                    </div>
                </div>
            <%} %>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(4);

            Pagination(20, 'paging-table');
        });

        function Delete(id) {
            alertify.confirm('Are you sure you want to DELETE this?', function () {
                ShowLoading();

                $.post(LOCAL_DO_URL + 'survey/survey-item-value/delete-survey-item-value.aspx', {
                    surveyItemValueID: id,

                }, function (data) {
                    HideLoading();

                    if (data == 1) {
                        alertify.success('Deleted!');

                        // refresh page
                        location.reload();
                    } else {
                        alertify.alert(data);
                    }
                });
            });
        }
    </script>
</asp:Content>

