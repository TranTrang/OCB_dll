﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_survey_survey_item_value_EditSurveyItemValue : BaseAdminPage
{
    public SurveyItemValueTBx itemValue = new SurveyItemValueTBx();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        int surveyItemValueID = Convert.ToInt32(Page.RouteData.Values["surveyItemValueID"]);

        SurveyItemValueManager SIVM = new SurveyItemValueManager();
        itemValue = SIVM.GetByID(surveyItemValueID);
    }
}