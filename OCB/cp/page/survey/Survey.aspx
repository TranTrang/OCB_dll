﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="Survey.aspx.cs" Inherits="cp_page_survey_Survey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">SURVEY</h4>
            </div>
        </div>
        <div class="row page-control-container">
            <div class="col-md-6 col-lg-6 col-xs-6">
                <a href="/cp-add-survey" class="btn btn-info">Add new Survey</a>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-6">
                <div class="search-box-container">
                    <input type="text" placeholder="Search question..." id="txtSearchInput" />
                    <span><i class="fas fa-search"></i></span>
                </div>
            </div>
        </div>
        <div class="row" id="table">
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(4);

            $('#txtSearchInput').keydown(function (e) {
                if (e.keyCode == 13) {
                    var strSearch = $(this).val();

                    Search(strSearch, "survey/search-survey.aspx");
                }
            });
            Search("", "survey/search-survey.aspx");
        });
    </script>
</asp:Content>

