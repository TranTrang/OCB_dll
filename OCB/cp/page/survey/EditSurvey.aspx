﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="EditSurvey.aspx.cs" Inherits="cp_page_survey_EditSurvey" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page" class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-survey" class="breadcrumbs__element">Survey</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Edit Survey</span></li>
                </ul>
            </div>
        </div>
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">EDIT SURVEY</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Code:* </label>
                <input id="txtCode" type="text" class="required" value="<%=survey.Code %>" disabled="disabled"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Text:* </label>
                <input id="txtText" type="text" class="required" value="<%=survey.Text %>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Number:* </label>
                <input id="txtNumber" type="number" class="required" value="<%=survey.Number %>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Type:* </label>
                <input id="txtType" type="text" class="required" value="<%=survey.Type %>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Order:* </label>
                <input id="txtOrder" type="number" class="required" value="<%=survey.Order %>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 ">
                <%if (Convert.ToBoolean(survey.Required)) {%>
                    <input id="txtRequired" type="checkbox" checked="checked" /> 
                <%} else {%>
                    <input id="txtRequired" type="checkbox" /> 
                <%} %>
                <label> Required:* </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12">
                <%if (Convert.ToBoolean(survey.IsDefault)) {%>
                    <input id="txtDefault" type="checkbox" checked="checked" /> 
                <%} else {%>
                    <input id="txtDefault" type="checkbox" /> 
                <%} %>
                <label>Is Default:* </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Step:* </label>
                <input id="txtStep" type="number" class="required" value="<%=survey.step %>"/>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <button class="btn btn-primary" onclick="Submit()">Submit</button>
                <button class="btn btn-danger" onclick="Cancel()">Cancel</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(4);
        });

        function Cancel() {
            location.href = "/cp-survey";
        }

        function Submit() {
            //var code = $('#txtCode').val();
            var text = $('#txtText').val();
            var number = $('#txtNumber').val();

            var type = $('#txtType').val();
            var order = $('#txtOrder').val();
            var required = $('#txtRequired').prop('checked');
            var isDefault = $('#txtDefault').prop('checked');
            var step = $('#txtStep').val();

            // check exception
            CheckFieldEmpty('page');
            var error = "";
            error += CheckFieldError();

            if (error != "") {
                alertify.alert('Error', error);
            } else {
                ShowLoading();
                $.post(LOCAL_DO_URL + 'survey/edit-survey.aspx', {
                    surveyID: <%=survey.ID %>,

                    //code: code,
                    text: text,
                    number: number,

                    type: type,
                    order: order,
                    required: required,
                    isDefault: isDefault,
                    step: step,

                }, function (data) {
                    HideLoading();

                    if (data != 1) {
                        alertify.alert('Error', data);
                    } else {
                        alertify.success('Successful!');
                        location.href = "/cp-survey";
                    }
                });
            }
        }
    </script>
</asp:Content>

