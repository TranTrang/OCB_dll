﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_survey_EditSurvey : BaseAdminPage
{
    public SurveyTBx survey = new SurveyTBx();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        int surveyID = Convert.ToInt32(Page.RouteData.Values["id"]);

        SurveyManager SM = new SurveyManager();
        survey = SM.GetByID(surveyID);
    }
}