﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_province_EditProvince : BaseAdminDo
{
    protected ProvinceTBx province;
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        try
        {
            int id = Convert.ToInt32(Page.RouteData.Values["provinceID"]);
            ProvinceManager PM = new ProvinceManager();
            province = PM.GetByID(id);

        }
        catch (Exception ex)
        {
            Response.Write(ex);
        }
    }
}