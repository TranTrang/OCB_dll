﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="Demo.aspx.cs" Inherits="cp_page_demo_Demo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- jQuery Mask -->
    <script src="/cp/plugins/jquery-mask/jquery.mask.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">DEMO</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h4>Basic Elements</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5>Buttons</h5>
            </div>
            <div class="col-md-12">
                <button class="btn btn-default">Default</button>
                <button class="btn btn-primary">Primary</button>
                <button class="btn btn-info">Info</button>
                <button class="btn btn-danger">Danger</button>
                <button class="btn btn-warning">Warning</button>
                <button class="btn btn-black">Black</button>
                <button class="btn btn-blue">Blue</button>
                <button class="btn btn-pink">Pink</button>
                <button class="btn btn-green">Green</button>
                <button class="btn btn-mint">Mint</button>
                <button class="btn btn-orange">Orange</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5>Inputs</h5>
            </div>
            <div class="col-md-6 form">
                <label>Regular</label>
                <input type="text" placeholder="Regular" />
            </div>
            <div class="col-md-6 form has-floating-label">
                <div class="floating-label-container">
                    <input type="text" name="floating" />                    
                    <label for="floating">Floating label</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h4>Plugins</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5>jQuery Mask</h5>
            </div>
            <div class="col-md-6 form">
                <input type="text" id="masked-input" />
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            SetTabActive(1);

            $('#masked-input').mask("00/00/0000", {placeholder: "__/__/____"});
        });
    </script>
</asp:Content>

