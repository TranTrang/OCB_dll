﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="AddBranch.aspx.cs" Inherits="cp_page_branch_AddBranch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page" class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-branch" class="breadcrumbs__element">Branch</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Add Branch</span></li>
                </ul>
            </div>
        </div>
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">ADD NEW BRANCH</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Province:* </label>
                <select id="txtProvince" class="required">
                    <%foreach (var province in listProvince)
                        {%>
                    <option value="<%=province.ID %>"><%=province.Name %></option>
                    <%} %>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Name VN:* </label>
                <input id="txtNameVN" type="text" class="required" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Name EN:* </label>
                <input id="txtNameEN" type="text" class="required" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Code:* </label>
                <input id="txtCode" type="text" class="required" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Desc:* </label>
                <input id="txtDesc" type="text" class="required" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <button class="btn btn-primary" onclick="Submit()">Submit</button>
                <button class="btn btn-danger" onclick="Cancel()">Cancel</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(8);
        });

        function Cancel() {
            location.href = "/cp-branch";
        }

        function Submit() {
            var nameVN = $('#txtNameVN').val();
            var nameEN = $('#txtNameEN').val();
            var code = $('#txtCode').val();
            var province = $("#txtProvince").val();
            var desc = $('#txtDesc').val();

            // check exception
            CheckFieldEmpty('page');
            var error = "";
            error += CheckFieldError();

            if (error != "") {
                alertify.alert('Error', error);
            } else {
                ShowLoading();
                $.post(LOCAL_DO_URL + 'branch/add-branch.aspx', {
                    nameVN: nameVN,
                    nameEN: nameEN,
                    code: code,
                    province: province,
                    desc: desc
                }, function (data) {
                    HideLoading();
                    if (data == 0) {
                        alertify.error('Branch is existed');
                    } else if (data == 1) {
                        alertify.success('Successful!');
                        location.href = "/cp-branch";
                    } else {
                        alertify.alert('Error', data);
                    }
                });
            }
        }
    </script>
</asp:Content>

