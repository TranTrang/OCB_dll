﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_branch_EditBranch : BaseAdminDo
{
    protected BranchTBx branch;
    protected List<ProvinceTBx> listProvince;
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }
    protected override void ActionAdminDo()
    {
        int id = Convert.ToInt32(Page.RouteData.Values["branchID"]);
        BranchManager BM = new BranchManager();
        ProvinceManager PM = new ProvinceManager();
        listProvince = PM.GetList();
        branch = BM.GetByID(id);
    }
}