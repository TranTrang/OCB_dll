﻿<%@ Page Language="C#"  MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="EditSlider.aspx.cs" Inherits="cp_page_slider_EditSlider" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page" class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 breadcrumbs-container">
                <ul class="breadcrumbs breadcrumbs_type5">
                    <li class="breadcrumbs__item"><a href="/cp-slider" class="breadcrumbs__element">Slider</a></li>
                    <li class="breadcrumbs__item breadcrumbs__item_active"><span class="breadcrumbs__element">Edit Slider</span></li>
                </ul>
            </div>
        </div>
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">EDIT SLIDER</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <label>Name/Link:* </label>
                <input id="txtLink" type="text" class="required" value="<%=slider.Link %>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 ">
                <%if (Convert.ToInt32(slider.Type) == 1) {%>
                    <input id="txtIsVideo" type="checkbox" checked="checked" />
                <%} else {%>
                    <input id="txtIsVideo" type="checkbox" />
                <%} %>
                <label> Is Video: </label>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <button class="btn btn-primary" onclick="Submit()">Submit</button>
                <button class="btn btn-danger" onclick="Cancel()">Cancel</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(13);
        });

        function Cancel() {
            location.href = "/cp-slider";
        }

        function Submit() {
            //var code = $('#txtCode').val();
            var link = $('#txtLink').val();
            var isVideo = $('#txtIsVideo').prop('checked');

            // check exception
            CheckFieldEmpty('page');
            var error = "";
            error += CheckFieldError();

            if (error != "") {
                alertify.alert('Error', error);
            } else {
                ShowLoading();
                $.post(LOCAL_DO_URL + 'slider/edit-slider.aspx', {
                    sliderID: <%=slider.ID %>,
                    link: link,
                    isVideo: isVideo,

                }, function (data) {
                    HideLoading();

                    if (data != 1) {
                        alertify.alert('Error', data);
                    } else {
                        alertify.success('Successful!');
                        location.href = "/cp-slider";
                    }
                });
            }
        }
    </script>
</asp:Content>