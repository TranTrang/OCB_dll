﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_slider_Slider : BaseAdminPage
{
    public List<SliderTBx> listSlider = new List<SliderTBx>();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        SliderManager SM = new SliderManager();
        listSlider = SM.GetList();
        listSlider.Reverse();
    }
}