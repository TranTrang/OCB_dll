﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_page_slider_EditSlider : BaseAdminPage
{
    public SliderTBx slider = new SliderTBx();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitPage();
    }

    protected override void ActionAdminPage()
    {
        int sliderID = Convert.ToInt32(Page.RouteData.Values["sliderID"]);

        SliderManager SM = new SliderManager();
        slider = SM.GetByID(sliderID);
    }
}