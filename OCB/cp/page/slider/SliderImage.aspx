﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeBehind="SliderImage.aspx.cs" Inherits="OCB.cp.page.slider.SliderImage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="container-fluid">
        <form class="row" runat="server">
            <div class="col-sm-6 col-md-6 col-xs-12">
                <h3 class="box-title">Upload Slider Image</h3>
                <img id="sliderImg" runat="server" class="width-auto center-block" style="width:400px"/><br />
            </div>
            <div class="col-sm-6 col-md-6 col-xs-12">
                <h3>
                    <br />
                </h3>
                <asp:FileUpload ID="fileUpload" runat="server" CssClass="btn btn-info" /><br />
                <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btn btn-primary" OnClick="btnUpload_Click" /><br /><br />
                <asp:Label ID="lblStatus" runat="server" Text="Upload status: "></asp:Label>
            </div>
        </form>
        <br />
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 form">
                <button class="btn btn-danger" onclick="Cancel()">Cancel</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(13);
        });

        function Cancel() {
            location.href = "/cp-slider";
        }
    </script>
</asp:Content>
