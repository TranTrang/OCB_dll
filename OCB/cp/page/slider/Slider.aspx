﻿<%@ Page Language="C#" MasterPageFile="~/cp/MasterPage.master" AutoEventWireup="true" CodeFile="Slider.aspx.cs" Inherits="cp_page_slider_Slider" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="row page-title-container">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h4 class="page-title">SLIDER</h4>
            </div>
        </div>
        <div class="row page-control-container">
            <div class="col-md-6 col-lg-6 col-xs-6">
                 <a onclick="CreateSliderImage()" class="btn btn-info">Create new Slider</a>
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-xs-6">
            <div class="search-box-container">
                <%--<input type="text" placeholder="Search..." id="txtSearchInput" />
                <span><i class="fas fa-search"></i></span>--%>
            </div>
        </div>
        </div>
        <div class="row" id="table">
            <%if (listSlider.Count == 0) { %>
                <div class="col-md-12 col-xs-12 col-lg-12"><p>No result found.</p></div>
            <%} else { %>
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <div class="pagination-table" id="paging-table">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Video URL</th>
                                    <th>Image Name</th>
                                    <th width="150px">Image</th>
                                    <th width="120px">Is Video</th>
                                    <th width="100px">Publish</th>
                                    <th align="center" width="250px">Action</th>
                                </tr>
                            </thead>
                            <tbody class="pagination-body">
                                <%foreach (var slider in listSlider) {%>
                                    <tr>
                                        <td><%=slider.ID %></td>
                                        <td><%=slider.Link %></td>
                                        <td><%=slider.Name %></td>
                                        <td> 
                                            <%if (Convert.ToInt32(slider.Type) == 0) {%> 
                                                <%if (System.IO.File.Exists(Server.MapPath("~/upload/slider/" + slider.Name))) {%>
                                                    <img src="/upload/slider/<%=slider.Name %>" class="center-block" />
                                                <%} else { %>
                                                    <img src="/images/index_v2/slider-demo.png" class="center-block" />
                                                <%} %>
                                            <%}else {%> Not Image <%} %>
                                        </td> 
                                        <td><input id="cbIsVideo" type="checkbox" disabled="disabled" <%if (Convert.ToInt32(slider.Type) == 1) {%>checked="checked" <%}%>></td>
                                        <td><input id="cbPublish" type="checkbox" onclick="TooglePublish(<%=slider.ID %>, this)" <%if (Convert.ToBoolean(slider.IsPublished)) {%>checked="checked" <%}%>></td>
                                        <td align="right">
                                            <%if (Convert.ToInt32(slider.Type) == 0) {%><a href="/cp-slider-image/sliderID-<%=slider.ID %>" class="btn btn-info">Image</a> <%} %> 
                                            <a href="/cp-edit-slider/sliderID-<%=slider.ID %>" class="btn btn-primary">Edit URL</a>
                                            <button class="btn btn-danger" onclick="Delete(<%=slider.ID %>)">Delete</button>
                                        </td>
                                    </tr>
                                <%} %>
                            </tbody>
                        </table>
                        <div id="pagination"></div>
                    </div>
                </div>
            <%} %>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            SetTabActive(13);

            Pagination(50, 'paging-table');
        });

        function CreateSliderImage() {
            alertify.confirm('Create a new Slider?', function () {
                ShowLoading();

                $.post(LOCAL_DO_URL + 'slider/add-slider.aspx', {

                }, function (data) {
                    HideLoading();

                    if (data == 1) {
                        alertify.success('Successful!');
                        location.reload();
                    } else {
                        alertify.alert(data);
                    }
                });
            });
        }

        function TooglePublish(id, cbID) {
            var isPublish = $(cbID).prop('checked')

            var strWarning = "";
            if (isPublish) {
                strWarning = "Are you sure you want to PUBLISH this Slider?";
            }
            else {
                strWarning = "Are you sure you want to UNPUBLISH this Slider?";
            }

            alertify.confirm(strWarning, function () {
                ShowLoading();
                $.post(LOCAL_DO_URL + 'slider/toogle-publish-slider.aspx', {
                    sliderID: id,
                    isPublish: isPublish,

                }, function (data) {
                    HideLoading();

                    if (data == 1) {
                        alertify.success('Successful!');
                    } else {
                        alertify.alert(data);

                        if (isPublish) {
                            $(cbID).prop('checked', false);
                        } else {
                            $(cbID).prop('checked', true);
                        }
                    }
                });
            }, function () { // player select cancel
                if (isPublish) {
                    $(cbID).prop('checked', false);
                } else {
                    $(cbID).prop('checked', true);
                }
            });
        }

        function Delete(id) {
            alertify.confirm('Are you sure you want to DELETE this?', function () {
                ShowLoading();

                $.post(LOCAL_DO_URL + 'slider/delete-slider.aspx', {
                    sliderID: id,

                }, function (data) {
                    HideLoading();

                    if (data == 1) {
                        alertify.success('Deleted!');

                        // refresh page
                        location.reload();
                    } else {
                        alertify.alert(data);
                    }
                });
            });
        }
    </script>
</asp:Content>


