﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.cp.page.slider
{
    public partial class SliderImage : BaseAdminPage
    {
        SliderManager SM = new SliderManager();
        public SliderTBx slider = new SliderTBx();

        protected void Page_Load(object sender, EventArgs e)
        {
            InitPage();
        }

        protected override void ActionAdminPage()
        {
            int sliderID = Convert.ToInt32(Page.RouteData.Values["sliderID"]);
            slider = SM.GetByID(sliderID);

            if (System.IO.File.Exists(Server.MapPath("~/upload/slider/" + slider.Name)))
            {
                sliderImg.Src = "/upload/slider/" + slider.Name + "?date=" + DateTime.UtcNow;
            }
            else
            {
                sliderImg.Src = "/images/index_v2/slider-demo.png";
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (fileUpload.HasFile)
            {
                try
                {
                    // ==================== Check exception ==================== //
                    // wrong image format
                    if (fileUpload.PostedFile.ContentType != "image/jpeg" && fileUpload.PostedFile.ContentType != "image/png")
                    {
                        lblStatus.Text = "Upload status: Only accept JPEG or PNG file.";
                        return;
                    }
                    // file size is too large
                    if (fileUpload.PostedFile.ContentLength > 3072000)
                    {
                        lblStatus.Text = "Upload status: The file has to be less than 3MB.";
                        return;
                    }
                    // ==================== Check exception ==================== //

                    //get extension name of filename
                    string extension = System.IO.Path.GetExtension(fileUpload.PostedFile.FileName);
                    string name = "slider_" + slider.ID.ToString() + extension;

                    // save image
                    fileUpload.SaveAs(Server.MapPath("~/upload/slider/" + name));
                    slider.Name = name;
                    SM.Save();

                    //// create thumbnail
                    //string thumbnail_name = "user_" + userID + "_thumbnail" + extension;
                    //UTIL.CreateThumbnail(300, Server.MapPath("~/upload/user/" + name), Server.MapPath("~/upload/user/" + thumbnail_name));

                    // show image
                    sliderImg.Src = "/upload/slider/" + name + "?date=" + DateTime.Now;
                    lblStatus.Text = "Upload status: File uploaded!";
                }
                catch (Exception ex)
                {
                    lblStatus.Text = "Upload status: The file could not be uploaded. The following error occurred: " + ex.Message;
                }
            }
        }
    }
}