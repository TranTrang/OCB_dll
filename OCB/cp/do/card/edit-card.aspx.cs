﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_card_edit_card : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int cardID = Convert.ToInt32(Request["cardID"]);

        string name = Request["name"];
        string type = Request["type"];

        string creditLimit = Request["creditLimit"];
        string chargebackCardFee = Request["chargebackCardFee"];
        string dayTradingLimit = Request["dayTradingLimit"];

        string desc = Request.Unvalidated["desc"];
        string utilities = Request.Unvalidated["utilities"];
        string features = Request.Unvalidated["features"];
        string highlights = Request.Unvalidated["highlights"];

        CardManager CM = new CardManager();
        CardTBx card = CM.GetByID(cardID);

        if (card.ID == 0)
        {
            Response.Write("Card not exist!");
            return;
        }

        card.Name = name;
        card.Type = type;

        card.CreditLimit = creditLimit;
        card.ChargebackCardFee = chargebackCardFee;
        card.DayTradingLimit = dayTradingLimit;

        card.Description = desc;
        card.Utilities = utilities;
        card.Features = features;
        card.Highlights = highlights;

        CM.Save();

        Response.Write(1);
    }
}