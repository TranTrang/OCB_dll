﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_card_edit_card_survey : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int cardID = Convert.ToInt32(Request["cardID"]);
        string strSurvey = Request["strSurvey"];

        CardSurveyManager CSM = new CardSurveyManager();

        // remove old data
        List<CardSurveyTBx> listOldCardSurvey = CSM.GetListByCardID(cardID);
        if (listOldCardSurvey.Count > 0)
        {
            CSM.Delete(listOldCardSurvey);
        }

        // add new
        List<CardSurveyTBx> listNewCardSurvey = new List<CardSurveyTBx>();
        string[] arrSurvey = strSurvey.Split('@');
        for (int i = 0; i < arrSurvey.Length; i++)
        {
            CardSurveyTBx newCardSurvey = new CardSurveyTBx();
            newCardSurvey.SurveyID = Convert.ToInt32(arrSurvey[i]);
            newCardSurvey.CardID = cardID;
            newCardSurvey.IsDeleted = false;

            listNewCardSurvey.Add(newCardSurvey);
        }

        CSM.Add(listNewCardSurvey);

        Response.Write(1);
    }
}