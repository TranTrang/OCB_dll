﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_card_card_image_edit_card_image : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int cardImageID = Convert.ToInt32(Request["cardImageID"]);
        string name = Request["name"];

        CardImageManager CIM = new CardImageManager();
        CardImageTBx cardImage = CIM.GetByID(cardImageID);
        if (cardImage.ID == 0)
        {
            Response.Write("Card image not exist!");
            return;
        }

        cardImage.Name = name;
        CIM.Save();

        Response.Write(1);
    }
}