﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_do_card_card_image_add_card_image : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int cardID = Convert.ToInt32(Request["cardID"]);

        CardImageManager CIM = new CardImageManager();
        CardImageTBx cardImage = new CardImageTBx();

        cardImage.CardID = cardID;
        cardImage.IsDeleted = false;
        CIM.Add(cardImage);

        if (cardImage.ID != 0)
        {
            cardImage.Name = "card_" + cardID.ToString() + "_" + cardImage.ID.ToString();
            CIM.Save();
        }

        Response.Write(1);
    }
}