﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_do_card_add_card : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        string name = Request["name"];
        string type = Request["type"];
        
        string creditLimit = Request["creditLimit"];
        string chargebackCardFee = Request["chargebackCardFee"];
        string dayTradingLimit = Request["dayTradingLimit"];

        string desc = Request.Unvalidated["desc"];
        string utilities = Request.Unvalidated["utilities"];
        string features = Request.Unvalidated["features"];
        string highlights = Request.Unvalidated["highlights"];

        CardManager CM = new CardManager();
        CardTBx card = new CardTBx();

        card.Name = name;
        card.Type = type;

        card.CreditLimit = creditLimit;
        card.ChargebackCardFee = chargebackCardFee;
        card.DayTradingLimit = dayTradingLimit;

        card.Description = desc;
        card.Utilities = utilities;
        card.Features = features;
        card.Highlights = highlights;

        card.CreatedDate = DateTime.UtcNow;
        card.IsDeleted = false;
        card.IsPublish = false;

        CM.Add(card);

        Response.Write(1);
    }
}