﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_card_toogle_publish_card : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int cardID = Convert.ToInt32(Request["cardID"]);
        bool isPublish = Convert.ToBoolean(Request["isPublish"]);

        CardManager CM = new CardManager();
        CardTBx card = CM.GetByID(cardID);

        if (card.ID == 0)
        {
            Response.Write("Card not exist!");
            return;
        }

        card.IsPublish = isPublish;
        CM.Save();

        Response.Write(1);
    }
}