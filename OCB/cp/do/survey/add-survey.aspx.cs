﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_survey_add_survey : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        string code = Request["code"];
        string text = Request["text"];
        double number = Convert.ToDouble(Request["number"]);

        string type = Request["type"];
        int order = Convert.ToInt32(Request["order"]);
        bool required = Convert.ToBoolean(Request["required"]);
        bool isDefault = Convert.ToBoolean(Request["isDefault"]);
        int step = Convert.ToInt32(Request["step"]);

        SurveyManager SM = new SurveyManager();
        SurveyTBx survey = new SurveyTBx();
        survey.Code = code;
        survey.Text = text;
        survey.Number = number;

        survey.Type = type;
        survey.Order = order;
        survey.Required = required;
        survey.IsDefault = isDefault;
        survey.step = step;
        survey.IsPublish = false;

        survey.IsDeleted = false;

        SM.Add(survey);

        if (survey.ID > 0)
        {
            SurveyItemValueManager SIVM = new SurveyItemValueManager();
            SurveyItemValueTBx itemValue = new SurveyItemValueTBx();

            itemValue.SurveyID = survey.ID;
            itemValue.Value = "default";
            itemValue.OrderBy = 1;
            itemValue.T24ID = "0";
            itemValue.Group = "0";
            itemValue.IsDeleted = false;

            SIVM.Add(itemValue);
        }

        Response.Write(1);
    }
}