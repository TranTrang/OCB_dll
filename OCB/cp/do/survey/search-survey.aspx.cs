﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_do_survey_search_survey : BaseAdminDo
{
    public List<SurveyTBx> listSurvey = new List<SurveyTBx>();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        string strSearch = Request["strSearch"];

        SurveyManager SM = new SurveyManager();
        listSurvey = SM.SearchText(strSearch);

        listSurvey = listSurvey.OrderBy(e => e.step).ThenBy(e => e.ID).ToList();
    }
}