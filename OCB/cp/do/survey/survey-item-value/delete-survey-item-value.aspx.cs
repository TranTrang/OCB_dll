﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_survey_survey_item_value_delete_survey_item_value : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int surveyItemValueID = Convert.ToInt32(Request["surveyItemValueID"]);

        SurveyItemValueManager SIVM = new SurveyItemValueManager();
        SurveyItemValueTBx itemValue = SIVM.GetByID(surveyItemValueID);

        if (itemValue.ID == 0)
        {
            Response.Write("Item Value not exist!");
            return;
        }

        itemValue.IsDeleted = true;
        SIVM.Save();

        Response.Write(1);
    }
}