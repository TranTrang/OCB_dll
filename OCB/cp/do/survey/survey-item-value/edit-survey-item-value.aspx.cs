﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_survey_survey_item_value_edit_survey_item_value : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int surveyItemValueID = Convert.ToInt32(Request["surveyItemValueID"]);
        string value = Request["value"];
        int orderBy = Convert.ToInt32(Request["orderBy"]);
        string T24ID = Request["T24ID"];
        string group = Request["group"];

        SurveyItemValueManager SIVM = new SurveyItemValueManager();
        SurveyItemValueTBx itemValue = SIVM.GetByID(surveyItemValueID);

        if (itemValue.ID == 0)
        {
            Response.Write("Item Value not exist!!");
            return;
        }

        itemValue.Value = value;
        itemValue.OrderBy = orderBy;
        itemValue.T24ID = T24ID;
        itemValue.Group = group;

        SIVM.Save();

        Response.Write(1);
    }
}