﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_survey_survey_item_value_add_survey_item_value : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int surveyID = Convert.ToInt32(Request["surveyID"]);
        string value = Request["value"];
        int orderBy = Convert.ToInt32(Request["orderBy"]);
        string T24ID = Request["T24ID"];
        string group = Request["group"];

        SurveyItemValueManager SIVM = new SurveyItemValueManager();
        SurveyItemValueTBx itemValue = new SurveyItemValueTBx();

        itemValue.SurveyID = surveyID;
        itemValue.Value = value;
        itemValue.OrderBy = orderBy;
        itemValue.T24ID = T24ID;
        itemValue.Group = group;

        itemValue.IsDeleted = false;

        SIVM.Add(itemValue);

        Response.Write(1);
    }
}