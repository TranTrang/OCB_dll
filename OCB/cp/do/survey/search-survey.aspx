﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="search-survey.aspx.cs" Inherits="cp_do_survey_search_survey" %>

<%if (listSurvey.Count == 0) { %>
    <div class="col-md-12 col-xs-12 col-lg-12"><p>No result found.</p></div>
<%} else { %>
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="pagination-table" id="paging-table">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Code</th>
                        <th>Question</th>
                        <th>Input Type</th>
                        <th>Type</th>
                        <th>Order</th>
                        <th>Required</th>
                        <th>Default</th>
                        <th>Step</th>
                        <th>Publish</th>
                        <th>Item Value</th>
                        <th>Manage</th>
                    </tr>
                </thead>
                <tbody class="pagination-body">
                    <%foreach (var survey in listSurvey) {%>
                        <tr>
                            <td><%=survey.ID %></td>
                            <td><%=survey.Code %></td>
                            <td><%=survey.Text %></td>
                            <td><%=survey.Number %></td>
                            <td><%=survey.Type %></td>
                            <td><%=survey.Order %></td>
                            <td>
                                <%if (Convert.ToBoolean(survey.Required)) {%>
                                    <input type="checkbox" checked="checked" disabled="disabled">
                                <%} else {%>
                                    <input type="checkbox" disabled="disabled">
                                <%} %>
                            </td>
                            <td>
                                <%if (Convert.ToBoolean(survey.IsDefault)) {%>
                                    <input type="checkbox" checked="checked" disabled="disabled">
                                <%} else {%>
                                    <input type="checkbox" disabled="disabled">
                                <%} %>
                            </td>
                            <td><%=survey.step %></td>
                            <td><input id="cbPublish" type="checkbox" onclick="TooglePublish(<%=survey.ID %>, this)" <%if (Convert.ToBoolean(survey.IsPublish)) {%>checked="checked" <%}%>></td>
                            <td><a href="/cp-survey-item-value/surveyID-<%=survey.ID %>" class="btn btn-info">Item Value</a></td>
                            <td>
                                <a href="/cp-edit-survey/id-<%=survey.ID %>" class="btn btn-primary">Edit</a>
                                <%-- <button class="btn btn-danger" onclick="Delete(<%=survey.ID %>)">Delete</button>--%>
                            </td>
                        </tr>
                    <%} %>
                </tbody>
            </table>
            <div id="pagination"></div>
        </div>
    </div>
<%} %>

<script>
    $(document).ready(function () {
        Pagination(50, 'paging-table');
    });

    function TooglePublish(id, cbID) {
        var isPublish = $(cbID).prop('checked')

        var strWarning = "";
        if (isPublish) {
            strWarning = "Are you sure you want to PUBLISH this survey?";
        }
        else {
            strWarning = "Are you sure you want to UNPUBLISH this survey?";
        }

        alertify.confirm(strWarning, function () {
            ShowLoading();
            $.post(LOCAL_DO_URL + 'survey/toogle-publish-survey.aspx', {
                surveyID: id,
                isPublish: isPublish,

            }, function (data) {
                HideLoading();

                if (data == 1) {
                    alertify.success('Successful!');
                } else {
                    alertify.alert(data);

                    if (isPublish) {
                        $(cbID).prop('checked', false);
                    } else {
                        $(cbID).prop('checked', true);
                    }
                }
            });
        }, function () { // player select cancel
            if (isPublish) {
                $(cbID).prop('checked', false);
            } else {
                $(cbID).prop('checked', true);
            }
        });
    }

    function Delete(id) {
        alertify.confirm('Are you sure you want to DELETE this?', function () {
            ShowLoading();

            $.post(LOCAL_DO_URL + 'survey/delete-survey.aspx', {
                surveyID: id,

            }, function (data) {
                HideLoading();

                if (data == 1) {
                    alertify.success('Deleted!');

                    // refresh page
                    location.reload();
                } else {
                    alertify.alert(data);
                }
            });
        });
    }
</script>