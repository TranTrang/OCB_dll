﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_survey_edit_survey : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int surveyID = Convert.ToInt32(Request["surveyID"]);

        //string code = Request["code"];
        string text = Request["text"];
        double number = Convert.ToDouble(Request["number"]);

        string type = Request["type"];
        int order = Convert.ToInt32(Request["order"]);
        bool required = Convert.ToBoolean(Request["required"]);
        bool isDefault = Convert.ToBoolean(Request["isDefault"]);
        int step = Convert.ToInt32(Request["step"]);


        SurveyManager SM = new SurveyManager();
        SurveyTBx survey = SM.GetByID(surveyID);

        // =============================================================
        if (survey.ID == 0)
        {
            Response.Write("Survey not exist!");
            return;
        }

        // =============================================================
        // 
        //survey.Code = code;
        survey.Text = text;
        survey.Number = number;

        survey.Type = type;
        survey.Order = order;
        survey.Required = required;
        survey.IsDefault = isDefault;
        survey.step = step;

        SM.Save();

        Response.Write(1);
    }
}