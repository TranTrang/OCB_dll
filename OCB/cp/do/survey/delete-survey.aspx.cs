﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_survey_delete_survey : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int surveyID = Convert.ToInt32(Request["surveyID"]);

        SurveyManager SM = new SurveyManager();
        SurveyTBx survey = SM.GetByID(surveyID);

        if (survey.ID == 0)
        {
            Response.Write("Survey not exist!");
            return;
        }

        survey.IsDeleted = true;
        SM.Save();

        Response.Write(1);
    }
}