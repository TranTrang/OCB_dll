﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_town_edit_town : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        try
        {
            int id = Convert.ToInt32(Request["id"]);
            TownManager TM = new TownManager();
            TownTBx town = TM.GetByID(id);
            town.Name = Request["name"];
            town.TownID = Request["code"];
            town.ProvinceID = Convert.ToInt32(Request["province"]);
            town.OrderBy = Convert.ToInt32(Request["order"]);

            TM.Save();
            Response.Write(1);
        }
        catch (Exception ex)
        {
            Response.Write(ex);
        }
    }
}