﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_town_toogle_publish_town : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int townID = Convert.ToInt32(Request["townID"]);
        bool isPublish = Convert.ToBoolean(Request["isPublish"]);

        TownManager TM = new TownManager();
        TownTBx town = TM.GetByID(townID);

        if (town.ID == 0)
        {
            Response.Write("Town not exist!");
            return;
        }

        town.IsPublish = isPublish;
        TM.Save();

        Response.Write(1);
    }
}