﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="search-town.aspx.cs" Inherits="cp_do_town_search_town" %>


<%if (listTown.Count == 0)
    { %>
<div class="col-md-12 col-xs-12 col-lg-12">
    <p>No result found.</p>
</div>
<%}
    else
    { %>
<div class="col-md-12 col-xs-12 col-lg-12">
    <div class="pagination-table" id="paging-table">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Province</th>
                    <th>Name</th>
                    <th>TownID</th>
                    <th>Order</th>
                    <th>Publish</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="pagination-body">
                <%foreach (var town in listTown)
                    {%>
                <tr>
                    <td><%=town.ID %></td>
                    <td><%=town.ProvinceTBx.Name%></td>
                    <td><%=town.Name %></td>
                    <td><%=town.TownID %></td>
                    <td><%=town.OrderBy %></td>
                    <td><input id="cbPublish" type="checkbox" onclick="TooglePublish(<%=town.ID %>, this)" <%if (Convert.ToBoolean(town.IsPublish)) {%>checked="checked" <%}%>></td>
                    <td>
                        <a href="/cp-edit-town-townID-<%=town.ID %>" class="btn btn-primary">Edit</a>
                        <%--<button class="btn btn-danger" onclick="Delete(<%=town.ID %>)">Delete</button></td>--%>
                </tr>
                <%} %>
            </tbody>
        </table>
        <div id="pagination"></div>
    </div>
</div>
<%} %>

<script>
    $(document).ready(function () {
        Pagination(50, 'paging-table');
    });

    function TooglePublish(id, cbID) {
        var isPublish = $(cbID).prop('checked')

        var strWarning = "";
        if (isPublish) {
            strWarning = "Are you sure you want to PUBLISH this town?";
        }
        else {
            strWarning = "Are you sure you want to UNPUBLISH this town?";
        }

        alertify.confirm(strWarning, function () {
            ShowLoading();
            $.post(LOCAL_DO_URL + 'town/toogle-publish-town.aspx', {
                townID: id,
                isPublish: isPublish,

            }, function (data) {
                HideLoading();

                if (data == 1) {
                    alertify.success('Successful!');
                } else {
                    alertify.alert(data);

                    if (isPublish) {
                        $(cbID).prop('checked', false);
                    } else {
                        $(cbID).prop('checked', true);
                    }
                }
            });
        }, function () { // player select cancel
            if (isPublish) {
                $(cbID).prop('checked', false);
            } else {
                $(cbID).prop('checked', true);
            }
        });
    }

</script>

