﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_do_town_search_town : BaseAdminDo
{
    protected List<TownTBx> listTown;
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }
    protected override void ActionAdminDo()
    {
        string strSearch = Request["strSearch"];

        TownManager TM = new TownManager();
        listTown = TM.SearchProvince(strSearch);
        listTown.Reverse();
    }
}