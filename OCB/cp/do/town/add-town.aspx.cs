﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_town_add_town : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        try
        {
            string name = Request["name"];
            string townID = Request["code"];
            int provinceID = Convert.ToInt32(Request["province"]);
            int order = Convert.ToInt32(Request["order"]);

            TownManager TM = new TownManager();
            if (TM.GetByNameAndTownID(name, townID).ID == 0)
            {
                TownTBx town = new TownTBx();
                town.IsDeleted = false;
                town.Name = name;
                town.TownID = townID;
                town.ProvinceID = provinceID;
                town.IsPublish = false;
                town.OrderBy = order;

                TM.Add(town);
                Response.Write(1);
            }
            else
            {
                Response.Write("Provine already exist!");
            }

        }
        catch (Exception ex)
        {
            Response.Write(ex);
        }
    }

}