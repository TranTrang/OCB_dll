﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_slider_delete_slider : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int sliderID = Convert.ToInt32(Request["sliderID"]);

        SliderManager SM = new SliderManager();
        SliderTBx slider = SM.GetByID(sliderID);
        if (slider.ID == 0)
        {
            Response.Write("Slider not exist!");
            return;
        }

        slider.IsDeleted = true;
        SM.Save();

        Response.Write(1);
    }
}