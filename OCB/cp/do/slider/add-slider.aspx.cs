﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_slider_add_slider : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        SliderManager SM = new SliderManager();
        SliderTBx slider = new SliderTBx();

        slider.Type = 0;
        slider.IsPublished = false;
        slider.IsDeleted = false;
        SM.Add(slider);

        Response.Write(1);
    }
}