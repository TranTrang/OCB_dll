﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_slider_edit_slider : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int sliderID = Convert.ToInt32(Request["sliderID"]);
        string link = Request["link"];
        bool isVideo = Convert.ToBoolean(Request["isVideo"]);

        SliderManager SM = new SliderManager();
        SliderTBx slider = SM.GetByID(sliderID);
        if (slider.ID == 0)
        {
            Response.Write("Slider not exist!");
            return;
        }

        slider.Link = link;
        if (isVideo) slider.Type = 1;
        else slider.Type = 0;
        SM.Save();

        Response.Write(1);
    }
}