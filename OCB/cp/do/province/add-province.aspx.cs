﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_province_add_province : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        try
        {
            string name = Request["name"];
            string T24ID = Request["code"];
            int order = Convert.ToInt32(Request["order"]);

            ProvinceManager PM = new ProvinceManager();
            if (PM.GetByNameAndT24ID(name, T24ID).ID == 0)
            {
                ProvinceTBx province = new ProvinceTBx();
                province.IsDeleted = false;
                province.Name = name;
                province.T24ID = T24ID;
                province.OrderBy = order;
                province.IsPublish = false;

                PM.Add(province);
                Response.Write(1);
            }
            else
            {
                Response.Write("Provine already exist!");
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

}