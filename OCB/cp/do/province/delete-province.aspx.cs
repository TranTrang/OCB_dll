﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_province_delete_province : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }
    protected override void ActionAdminDo()
    {
        int ID = Convert.ToInt32(Request["id"]);
        ProvinceManager PM = new ProvinceManager();
        ProvinceTBx province = PM.GetByID(ID);
        if (province.ID != 0)
        {
            province.IsDeleted = true;
            PM.Save();
            Response.Write(1);
        }
        else
        {
            Response.Write("Not found");
        }
    }
}