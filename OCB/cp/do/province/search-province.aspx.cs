﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_do_province_search_province : BaseAdminDo
{
    public List<ProvinceTBx> listProvince = new List<ProvinceTBx>();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        string strSearch = Request["strSearch"];


        ProvinceManager PM = new ProvinceManager();
        listProvince = PM.SearchProvince(strSearch);
        listProvince.Reverse();
    }
}