﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="search-province.aspx.cs" Inherits="cp_do_province_search_province" %>


<%if (listProvince.Count == 0)
    { %>
<div class="col-md-12 col-xs-12 col-lg-12">
    <p>No result found.</p>
</div>
<%}
    else
    { %>
<div class="col-md-12 col-xs-12 col-lg-12">
    <div class="pagination-table" id="paging-table">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>T24.ID</th>
                    <th>Name</th>   
                    <th>Order</th>   
                    <th>Publish</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="pagination-body">
                <%foreach (var province in listProvince)
                    {%>
                <tr>
                    <td><%=province.ID %></td>
                    <td><%=province.T24ID%></td>
                    <td><%=province.Name %></td>
                    <td><%=province.OrderBy %></td>
                    <td><input id="cbPublish" type="checkbox" onclick="TooglePublish(<%=province.ID %>, this)" <%if (Convert.ToBoolean(province.IsPublish)) {%>checked="checked" <%}%>></td>
                    <td>
                        <a href="/cp-edit-province-provinceID-<%=province.ID %>" class="btn btn-primary">Edit</a>
                       <%-- <button class="btn btn-danger" onclick="Delete(<%=province.ID %>)">Delete</button></td>--%>
                </tr>
                <%} %>
            </tbody>
        </table>
        <div id="pagination"></div>
    </div>
</div>
<%} %>

<script>
    $(document).ready(function () {
        Pagination(50, 'paging-table');
    });

    function TooglePublish(id, cbID) {
        var isPublish = $(cbID).prop('checked')

        var strWarning = "";
        if (isPublish) {
            strWarning = "Are you sure you want to PUBLISH this province?";
        }
        else {
            strWarning = "Are you sure you want to UNPUBLISH this province?";
        }

        alertify.confirm(strWarning, function () {
            ShowLoading();
            $.post(LOCAL_DO_URL + 'province/toogle-publish-province.aspx', {
                provinceID: id,
                isPublish: isPublish,

            }, function (data) {
                HideLoading();

                if (data == 1) {
                    alertify.success('Successful!');
                } else {
                    alertify.alert(data);

                    if (isPublish) {
                        $(cbID).prop('checked', false);
                    } else {
                        $(cbID).prop('checked', true);
                    }
                }
            });
        }, function () { // player select cancel
            if (isPublish) {
                $(cbID).prop('checked', false);
            } else {
                $(cbID).prop('checked', true);
            }
        });
    }

</script>
