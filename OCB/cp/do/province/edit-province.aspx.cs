﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_province_edit_province : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }
    protected override void ActionAdminDo()
    {
        try
        {
            int id = Convert.ToInt32(Request["id"]);
            string name = Request["name"];
            string code = Request["code"];
            int order = Convert.ToInt32(Request["order"]);

            ProvinceManager PM = new ProvinceManager();
            ProvinceTBx province = PM.GetByID(id);
            if (province.ID == 0)
            {
                Response.Write(0);
            }
            else
            {
                province.Name = name;
                province.T24ID = code;
                province.OrderBy = order;

                PM.Save();
                Response.Write(1);

            }
        }
        catch (Exception ex)
        {
            Response.Write(ex);
        }
    }

}