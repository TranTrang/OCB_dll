﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_province_toogle_publish_province : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int provinceID = Convert.ToInt32(Request["provinceID"]);
        bool isPublish = Convert.ToBoolean(Request["isPublish"]);

        ProvinceManager PM = new ProvinceManager();
        ProvinceTBx province = PM.GetByID(provinceID);

        if (province.ID == 0)
        {
            Response.Write("Province not exist!");
            return;
        }

        province.IsPublish = isPublish;
        PM.Save();

        Response.Write(1);
    }
}