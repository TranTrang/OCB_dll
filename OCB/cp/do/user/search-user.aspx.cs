﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_do_user_search_user : BaseAdminDo
{
    public List<string> listRegisterStatus = new List<string>();
    public List<UserTBx> listUser = new List<UserTBx>();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        string strSearch = Request["strSearch"];

        // list register status
        listRegisterStatus.Add("NỘP ĐƠN ĐĂNG KÝ");
        listRegisterStatus.Add("NỘP GIẤY TỜ");
        listRegisterStatus.Add("THẨM ĐỊNH");
        listRegisterStatus.Add("ĐÃ GỬI THẺ ĐI");
        listRegisterStatus.Add("ĐÃ NHẬN THẺ");

        // get list user
        UserManager UM = new UserManager();
        listUser = UM.SearchPhone(strSearch);
        listUser.Reverse();
    }

    public string GetUserEmail(int userID)
    {
        UserManager UM = new UserManager();
        return UM.GetByID(userID).Email;
    }
}