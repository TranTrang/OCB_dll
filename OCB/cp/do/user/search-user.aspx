﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="search-user.aspx.cs" Inherits="cp_do_user_search_user" %>

<%if (listUser.Count == 0) { %>
    <div class="col-md-12 col-xs-12 col-lg-12">
        <p>No result found.</p>
    </div>
<%} else { %>
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="pagination-table" id="paging-table">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Full name</th>
                        <th>Register Status</th>
                        <th>Detail</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody class="pagination-body">
                    <%foreach (var user in listUser) {%>
                        <tr>
                            <td><%=user.ID %></td>
                            <td><%=user.Email %></td>
                            <td><%=user.Phone %></td>
                            <td><%=user.FullName %></td>
                            <td>
                                <select class="form-control" name="class" onchange="ChangeRegisterStatus(this, <%=user.ID %>, '<%=user.RegisterStatus %>')">
                                    <%for (int i = 0; i < 5; i++) {%>
                                        <option value="<%=i.ToString() %>" <%if (user.RegisterStatus == i)
                                            {%>selected="selected"<%} %>><%=listRegisterStatus[i] %></option>
			                        <%} %>
                                </select>
                            </td>
                            <td width="210px">
                                <a href="/cp-user-survey/userID-<%=user.ID %>" class="btn btn-info">Survey</a>
                                <a href="/cp-user-profile-image/userID-<%=user.ID %>" class="btn btn-primary">Profile Image</a>
                            </td>
                            <td width="80px"><button class="btn btn-danger" onclick="Delete(<%=user.ID %>)">Delete</button></td>
                        </tr>
                    <%} %>
                </tbody>
            </table>
            <div id="pagination"></div>
        </div>
    </div>
<%} %>

<script>
    $(document).ready(function () {
        Pagination(50, 'paging-table');
    });

    function ChangeRegisterStatus(ddStatus, userID, oldStatus) {
        alertify.confirm("Confirm", "Are you sure you want to change Register Status for this user?", function () {
            var registerStatus = $(ddStatus).val();

            $.post(LOCAL_DO_URL + "user/set-user-register-status.aspx", {
                userID: userID,
                registerStatus: registerStatus,

            }, function (data) {
                if (data == 1) {
                    alertify.success("Change Status Successful!");
                    if (registerStatus == 0 || registerStatus == 4) return;

                    // send mail notice change status
                    alertify.confirm("Confirm", "Shall we need to send an email to notice that user??", function () {
                        $.post(LOCAL_DO_URL + "email/send-mail-change-status.aspx", {
                            userID: userID,

                        }, function (data) {
                            if (data == 1) {
                                alertify.success("Send Mail Successful!");
                            } else {
                                alertify.alert("Error", data);
                            }
                        });
                    }, function () { });// cancel: not send mail
                    // \end send mail status
                } else {
                    alertify.alert("Error", data);
                }
            });
        }, function () {
            $(ddStatus).val(oldStatus);
        });
    }
</script>