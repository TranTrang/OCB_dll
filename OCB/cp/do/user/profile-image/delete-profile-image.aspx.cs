﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_user_profile_image_delete_profile_image : BaseAdminDo
{ 
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int profileImageID = Convert.ToInt32(Request["profileImageID"]);

        UserProfileImageManager UPIM = new UserProfileImageManager();
        UserProfileImageTBx profile = UPIM.GetByID(profileImageID);

        if (profile.ID == 0)
        {
            Response.Write("Profile not exist!");
            return;
        }

        profile.IsDeleted = true;
        UPIM.Save();

        Response.Write(1);
    }
}