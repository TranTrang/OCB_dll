﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_user_set_user_register_status : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int userID = Convert.ToInt32(Request["userID"]);
        int registerStatus = Convert.ToInt32(Request["registerStatus"]);

        UserManager UM = new UserManager();
        UserTBx user = UM.GetByID(userID);

        if (user.ID == 0)
        {
            Response.Write("User not exist!");
            return;
        }

        user.RegisterStatus = registerStatus;
        UM.Save();

        Response.Write(1);
    }
}