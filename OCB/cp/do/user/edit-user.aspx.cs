﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_user_edit_user : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int id = Convert.ToInt32(Request["id"]);
        string fullName = Request["fullName"];
        string email = Request["email"];
        string phone = Request["phone"];

        string shortDesc = Request["shortDesc"];
        string desc = Request.Unvalidated["desc"];

        UserManager UM = new UserManager();
        UserTBx user = UM.GetByID(id);

        // kiểm tra nếu user không tồn tại
        if (user.ID == 0)
        {
            Response.Write("User doesn't exist. Please try again.");
            return;
        }

        //user.Fullname = fullName;
        //user.Email = email;
        //user.Phone = phone;

        //user.ShortDesc = shortDesc;
        //user.Desc = desc;

        // lưu thay đổi vào database
        UM.Save();
        
        Response.Write(1);
    }
}