﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_user_delete_user : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int id = Convert.ToInt32(Request["id"]);

        UserManager UM = new UserManager();
        UserTBx user = UM.GetByID(id);

        // disable record
        user.Status = -1;

        UM.Save();

        Response.Write(1);
    }
}