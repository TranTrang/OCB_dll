﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_user_add_user : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        string fullName = Request["fullName"];
        string email = Request["email"];
        string phone = Request["phone"];

        string shortDesc = Request["shortDesc"];

        // Content của tinymce có chứa thẻ html nên cần Unvalidated
        string desc = Request.Unvalidated["desc"];

        // ================================================================
        // thêm dữ liệu vào database

        UserManager UM = new UserManager();
        UserTBx user = new UserTBx();

        // Không cần gán ID vì ID được set tự tăng trong database
        //user.Fullname = fullName;
        //user.Email = email;
        //user.Phone = phone;

        //user.ShortDesc = shortDesc;
        //user.Desc = desc;

        // Status = 1: available, Status = -1: unavailable
        user.Status = 1;

        UM.Add(user);

        // Upload default image        
        string defaultAvatarPath = Server.MapPath("~/cp/images/" + "default_avatar.jpg");
        string userAvatarPath = Server.MapPath("~/upload/user/" + "user_" + user.ID + ".jpg");
        System.IO.File.Copy(defaultAvatarPath, userAvatarPath);

        // trả về 1 khi thành công
        Response.Write(1);
    }
}