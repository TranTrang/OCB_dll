﻿using Newtonsoft.Json;
using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_email_send_mail_change_status : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        try
        {
            // get user data
            int userID = Convert.ToInt32(Request["userID"]);
            UserManager UM = new UserManager();
            UserTBx user = UM.GetByID(userID);
            if (user.ID == 0)
            {
                Response.Write("User not exist!");
                return;
            }

            // get mail base data
            int emailIndex = 0;
            if (user.RegisterStatus == 1) emailIndex = 1;
            else if (user.RegisterStatus == 2) emailIndex = 2;
            else if (user.RegisterStatus == 3) emailIndex = 3;
            else
            {
                Response.Write("Can't send email, wrong user status: " + user.RegisterStatus.ToString());
                return;
            }

            PageManager PM = new PageManager();
            EmailData emailData = JsonConvert.DeserializeObject<EmailData>(PM.GetByID(emailIndex).Metadata);

            // =========================================================================
            // init email data
            string url = "http://emk.tuscript.com/ocb/send-mail.aspx";

            List<string> listMail = new List<string>();
            listMail.Add(user.Email);

            // test mail
            listMail.Add("trantrangit.cntp@gmail.com");
            listMail.Add("pgbao13@gmail.com");

            string jsonListMail = JsonConvert.SerializeObject(listMail);

            // replace variable to body 
            emailData.body = emailData.body.Replace("val_UserFullName", user.FullName); //val_UserFullName
            emailData.body = emailData.body.Replace("val_UserEmail", user.Email); //val_UserEmail
            emailData.body = emailData.body.Replace("val_UserPhone", user.Phone); //val_UserPhone

            // serialize mail data
            string jsonEmailData = JsonConvert.SerializeObject(emailData);

            // =================================================================================================================
            // request http to emk server
            var request = (HttpWebRequest)WebRequest.Create(url);

            string postData = "";
            postData += "jsonListMail=" + jsonListMail;
            postData += "&" + "jsonEmailData=" + jsonEmailData;
            var data = Encoding.UTF8.GetBytes(postData);

            // post to emk
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            // response
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            if (responseString == "1")
            {
                Response.Write(1);
            }
            else
            {
                Response.Write(responseString);
            }

            // =================================================================================================================
            // =================================================================================================================
            // =================================================================================================================
            // emk

            //Response.AppendHeader("Access-Control-Allow-Origin", "*");

            //string jsonListMail = Request["jsonListMail"];
            //string jsonEmailData = Request.Unvalidated["jsonEmailData"];
            //Response.Write(jsonEmailData);

            //List<string> listEmail = new List<string>();
            //listEmail = JsonConvert.DeserializeObject<List<string>>(jsonListMail);
            //EmailData emailData = JsonConvert.DeserializeObject<EmailData>(jsonEmailData);

            //// exception
            //string error = "";
            //if (string.IsNullOrEmpty(jsonListMail)) error += "list email not exist! <br />";
            //if (listEmail.Count == 0) error += "list email not exist! <br />";
            //if (string.IsNullOrEmpty(emailData.subject)) error += "mailSubject not exist! <br />";
            //if (string.IsNullOrEmpty(emailData.body)) error += "mailBody not exist! <br />";
            //if (string.IsNullOrEmpty(emailData.sendFromEmail)) error += "fromMail not exist! <br />";
            //if (string.IsNullOrEmpty(emailData.title)) error += "title not exist! <br />";

            //if (error != "")
            //{
            //    Response.Write(error);
            //    return;
            //}

            //// send mail
            //UTIL_SENDMAIL.SendMail("", listEmail, false, emailData.subject, emailData.body, emailData.sendFromEmail, emailData.title);

            //Response.Write(1);
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}