﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.cp.branch
{
    public partial class search_branch : BaseAdminDo
    {
        protected List<BranchTBx> listBranch;
        protected void Page_Load(object sender, EventArgs e)
        {
            InitDo();
        }
        protected override void ActionAdminDo()
        {
            string strSearch = Request["strSearch"];
            BranchManager BM = new BranchManager();
            listBranch = BM.SearchBranch(strSearch);
            listBranch.Reverse();
        }
    }
}