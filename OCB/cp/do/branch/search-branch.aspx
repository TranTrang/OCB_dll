﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="search-branch.aspx.cs" Inherits="OCB.cp.branch.search_branch" %>

<%if (listBranch.Count == 0)
    { %>
<div class="col-md-12 col-xs-12 col-lg-12">
    <p>No result found.</p>
</div>
<%}
    else
    { %>
<div class="col-md-12 col-xs-12 col-lg-12">
    <div class="pagination-table" id="paging-table">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Province</th>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="pagination-body">
                <%foreach (var branch in listBranch)
                    {%>
                <tr>
                    <td><%=branch.ID %></td>
                    <td><%=branch.ProvinceTBx.Name%></td>
                    <td><%=branch.NameVN %></td>
                    <td><%=branch.ItemCode %></td>
                    <td>
                        <a href="/cp-edit-branch-branchID-<%=branch.ID %>" class="btn btn-primary">Edit</a>
                        <button class="btn btn-danger" onclick="Delete(<%=branch.ID %>)">Delete</button></td>
                </tr>
                <%} %>
            </tbody>
        </table>
        <div id="pagination"></div>
    </div>
</div>
<%} %>

<script>
    $(document).ready(function () {
        Pagination(50, 'paging-table');
    });

</script>
