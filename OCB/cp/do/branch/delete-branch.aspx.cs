﻿using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.cp.branch
{
    public partial class delete_branch : BaseAdminDo
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            InitDo();
        }
        protected override void ActionAdminDo()
        {
            try
            {
                int id = Convert.ToInt32(Request["id"]);
                BranchManager BM = new BranchManager();
                BM.GetByID(id).IsDeleted = true;
                BM.Save();
                Response.Write(1);
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}