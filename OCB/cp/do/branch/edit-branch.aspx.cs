﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.cp.branch
{
    public partial class edit_branch : BaseAdminDo
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            InitDo();
        }
        protected override void ActionAdminDo()
        {
            try
            {
                int id = Convert.ToInt32(Request["id"]);
                BranchManager BM = new BranchManager();
                BranchTBx branch = BM.GetByID(id);
                branch.NameVN = Request["nameVN"];
                branch.NameEN = Request["nameEN"];
                branch.ItemCode = Request["code"];
                branch.ItemDesc = Request["desc"];
                branch.ProvinceID = Convert.ToInt32(Request["province"]);
                branch.IsDeleted = false;
                BM.Save();
                Response.Write(1);
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }
    }
}