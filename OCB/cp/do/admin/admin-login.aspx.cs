﻿using OCB.AppCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.cp.admin
{
    public partial class admin_login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string email = Request["email"];
            string password = Request["password"];

            if (Admin.CheckLogin(email, password))
            {
                CPSession cpSession = new CPSession();
                cpSession.sessionType = SessionType.admin;
                cpSession.admin = new Admin(email, password);
                Session["cp-session"] = cpSession;

                Response.Write(1);
            }
            else if (false)
            {
                // Check user login if necessary
                // cpSession.userID = user.ID;
            }
            else
            {
                Response.Write("Wrong email or password. Please try again!");
                return;
            }
        }
    }
}