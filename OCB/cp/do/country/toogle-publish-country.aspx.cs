﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_country_toogle_publish_country : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int countryID = Convert.ToInt32(Request["countryID"]);
        bool isPublish = Convert.ToBoolean(Request["isPublish"]);

        CountryManager CM = new CountryManager();
        CountryTBx country = CM.GetByID(countryID);

        if (country.ID == 0)
        {
            Response.Write("Country not exist!");
            return;
        }

        country.IsPublish = isPublish;
        CM.Save();

        Response.Write(1);
    }
}