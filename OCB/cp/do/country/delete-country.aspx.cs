﻿using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_country_delete_country : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        try
        {
            int id = Convert.ToInt32(Request["id"]);
            CountryManager CM = new CountryManager();
            CM.GetByID(id).IsDeleted = true;
            CM.Save();
            Response.Write(1);

        }
        catch (Exception ex)
        {
            Response.Write(ex);
        }
    }
}