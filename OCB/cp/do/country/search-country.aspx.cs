﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OCB.AppCode;
using OCB.AppCode.Manager;
public partial class cp_do_country_search_country : BaseAdminDo
{        
    public List<CountryTBx> listCountry = new List<CountryTBx>();

    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        string strSearch = Request["strSearch"];

        CountryManager UM = new CountryManager();
        listCountry = UM.SearchCountry(strSearch);
        listCountry.Reverse();
    }
}