﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_country_add_country : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        try
        {
            string name = Request["name"];
            string code = Request["code"];
            CountryManager CM = new CountryManager();
            if (CM.IsExist(name, code))
            {
                Response.Write("Provine already exist!");
            }
            else
            {
                CountryTBx country = new CountryTBx();
                country.IsDeleted = false;
                country.Name = name;
                country.CountryCode = code;
                country.IsPublish = false;
                country.OrderBy = Convert.ToInt32(Request["order"]);

                CM.Add(country);
                Response.Write(1);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex);
        }
    }
}
