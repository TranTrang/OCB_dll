﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="search-country.aspx.cs" Inherits="cp_do_country_search_country" %>

<%if (listCountry.Count == 0)
    { %>
<div class="col-md-12 col-xs-12 col-lg-12">
    <p>No result found.</p>
</div>
<%}
    else
    { %>
<div class="col-md-12 col-xs-12 col-lg-12">
    <div class="pagination-table" id="paging-table">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Country Code</th>
                    <th>Name</th>
                    <th>Order</th>
                    <th>Publish</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody class="pagination-body">
                <%foreach (var country in listCountry){%>
                    <tr>
                        <td><%=country.ID %></td>
                        <td><%=country.CountryCode%></td>
                        <td><%=country.Name %></td>
                        <td><%=country.OrderBy %></td>
                        <td><input id="cbPublish" type="checkbox" onclick="TooglePublish(<%=country.ID %>, this)" <%if (Convert.ToBoolean(country.IsPublish)) {%>checked="checked" <%}%>></td>
                        <td>
                            <a href="/cp-edit-country-countryID-<%=country.ID %>" class="btn btn-primary">Edit</a>
                            <button class="btn btn-danger" onclick="Delete(<%=country.ID %>)">Delete</button></td>
                    </tr>
                <%} %>
            </tbody>
        </table>
        <div id="pagination"></div>
    </div>
</div>
<%} %>

<script>
    $(document).ready(function () {
        Pagination(50, 'paging-table');
    });

    function TooglePublish(id, cbID) {
        var isPublish = $(cbID).prop('checked')

        var strWarning = "";
        if (isPublish) {
            strWarning = "Are you sure you want to PUBLISH this country?";
        }
        else {
            strWarning = "Are you sure you want to UNPUBLISH this country?";
        }

        alertify.confirm(strWarning, function () {
            ShowLoading();
            $.post(LOCAL_DO_URL + 'country/toogle-publish-country.aspx', {
                countryID: id,
                isPublish: isPublish,

            }, function (data) {
                HideLoading();

                if (data == 1) {
                    alertify.success('Successful!');
                } else {
                    alertify.alert(data);

                    if (isPublish) {
                        $(cbID).prop('checked', false);
                    } else {
                        $(cbID).prop('checked', true);
                    }
                }
            });
        }, function () { // player select cancel
            if (isPublish) {
                $(cbID).prop('checked', false);
            } else {
                $(cbID).prop('checked', true);
            }
        });
    }
</script>
