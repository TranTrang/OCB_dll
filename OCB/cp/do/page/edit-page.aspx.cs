﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_page_edit_page : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int pageID = Convert.ToInt32(Request["pageID"]);

        string name = Request["name"];
        string website = Request["website"];
        int position = Convert.ToInt32(Request["position"]);
        string metadata = Request.Unvalidated["metadata"];

        PageManager PM = new PageManager();
        PageTBx page = PM.GetByID(pageID);

        if (page.ID == 0)
        {
            Response.Write("Page not exist!");
            return;
        }

        page.Name = name;
        page.Website = website;
        page.Postion = position;
        page.Metadata = metadata;

        PM.Save();

        Response.Write(1);
    }
}