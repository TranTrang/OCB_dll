﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_page_customize_email : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int pageID = Convert.ToInt32(Request["pageID"]);
        string sendFromEmail = Request["sendFromEmail"];
        string title = Request["title"];
        string subject = Request["subject"];
        string body = Request.Unvalidated["body"];

        EmailData emailData = new EmailData();
        emailData.sendFromEmail = sendFromEmail;
        emailData.title = title;
        emailData.subject = subject;
        emailData.body = body;

        PageManager PM = new PageManager();
        PageTBx page = PM.GetByID(pageID);
        if (page.ID == 0)
        {
            Response.Write("Page Email data not exist!");
            return;
        }

        page.Metadata = Newtonsoft.Json.JsonConvert.SerializeObject(emailData);
        PM.Save();

        Response.Write(1);
    }
}