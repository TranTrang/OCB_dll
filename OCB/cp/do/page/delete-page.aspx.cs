﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_page_delete_page : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        int pageID = Convert.ToInt32(Request["pageID"]);

        PageManager PM = new PageManager();
        PageTBx page = PM.GetByID(pageID);
        if (page.ID == 0)
        {
            Response.Write("Page not exist!");
            return;
        }

        page.Isdelete = true;
        PM.Save();

        Response.Write(1);
    }
}