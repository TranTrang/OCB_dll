﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cp_do_page_add_page : BaseAdminDo
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitDo();
    }

    protected override void ActionAdminDo()
    {
        string name = Request["name"];
        string website = Request["website"];
        int position = Convert.ToInt32(Request["position"]);
        string metadata = Request.Unvalidated["metadata"];

        PageManager PM = new PageManager();
        PageTBx page = new PageTBx();

        page.Name = name;
        page.Website = website;
        page.Postion = position;
        page.Metadata = metadata;

        page.Isdelete = false;
        page.Ispublic = true;

        PM.Add(page);

        Response.Write(1);
    }
}