﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="OCB.cp.cp_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>Simple Admin Control | Admin Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="/cp/images/favicon.png" rel="shortcut icon" />

    <!-- jQuery -->
    <script src="/cp/plugins/jquery/jquery-2.2.4.min.js"></script>

    <!-- Bootstrap -->
    <link href="/cp/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script src="/cp/plugins/bootstrap/js/bootstrap.min.js"></script>

    <!-- Fontawesome -->
    <script src="/cp/plugins/fontawesome/fontawesome-all.min.js"></script>

    <!-- AlertifyJS -->
    <link href="/cp/plugins/alertify/css/alertify.min.css" rel="stylesheet" />
    <link href="/cp/plugins/alertify/css/themes/default.min.css" rel="stylesheet" />
    <script src="/cp/plugins/alertify/alertify.min.js"></script>

    <!-- Loading CSS -->
    <link href="/cp/css/loading.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link href="/cp/stylesheet/main.min.css" rel="stylesheet" />

    <!-- Custom JS -->
    <script src="/cp/js/effect.js"></script>
    <script src="/cp/js/check-exception.js"></script>
    <script src="/cp/js/handle.js"></script>
</head>
<body>
    <div id="login-container">
        <div class="overlay"></div>
        <div id="login-form" class="login-panel">
            <div class="row form">
                <div class="col-sm-12">
                    <h3 class="text-center header-title">ADMIN LOGIN</h3>
                </div>
            </div>
            <div class="row form">
                <div class="col-sm-12">
                    <label for="floating">Email</label>
                    <input type="text" name="floating" class="required" id="txtLoginEmail" />
                </div>
            </div>
            <div class="row form">
                <div class="col-sm-12">
                    <label for="floating">Password</label>
                    <input type="password" name="floating" class="required" id="txtLoginPassword" />
                </div>
            </div>
            <div class="row form">
                <div class="col-md-12">
                    <a href="#" onclick="ShowForgetPasswordForm()">Forgot Password?</a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button class="btn btn-primary full-width" onclick="Login()">Submit</button>
                </div>
            </div>
        </div>
        <div id="forgot-password-form" class="login-panel">
            <div class="row form">
                <div class="col-sm-12">
                    <h3 class="text-center header-title">RECOVER Password</h3>
                </div>
            </div>
            <div class="row form">
                <div class="col-sm-12">
                    <p>Enter your Email and instructions will be sent to you!</p>
                </div>
            </div>
            <div class="row form">
                <div class="col-sm-12 has-floating-label">
                    <div class="floating-label-container">
                        <input type="text" name="floating" class="required" id="txtFPEmail" />
                        <label for="floating">Email</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button class="btn btn-primary full-width" onclick="ResetPassword()">Reset</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Loading -->
    <div id="loading-screen">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
    <!-- ./Loading -->

    <script>
        $(function () {
            $('#txtLoginEmail').keydown(function (e) {
                if (e.keyCode == 13) {
                    Login();
                }
            });

            $('#txtLoginPassword').keydown(function (e) {
                if (e.keyCode == 13) {
                    Login();
                }
            });
        });

        function ShowForgetPasswordForm() {
            $('#login-form').hide();
            $('#forgot-password-form').show();
        }

        function Login() {
            var email = $('#txtLoginEmail').val();
            var password = $('#txtLoginPassword').val();

            CheckFieldEmpty('login-form');
            var error = CheckFieldError();

            if (error != "") {
                alertify.alert('Error', error);
            } else {
                ShowLoading();

                $.post(LOCAL_DO_URL + 'admin/admin-login.aspx', {
                    email: email,
                    password: password,
                }, function (data) {
                    HideLoading();

                    if (data != 1) {
                        alertify.alert('Error', data);
                    } else {
                        location.href = '/cp-default';
                    }
                });
            }
        }
    </script>
</body>
</html>
