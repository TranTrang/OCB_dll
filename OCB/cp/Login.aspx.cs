﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace OCB.cp
{
    public partial class cp_Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["cp-session"] != null)
            {
                Response.Redirect("/cp-default");
            }
        }
    }
}