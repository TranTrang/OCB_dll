﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="UploadImageUser.aspx.cs" Inherits="OCB.UploadImageUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div style="padding-top: 60px" onload="UploadFileUser()">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-13 col-md-12 text-right">
                    <h4><i class="fas fa-info-circle"></i>Hướng dẫn</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-lg-12">
                    <div class="col-sm-9 col-md-9 col-xs-12">
                        <h3 class="box-title"><%=surveyUserFile.Code %></h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <% if (surveyUserFile.Type == "S")
                        {
                            listOption = SIM.GetListOption(surveyUserFile.ID);
                    %>

                    <select class="sl-item survey require_<%=surveyUserFile.Required %>" id="slx_<%=surveyUserFile.ID%>" data-error="<%=surveyUserFile.Code %>">
                        <option value=""><%=surveyUserFile.Code%></option>
                        <%

                            for (int k = 0; k < listOption.Count; k++)
                            {%>
                        <option value="<%=listOption[k].Value%>"><%=listOption[k].Value%></option>
                        <% }
                            }
                        %>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-5">
                    <form class="row" runat="server">
                        <!-- Up Hinh Anh -->
                        <iframe class="iframeUpload" src="" frameborder="0" style="width: 100%; height: 500px"></iframe>

                        <!-- ./ Up Hinh Anh -->
                    </form>
                </div>

            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <a href="/card-medium-1/step6" class="btn btn-success">Hoàn thành Upload hồ sơ
                    </a>
                </div>

            </div>
            <hr />

        </div>

    </div>
    <script>

        $(function () {


            $("#slx_87").change(function () {

                fileUser = $("#slx_87 option:selected").val();

                localStorage.setItem("fileUser", fileUser);

                var linkImage = "/api/uploadImage.aspx?phone=" + localStorage.getItem("phoneuser") + "&type=" + localStorage.getItem("fileUser");
                $(".iframeUpload").attr("src", linkImage);

            });
            $("#slx_88").change(function () {
                fileAddress = $("#slx_88 option:selected").val();
                localStorage.setItem("fileAddress", fileAddress);
                var linkImage = "/api/uploadImage.aspx?phone=" + localStorage.getItem("phoneuser") + "&type=" + localStorage.getItem("fileAddress");
                $(".iframeUpload").attr("src", linkImage);
            });
            $("#slx_89").change(function () {
                fileAccount = $("#slx_89 option:selected").val();
                localStorage.setItem("fileAccount", fileAccount);
                var linkImage = "/api/uploadImage.aspx?phone=" + localStorage.getItem("phoneuser") + "&type=" + localStorage.getItem("fileAccount");
                $(".iframeUpload").attr("src", linkImage);
            });

        });


    </script>
</asp:Content>
