﻿$(function () {
    if (!String.prototype.trim) {
        (function () {
            // Make sure we trim BOM and NBSP

            var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
            String.prototype.trim = function () {
                return this.replace(rtrim, '');
            };


        })();
    }

    [].slice.call(document.querySelectorAll('input.input-field')).forEach(function (inputEl) {
        // in case the input is already filled..
        if (inputEl.value.trim() !== '') {
            classie.add(inputEl.parentNode, 'input-filled');
        }

        // events:
        inputEl.addEventListener('focus', onInputFocus);
        inputEl.addEventListener('blur', onInputBlur);
    });

    function onInputFocus(ev) {
        classie.add(ev.target.parentNode, 'input-filled');
    }

    function onInputBlur(ev) {
        if (ev.target.value.trim() === '') {
            classie.remove(ev.target.parentNode, 'input-filled');
        }
    }

    $("#slx_17").parent().closest('div').addClass("col-xs-4 col-sm-4 col-md-4");
    $("#slx_49").parent().closest('div').addClass("col-xs-4 col-sm-4 col-md-4");
    $("#slx_16").parent().closest('div').addClass("col-xs-4 col-sm-4 col-md-4");
    $("#slx_7 ").parent().closest('div').before('<div class="col-xs-12 col-sm-12 col-md-12" style="margin-top:20px;color:#0e8c45"><label>ĐỊA CHỈ LIÊN HỆ <span class="color-red">(*)</span><small style="color:#aca69f"> Địa chỉ bạn muốn hẹn gặp</small><label></div>');
    $("#chk_57").parent().parent().closest('div').before('<div class="col-xs-12 col-sm-12 col-md-12" style="margin-top:20px;color:#0e8c45"><label>ĐỊA CHỈ THƯỜNG TRÚ  <span class="color-red">(*)</span><small style="color:#aca69f"> Địa chỉ theo hộ khẩu</small><label> <br/><span style="color:#0e8c45">Địa chỉ thường trú có giống địa chỉ liên hệ ?</span>  </div>');
    $("#slx_115").parent().closest('div').before('<div class="col-xs-12 col-sm-12 col-md-12" style="margin-left:5px;margin-top:20px;color:#0e8c45"><label>ĐỊA CHỈ HIỆN TẠI <span class="color-red">(*)</span><small style="color:#aca69f"> Địa chỉ nhà bạn đang sống</small><label></div>');
})