﻿// v1.0.1 20182307
var SENDMAIL_URL = "http://emk.tuscript.com/ocb/";
var errRegister = "";
var provinceid = "";
var fileUser = "";
var fileAddress = "";
var fileAccount = "";
var step = "";
var cardid = 3;
var cardstep = 1;
var pathname = "";
var lengthpath = "";
var path = "";
var length = "";
var stringUser = "";
var dict = [];
var JsonUserData = "";
var UserData = [];
var dctt = "";
var dcht = "";
var flag = 0;
$(function () {
  
    cardstep = window.location.pathname.split('/')[2];
    step = "step" + $("#ipstep").val();


    pathname = window.location.pathname;
    lengthpath = pathname.length;
    path = pathname.substr(parseInt(lengthpath) - 6);
    length = $(".survey").length;
    if (path == "/step1") {
        $("#number1").addClass("active-number").siblings().removeClass("active-number");
        var form = $(".box-form");
        var offset = form.offset();
        setTimeout(function () {

            $("html, body").animate({ scrollTop: offset.top - 100 }, 1000);

        }, 2000);//call every 2000 miliseconds

        $("#dtt_5").on("blur", function () {
            CheckAvailablePhone($("#dtt_5").val());
        })

        $("#dtt_6").on("blur", function () {
            CheckAvailableEmail($("#dtt_6").val());
        })
        $("#dtt_14").parent().hide();
        $("#slx_82").parent().hide();
        $("#dtt_14").removeClass("require_True");
        $("#slx_82").removeClass("require_True");

        $(".input-checkbox").click(function () {
            if ($("#chk_12").is(":checked")) {
                $("#chk_12").prop("checked", false);
                $("#dtt_14").removeClass("require_True");
                $("#slx_82").removeClass("require_True");
                $("#dtt_14").parent().hide();
                $("#slx_82").parent().hide();
            }
            else {
                $("#chk_12").prop("checked", true);
                $('#slx_82').parent("").show();
                $('#dtt_14').parent().show();
                $("#dtt_14").addClass("require_True");
                $("#slx_82").addClass("require_True");

            }
        });

       
        loadTextStep(step);
    }
    else if (path == "step2a") {

        $(".input-checkbox-one").click(function () {
          
            if ($("#chk_57").is(":checked")) {
                
                $("#chk_57").prop("checked", true);
                $("#chl_57").prop("checked", false);
                $('#dtt_111').addClass("require_True");
                $('#dtt_111').parent().parent().parent().show();
                $('.slx_58').parent().hide();
                $('.slx_59').parent().hide();

                $('#dtt_60').parent().hide();
                $('#dtt_61').parent().hide();
                $("#dtt_112").val(localStorage.getItem("dctt"));
                $('.slx_58').removeClass("require_True");
                $('.slx_59').removeClass("require_True");
                $('#dtt_60').removeClass("require_True");
                $('#dtt_61').removeClass("require_True");
            }
            else {
             
                $("#chk_57").prop("checked", false);
                $("#chl_57").prop("checked", true);
                $('#dtt_111').removeClass("require_True");
                $('#dtt_111').parent().parent().parent().hide();
                $('.slx_58').parent().hide();
                $('.slx_59').parent().hide();

                $('#dtt_60').parent().hide();
                $('#dtt_61').parent().hide();
                $("#dtt_112").val(localStorage.getItem("dctt"));
                $('.slx_58').removeClass("require_True");
                $('.slx_59').removeClass("require_True");
                $('#dtt_60').removeClass("require_True");
                $('#dtt_61').removeClass("require_True");
            }
        });

        $(".input-checkbox-two").click(function () {

            if ($("#chl_57").is(":checked")) {
                $("#chl_57").prop("checked", true);
                $("#chk_57").prop("checked", false);
                $('.slx_58').parent().show();
                $('.slx_59').parent().show();
                $('#dtt_60').parent().show();
                $('#dtt_61').parent().show();
                $("#dtt_112").val(localStorage.getItem("dctt"));
                $('.slx_58').addClass("require_True");
                $('.slx_59').addClass("require_True");
                $('#dtt_60').addClass("require_True");
                $('#dtt_61').addClass("require_True");
                $('#dtt_111').removeClass("require_True");
                $('#dtt_111').parent().parent().parent().hide();
               

            }
            else {
                $("#chk_57").prop("checked", false);
               
                $('.slx_58').parent().hide();
                $('.slx_59').parent().hide();

                $('#dtt_60').parent().hide();
                $('#dtt_61').parent().hide();
                $("#dtt_112").val(localStorage.getItem("dctt"));
                $('.slx_58').removeClass("require_True");
                $('.slx_59').removeClass("require_True");
                $('#dtt_60').removeClass("require_True");
                $('#dtt_61').removeClass("require_True");
                $('#dtt_111').removeClass("require_True");
                $('#dtt_111').parent().parent().parent().hide();

            }
        });




       
        $("#dtt_111").val(localStorage.getItem("address"));

        $("#number2").addClass("active-number").siblings().removeClass("active-number");
        $("#dtt_57").val(localStorage.getItem("address"));
        var addressNow = localStorage.getItem("addressVal");
        var address = [];

        address = addressNow.split(",");
        $("#dtt_99").val(address[0]);
        $("#dtt_98").val(address[1]);
        $(".slx_97").val(address[2]);
        $(".slx_63").val(address[3]);

        $("#dtt_111").val(localStorage.getItem("address"));
        $("#slx_63 ul").find("[data-id=" + localStorage.getItem("provinceID") + "]").addClass("selected");
        $("#slx_100").parent().hide();
        $("#slx_100").removeClass("require_True");
        LoadTownAddressNow(localStorage.getItem("provinceID"), localStorage.getItem("townID"));
        loadTextStep(step);
       
       

    }
    else if (path == "step2b") {
        $("#number2").addClass("active-number").siblings().removeClass("active-number");

        $("#dtt_63").val(localStorage.getItem("address"));
        $("#dtt_83").val(localStorage.getItem("addressNow"));
        $("#dtt_73").val(localStorage.getItem("monthlyincome"));

        loadTextStep(step);

    }
    else if (path == "/step3") {
        $("#number3").addClass("active-number").siblings().removeClass("active-number");
        $("#dtt_83").val(localStorage.getItem("addressNow"));
        LoadUsesrid();
    }


});


function OpenSelect(input) {
    $(input).parent().find(".select-item").show();
}

function CloseSelect(input) {
    setTimeout(() => {
        $(input).parent().find(".select-item").hide();
    }, 150);
}


function ValidationCardStep(cardid, step) {
    console.log("step = " + step);

    if (step == "step2b") {

        if ($.isNumeric($("#dtt_70").val()) == false) {
            errRegister += "<li>Thời gian làm việc hiện tại nhập số</li>";
            lasterr = i;
        }
        CallFunctionRegister();
    }
    if (step == "step2a") {
        var tinh2 = $("#slx_63 .selected").text();
        var qh2 = $("#slx_97 .selected").text();
        var px2 = $("#dtt_98").val();
        var sn2 = $("#dtt_99").val();

        localStorage.setItem("dcht", sn2 + ", " + px2 + ", " + qh2 + ", " + tinh2);

        if ($.isNumeric($("#dtt_109").val()) == false) {
            errRegister += "<li>Số giấy tờ nhập số</li>";
            $("#dtt_109").focus();
        }
        var dateRange = $("#slx_25 .selected").data("id");
        var monthRange = $("#slx_54 .selected").data("id");
        var yearRange = $("#slx_50 .selected").data("id");

        var day = localStorage.getItem("birdthday");

        var strDay2 = monthRange + "/" + dateRange + "/" + yearRange;


        if (HowManyYear(day, strDay2) < 15) {
            errRegister += "<li> Ngày tháng năm sinh phải cách ngày CMND ít nhất 15 năm</li>";
            $("#slx_25").focus();
        }
        else {
            errRegister = "";
            $("#slx_25").focus();
        }
        CallFunctionRegister();
    } else if (step == "step1") {
        var email = localStorage.getItem("email");
        var phoneuser = localStorage.getItem("phoneuser");
        $.post("/do/register/CheckUser", {
            email: email,
            phoneuser: phoneuser
        }, function (data) {
            HideLoading();
            if (data == 1) {
                alertify.alert("Vui lòng nhập lại", "<li>Số điện thoại đã được đăng ký</li>");
                $("#dtt_5").css("border", "1px solid red");
            } else if (data == 2) {
                alertify.alert("Vui lòng nhập lại", "<li>Email đã được đăng ký</li>");
                $("#dtt_6").css("border", "1px solid red");
            } else {
                $("#dtt_6").css("border", "unset");
                $("#dtt_5").css("border", "unset");
                CallFunctionRegister();
            }
        });
    }

}

function Open() {
    ShowLoading();
    AddressInitLoad();
    var step = $("#ipstep").val();
    var length = $(".survey").length;
    var value = "";
    var lasterr = 0;
    console.log(step);
    if (step == 1) {
        var dateBirthday = $("#slx_17 .selected").data("id");
        var monthBirtday = $("#slx_49 .selected").data("id");
        var yearBirtday = $("#slx_16 .selected").data("id");
        var strDay1 = monthBirtday + "/" + dateBirthday + "/" + yearBirtday;
        var d = new Date();
        var yearNow = d.getMonth() + "/" + d.getDate() + "/" + d.getFullYear();
        localStorage.setItem("birdthday", strDay1);
        if (HowManyYear(strDay1, d) < 18) {
            errRegister += "<li> Khách hàng phải trên 18 tuổi</li>";
            lasterr = i;
        }
    }
    ValidationCardStep(cardid, cardstep);
   
}

function loadTextStep(step) {

    console.log("Bạn đã ở " + step);
    var length = $(".survey").length;
    LoadTown(localStorage.getItem("provinceID"), localStorage.getItem("townID"));
    JSON.parse(localStorage.getItem(step)).forEach(element => {
        var title = $("#slx_" + element.key + " ul").find("[data-id=" + element.value + "]").text();
        if ($("#slx_" + element.key).length > 0) {
            $("#slx_" + element.key + " ul").find("[data-id=" + element.value + "]").addClass("selected");
            $(".slx_" + element.key).val(title);
        } else {
            if (step == "step2") {
                if (localStorage.getItem("chkdctt") == 1) {


                    $("#chk_57").prop("checked", true);
                    $("#chl_57").prop("checked", false);
                    $('#dtt_111').addClass("require_True");
                    $('#dtt_111').parent().parent().parent().show();
                    $('.slx_58').parent().hide();
                    $('.slx_59').parent().hide();

                    $('#dtt_60').parent().hide();
                    $('#dtt_61').parent().hide();
                    $("#dtt_112").val(localStorage.getItem("dctt"));
                    $('.slx_58').removeClass("require_True");
                    $('.slx_59').removeClass("require_True");
                    $('#dtt_60').removeClass("require_True");
                    $('#dtt_61').removeClass("require_True");

                }
                else {
                    $("#chk_57").prop("checked", false);
                    $("#chl_57").prop("checked", true);
                    $('#dtt_111').removeClass("require_True");
                    $('#dtt_111').parent().parent().parent().hide();
                    $('.slx_58').parent().show();
                    $('.slx_59').parent().show();

                    $('#dtt_60').parent().show();
                    $('#dtt_61').parent().show();
                    $("#dtt_112").val(localStorage.getItem("dctt"));
                    $('.slx_58').addClass("require_True");
                    $('.slx_59').addClass("require_True");
                    $('#dtt_60').addClass("require_True");
                    $('#dtt_61').addClass("require_True");
                    $.post("/do/town/loadTown", { provinceid: localStorage.getItem("provinceID") }, function (data) {
                        $("#slx_59").html(data);
                        $("#slx_59 ul").find("[data-id=" + localStorage.getItem("townID") + "]").addClass("selected");
                        $(".slx_59").val($("#slx_59 ul").find("[data-id=" + localStorage.getItem("townID") + "]").text());
                    });
                }
            }
            $("#dtt_" + element.key).val(element.value);
        }

    });


    //if (localStorage.length > 0) {
    //    for (var k = 0; k < localStorage.length; k++) {
    //        if (step == localStorage.key(k)) {
    //            value = localStorage.getItem(localStorage.key(k));
    //            objectStep = JSON.parse(value);
    //            for (var j = 0; j < length; j++) {
    //                v = objectStep[j].value;
    //                key = objectStep[j].key;

    //                if (key == 7) {
    //                    $(".survey").eq(j).val(v);
    //                    tid = objectStep[j + 1].value;

    //                }

    //                if (key == 63) {
    //                    $(".survey").eq(j).val(v);
    //                    tid = objectStep[j + 1].value;
    //                    LoadTownAddressNow(v, tid);
    //                }

    //                $("#dtt_14").remove("require_True");
    //                $("#slx_82").remove("require_True");
    //                if (key == 12) {

    //                    $(".survey").eq(j).val(v);
    //                    if (v == "YES") {
    //                        $('#slx_82').parent().show();
    //                        $('#dtt_94').parent().show();
    //                        $('#dtt_14').parent().show();

    //                        $("#dtt_14").addClass("require_True");
    //                        $("#slx_82").addClass("require_True");

    //                    }
    //                    else {
    //                        $("#dtt_14").remove("require_True");
    //                        $("#slx_82").remove("require_True");

    //                    }
    //                }
    //                if (key == 74) {

    //                    $(".survey").eq(j).val(v);
    //                    if (v == "YES") {
    //                        $('#slx_100').parent().show();
    //                        $("#slx_100").addClass("require_True");


    //                    }
    //                }
    //                else
    //                    $(".survey").eq(j).val(v);
    //            }
    //        }
    //    }

    //}
}

function LoadTown(provinceid, townid) {

    $.post("/do/town/loadTown", { provinceid: provinceid }, function (data) {
        $("#slx_8").html(data);
        $("#slx_8 ul").find("[data-id=" + townid + "]").addClass("selected");
        $(".slx_8").val($("#slx_8 ul").find("[data-id=" + townid + "]").text());
    });

}
function LoadTownAddressNow(provinceid, townid) {
    $.post("/do/town/loadTown", { provinceid: provinceid }, function (data) {
        $("#slx_97").html(data);
        $("#slx_97 ul").find("[data-id=" + townid + "]").addClass("selected");
    });

}
function AddressInitLoad() {
    var step = $("#ipstep").val();
    if (step == 1) {
        address = $("#dtt_10").val() + ", " + $("#slx_8 .selected").text() + ", " + $("#slx_7 .selected").text();
        localStorage.setItem("address", address);
        localStorage.setItem("provinceID", $("#slx_7 .selected").data("id"));
        localStorage.setItem("townID", $("#slx_8 .selected").data("id"));

        addressVal = $("#dtt_10").val() + "," + $("#dtt_9").val() + "," + $("#slx_8 .selected").text() + "," + $("#slx_7 .selected").text();
        localStorage.setItem("addressVal", addressVal);

        phone = $("#dtt_5").val();
        localStorage.setItem("phoneuser", phone);

        email = $("#dtt_6").val();
        localStorage.setItem("email", email);

        monthlyincome = $("#dtt_11").val();
        localStorage.setItem("monthlyincome", monthlyincome);

    }
    addressNow = $("#dtt_99").val() + ", " + $("#dtt_98").val() + ", " + $("#slx_97 .selected").text() + ", " + $("#slx_63 .selected").text();
    localStorage.setItem("addressNow", addressNow);

}
function HowManyYear(from, to) {
    var stempFrom = new Date(from).getTime();
    var stempTo = new Date(to).getTime();

    if (stempTo > stempFrom) {
        var howLong = new Date(stempTo - stempFrom).getFullYear() - 1970;
        return howLong;
    }
    else {
        return -1;
    }
}
function LoadUsesrid() {
    userid = localStorage.getItem("userid");
    $.post("/do/register/infoUserRegister", { userid: userid }, function (data) {

        $(".table-info-resgiter").html(data);
    });
}
function CallFunctionRegister() {
    var step = $("#ipstep").val();

    for (i = 0; i < length; i++) {
        if ($(".survey").eq(i).parent().find(".select-item").length > 0) {
            var idSurvey = $(".survey").eq(i).parent().find(".select-item").attr("id").substr(4);
            value = $("#slx_" + idSurvey + " .selected").data("id");
        } else {
            var id = $(".survey").eq(i).attr("id");
            
            idSurvey = $(".survey").eq(i).attr("id").substr(4);
            value = $("#" + id).val();
            if (step == 2) {
                if ($("#chk_57").is(":checked")) {


                    if (flag == 0) {
                        value = $("#chk_57").val();
                        localStorage.setItem("chkdctt", 1);
                        flag = 1;
                    }
                }
                else {
                    if (flag == 0) {
                        value = $("#chl_57").val();
                        localStorage.setItem("chkdctt",0);
                        flag = 1;
                    }

                }
            }
        }
        if ($(".survey").eq(i).hasClass("require_True") && (value == "" || value == undefined)) {
            errRegister += "<li>" + $(".survey").eq(i).attr("data-error") + "</li>";
            lasterr = i;
        }
        else if ($(".survey").eq(i).hasClass("email_True")) {
            var email = $('.email_True').val();

            if (CheckEmailValid(email.trim()) != "") {
                errRegister += "<li>" + $(".survey").eq(i).attr("data-email") + "</li>";
                lasterr = i;
            }

        }
        else if ($(".survey").eq(i).hasClass("phone_True")) {
            var phone = $('.phone_True').val();

            if (CheckPhoneValid(phone) == false) {
                errRegister += "<li>" + $(".survey").eq(i).attr("data-phone") + "</li>";
                lasterr = i;
            }
        }

        dict.push({
            key: idSurvey,
            value: value,
        });

    }

    JsonUserData = JSON.stringify(dict);
    dict = [];

    $(".survey").attr("style", "");

    if (errRegister != "") {
        HideLoading();
        alertify.alert("Bạn vui lòng nhập đầy đủ thông tin", "<ul>" + errRegister + "</ul>");
        errRegister = "";
        $(".survey").eq(lasterr).attr("style", "border:1px inset red;margin-top:-1px");
        $(".survey").eq(lasterr).focus();
    }
    else {
        HideLoading();
        var step_next = parseInt(step) + 1;
        var step_prev = parseInt(step) + 1;
        if (step < 4 && step_next < 4) {

            localStorage.setItem('step' + step, JsonUserData);
            if (step == 1) {
                location.href = "step2a";
            }
            else if (step == 2) {
                location.href = "step2b";
            }

            else {
                location.href = "step" + step_next;
            }

        }

        if (step == 3) {

            localStorage.setItem('step' + step, JsonUserData);
            $(".btn-open").attr("disabled", "disabled");
            var item = "";
            for (var i = 0; i < localStorage.length; i++) {

                if (localStorage.key(i) == "step1" || localStorage.key(i) == "step2" || localStorage.key(i) == "step3") {

                    item += localStorage.getItem(localStorage.key(i)) + "*";
                    localStorage.setItem('user' + step, item);
                }
            }


            email = localStorage.getItem("email");
            dctt = localStorage.getItem("dctt");
            dcht = localStorage.getItem("dcht");
            $.post("/do/register/register-card", { item: item, step: step }, function (data) {

                if (data != 2 && data != 3) {

                    localStorage.setItem("userid", data);
                    location.href = "step3";

                    //$.post("/do/email/send-mail", { email: email }, function (result) {
                    //    if (result == 1) {

                    //        $(".btn-open").removeAttr("disabled");
                    //        //localStorage.clear();

                    //        alertify.alert("Bạn đã đăng ký thành công", function () {
                    //            location.href = "step3";
                    //        });
                    //    }

                    //    else {
                    //        alertify.alert("Thông báo ", "Bạn đã đăng ký không thành công. Vui lòng thử lại");
                    //    }
                    //});
                }
                else if (data == 2) {
                    alertify.alert("Thông báo", "Số điện thoại này đã được sử dụng");
                }
                else if (data == 3) {
                    alertify.alert("Thông báo", "Email này đã được sử dụng");
                }
                else {
                    alertify.alert("Thông báo ", "Bạn đã đăng ký không thành công. Vui lòng thử lại");
                }
            });
        }

        if (step == 4) {
            $.post("/api/get-OTP", {
                phone: localStorage.getItem("phoneuser"),
                userID: "test1",
                branchCode: "VN0010037"
            }, function (data) {
                data = JSON.parse(data);
                if (data.transactionErrorCode == 90) {
                    if (data.operationStatus == "EXECUTED") {
                        console.log("sent");
                        localStorage.setItem("authId", data.authId);
                        location.href = "step3a";
                    } else {
                        console.log("error");
                    }
                } else {
                    console.log("error");
                }
            })
        }
    }
}

function Select(input, type) {
    $(input).parent().parent().parent().find("input").val($(input).text());
    $(input).parent().find("li").removeClass("selected");
    $(input).addClass("selected");

    if (type == 7) {
        $(".slx_8").val("Quận/Huyện");
        $.post("/do/town/loadTown", {
            provinceid: $("#slx_7 .selected").data("id")
        }, function (data) {
            console.log(data);
            $("#slx_8").html(data);
        })
    } else if (type == 2) {
        fileUser = $("#slx_87 .selected").data("id");
        localStorage.setItem("fileUser", fileUser);
    } else if (type == 3) {
        fileAddress = $("#slx_88 .selected").data("id");
        localStorage.setItem("fileAddress", fileAddress);
    } else if (type == 4) {
        fileAccount = $("#slx_89 .selected").data("id");
        localStorage.setItem("fileAccount", fileAccount);
    } else if (type == 84) {
        provinceid = $("#slx_84 .selected").data("id");
        $(".slx_85").val("Chọn danh sách chi nhánh");
        $.post("/do/branch/loadBranch", {
            provinceid: provinceid
        }, function (data) {
            $("#slx_85").html(data);
        });
    } else if (type == 63) {
        provinceid = $("#slx_63 .selected").data("id");
        $(".slx_97").val("Quận/Huyện");
        $.post("/do/town/loadTown", {
            provinceid: provinceid
        }, function (data) {
            $("#slx_97").html(data);
        });
    } else if (type == 74) {
        if ($("#slx_74 .selected").data("id") == 80) {
            $("#slx_100").parent().hide();
            $(".slx_100").removeClass("require_True");
        } else {
            $("#slx_100").parent().show();
            $(".slx_100").addClass("require_True");
        }
    } else if (type == 104) {
        provinceid = $("#slx_104 .selected").data("id");
        $(".slx_85").val("Danh sách chi nhánh ngân hàng OCB");
        $.post("/do/branch/loadBranch", {
            provinceid: provinceid
        }, function (data) {
            $("#slx_85").html(data);
        });
    } else if (type == 105) {
        provinceid = $("#slx_105 .selected").data("id");
        $(".slx_101").val("Quận/Huyện");
        $.post("/do/town/loadTown", {
            provinceid: provinceid
        }, function (data) {
            $("#slx_101").html(data);
        });
    } else if (type == 52) {
        provinceid = $("#slx_52 .selected").data("id");
        $(".slx_59").val("Quận/Huyện");
        $.post("/do/town/loadTown", {
            provinceid: provinceid
        }, function (data) {
            $("#slx_59").html(data);
        });

    } else if (type == 58) {
        $(".slx_59").val("Quận/Huyện");
        provinceid = $("#slx_58 .selected").data("id");
        $.post("/do/town/loadTown", {
            provinceid: provinceid
        }, function (data) {
            $("#slx_59").html(data);
        });
    } else if (type == 81) {
        if ($("#slx_81 .selected").data("id") == 85) {
            $('#dtt_83').parent().show();
            $('#dtt_83').addClass("require_True");
            $('#slx_84').parent().hide();
            $('#slx_85').parent().hide();
            $('#dtt_86').parent().hide();
            $('#slx_104').parent().hide();
            $('#slx_101').parent().hide();
            $('#slx_105').parent().hide();
            $('#slx_106').parent().hide();
            $('#dtt_102').parent().hide();
            $('#dtt_103').parent().hide();
            $('#dtt_107').parent().hide();
            $('#dtt_108').parent().hide();
        } else if ($("#slx_81 .selected").data("id") == 86) {
            $('#dtt_83').parent().hide();
            $('#slx_84').parent().hide();
            $('#slx_85').parent().hide();
            $('#dtt_86').parent().hide();
            $('#slx_104').parent().hide();
            $('#slx_101').parent().show();
            $('#slx_105').parent().show();
            $('#slx_106').parent().show();
            $('#dtt_102').parent().hide();
            $('#dtt_103').parent().hide();
            $('#dtt_107').parent().show();
            $('#dtt_108').parent().show();
        } else if ($("#slx_81 .selected").data("id") == 88) {
            $('#dtt_83').parent().hide();
            $('#slx_84').parent().hide();
            $('#slx_85').parent().hide();
            $('#dtt_86').parent().hide();
            $('#slx_104').parent().show();
            $('#slx_101').parent().show();
            $('#slx_105').parent().hide();
            $('#slx_106').parent().hide();
            $('#dtt_102').parent().show();
            $('#dtt_103').parent().show();
            $('#dtt_107').parent().hide();
            $('#dtt_108').parent().hide();
        } else if ($("#slx_81 .selected").data("id") == 87) {
            $('#dtt_83').parent().hide();
            $('#slx_84').parent().show();
            $('#slx_85').parent().show();
            $('#dtt_86').parent().hide();
            $('#slx_104').parent().show();
            $('#slx_101').parent().hide();
            $('#slx_105').parent().hide();
            $('#slx_106').parent().hide();
            $('#dtt_102').parent().hide();
            $('#dtt_103').parent().hide();
            $('#dtt_107').parent().hide();
            $('#dtt_108').parent().hide();
        }
    }
    else if (type == 115) {
        if ($("#slx_115 .selected").data("id") == 334) {

            $('.slx_63').parent().hide();
            $('.slx_97').parent().hide();

            $('#dtt_99').parent().parent().hide();
            $('#dtt_98').parent().parent().hide();

            $('.slx_63').removeClass("require_True");
            $('.slx_97').removeClass("require_True");
            $('#dtt_98').removeClass("require_True");
            $('#dtt_99').removeClass("require_True");
            $('#dtt_112').parent().show();
            $('#dtt_112').addClass("require_True");
            var tinh = $("#slx_58 .selected").text();
            var qh = $("#slx_59 .selected").text();
            var px = $("#dtt_60").val();
            var sn = $("#dtt_60").val();
            localStorage.setItem("dctt", sn + ", " + px + ", " + qh + ", " + tinh);
            if (localStorage.getItem("dctt").length <= 4) {
                $("#dtt_112").val(localStorage.getItem("address"));
            }
            else {
                $("#dtt_112").val(localStorage.getItem("dctt"));
            }



        }
        else if ($("#slx_115 .selected").data("id") == 333) {

            $('.slx_63').parent().hide();
            $('.slx_97').parent().hide();

            $('#dtt_99').parent().parent().hide();
            $('#dtt_98').parent().parent().hide();

            $('.slx_63').removeClass("require_True");
            $('.slx_97').removeClass("require_True");
            $('#dtt_98').removeClass("require_True");
            $('#dtt_99').removeClass("require_True");
            $('#dtt_112').parent().show();
            $('#dtt_112').addClass("require_True");

        }
        else if ($("#slx_115 .selected").data("id") == 335) {

            $('#slx_63').parent().show();
            $('#slx_97').parent().show();

            $('#dtt_99').parent().parent().show();
            $('#dtt_98').parent().parent().show();
            $('#dtt_112').parent().hide();

            $('#slx_63').addClass("require_True");
            $('#slx_97').addClass("require_True");
            $('#dtt_98').addClass("require_True");
            $('#dtt_99').addClass("require_True");
            $('#dtt_112').removeClass("require_True");
        }
    }
}

function CheckOTP() {
    var valueOTP = $("#codeOTP").val();
    if (valueOTP.length > 0) {
        $.post("/api/check-OTP.", {
            phone: localStorage.getItem("phoneuser"),
            authId: localStorage.getItem("authID"),
            userID: "test1",
            branchCode: "VN0010037",
            valueOTP: valueOTP
        }, function (data) {
            data = JSON.parse(data);
            if (data.transactionErrorCode == 90) {
                if (data.operationStatus == "EXECUTED") {
                    console.log("sent");
                    localStorage.setItem("authId", data.authId);
                    location.href = "step4";
                } else {
                    console.log("error");
                }
            } else {
                console.log("error");
            }
        })
    }
}


function CheckAvailableEmail(email) {

    if (CheckEmailValid(email.trim()) != "") {
        $("#dtt_6").css("border", "1px solid red");
    } else {


        $.post("/do/register/CheckEmail", {
            email: email,
        }, function (data) {
            data = JSON.parse(data);
            if (data.success == -1) {
                console.log(data.error);
            } else if (data.success == 0) {
                $("#dtt_6").css("border", "unset");
            } else if (data.success == 1) {
                alertify.alert("Vui lòng nhập lại", "<li>Email đã được đăng ký</li>");
                $("#dtt_6").css("border", "1px solid red");
            }
        });
    }
}

function CheckAvailablePhone(phone) {
    if (CheckPhoneValid(phone) == false) {
        $("#dtt_5").css("border", "1px solid red");
    } else {


        $.post("/do/register/CheckPhone", {
            phone: phone,
        }, function (data) {
            data = JSON.parse(data);
            if (data.success == -1) {
                console.log(data.error);
            } else if (data.success == 0) {
                $("#dtt_5").css("border", "unset");
            } else if (data.success == 1) {
                alertify.alert("Vui lòng nhập lại", "<li>Phone đã được đăng ký</li>");
                $("#dtt_5").css("border", "1px solid red");
            }
        });
    }
}