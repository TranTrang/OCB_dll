﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="card3.aspx.cs" Inherits="OCB.card3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header-page bg-green-dark">
        <div class="container-fluid">
            <div class="row  box-logo-pass-menu">
                <div class="col-xs-12 col-sm-5 col-md-5">
                    <div class="row m-row-pass">
                        <div class="col-xs-2 hidden-sm hidden-md hidden-lg">
                            <a href="#">
                                <div class="icon-bar-mobile">
                                    <i class="fas fa-bars"></i>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-7 col-sm-5 col-md-5 col-lg-5">
                            <a href="#">
                                <div class="img-logo">
                                    <img src="/images/OCB/logo.png" class="img-responsive" />
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7">
                            <a href="#">
                                <div class="img-pass">
                                    <img src="/images/OCB/pass.png" class="img-responsive" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 list-menu">
                    <nav>
                        <ul class="list-menu-parent">
                            <li><a href="#">Tài khoản OCB OMMI</a></li>
                            <li><a href="#">Thẻ</a></li>
                            <li><a href="#">Vay</a></li>
                            <li><a href="#">Tiết kiệm</a></li>
                            <li><a href="#">Khuyến mãi</a></li>
                            <li><a href="#">Blog FAQ</a></li>
                            <li class=" hidden-sm hidden-md  hidden-lg"><a href="#">Quy trình mở thẻ</a></li>
                            <li class=" hidden-sm hidden-md hidden-lg"><a href="#">Loại thẻ</a></li>
                            <li class=" hidden-sm hidden-md hidden-lg"><a href="#">Hồ sơ và điều kiện </a></li>
                            <li class="hidden-sm hidden-md hidden-lg"><a href="#">Ưu đãi thẻ </a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <form class="row" runat="server">

        <div class="box-step6" style="margin: 60px">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-13 col-md-12 text-right">
                        <h4><i class="fas fa-info-circle"></i>Hướng dẫn</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-13 col-md-12 text-center">
                        <h4>TẢI TÀI LIỆU</h4>
                        <p>Nhấn hoặc tải lên một ảnh chụp cho mỗi nhóm hồ sơ trên</p>
                        <p>Giờ bạn có thể chuyển sang dùng điện thoại. Nhập số điện thoại vào phần "Hoàn tất đăng ký hoặc theo dõi trạng thái để tiếp tục lại bước tải hồ sơ" </p>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <a href="/card-medium-1/step6/hosocanhan-87">
                            <div class="ct-btn">
                                <div class="img-icon">
                                    <i class="fas fa-users"></i>
                                </div>

                                <span>Hồ sơ cá nhân</span>

                            </div>
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <a href="/card-medium-1/step6/hosonoio-88">
                            <div class="ct-btn">
                                <div class="img-icon">
                                    <i class="fas fa-home"></i>
                                </div>

                                <span>Hồ sơ nơi ở</span>

                            </div>
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <a href="/card-medium-1/step6/hosotaichinh-89">
                            <div class="ct-btn">
                                <div class="img-icon">
                                    <i class="fas fa-money-check-alt"></i>
                                </div>

                                <span>Hồ sơ tài chính</span>

                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xm-12 col-sm-12 col-md-12 col-lg-12 box-success-file">
                        <div class="btn-success-file">
                            <a href="/card-medium-1/step7" class="btn btn-success">HOÀN THÀNH</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</asp:Content>
