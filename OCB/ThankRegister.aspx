﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="ThankRegister.aspx.cs" Inherits="OCB.ThankRegister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="box-thank text-center">
                    <h3 class="color-green-dark">Cảm ơn bạn đã hoàn tất đăng ký !</h3>
                    <p class="text-center">
                        Chúng tôi đã tiếp nhận hồ sơ của bạn và sẽ phản hồi trong thời gian sớm nhất
                        <br />
                        Bất cứ lúc nào bạn cũng có thể theo dõi trạng thái đăng ký bằng cách
                        <br />
                        vào My account, nhập số điện thoại đã đăng ký.
                    </p>
                    <div class="box-complete">
                        <button class="btn btn-complete" onclick="Open()">XEM TRẠNG THÁI HỒ SƠ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>    
        function Open() {
            Location.href = "/StatusRegister.aspx";
        }

    </script>
</asp:Content>

