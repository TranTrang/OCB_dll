﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Blog.aspx.cs" Inherits="OCB.Blog" MasterPageFile="~/MasterPageBlog.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <section class="box-blog">
       
       <div class="container">
            <div class="head-blog text-center">
               <div class="img-head-blog">
                   <img src="/images/blog/imgBlog.jpg" class="img-responsive" />
               </div>
               <p>Follow the blog for product announcements, feature updates, user stories,<br /> and technical posts about banking</p>
            </div>
           <section class="blog-new">
               <div class="row">
               <div class="col-xs-12 col-sm-7 col-md-7">
                   <div class="img-new-blog">
                       <img src="/images/blog/bai1_anh1.jpg" class="img-responsive"/>
                   </div>
               </div>
               <div class="col-xs-12 col-sm-5 col-md-5">
                   <div class="content-blog-new">
                       <article class="head-content">
                            <h4><strong>Bạn biết gì về ngân hàng hợp kênh OMNI? Bạn được gì khi sử dụng OMNI?</strong>
                             </h4>
                       </article>
                       <article class="body-content">
                           <p>Bạn đã từng nghe và thắc mắc về Omni Channel và ngân hàng hợp kênh? Bạn được gì khi sử dụng Omni? Bài viết này sẽ mang đến cho bạn cái nhìn cơ bản về sự khác nhau giữa ngân hàng đa kênh và hợp kênh, cùng những đặc điểm nổi bật chỉ có tại Omni.
                            Omni Channel là gì?
                            Những năm gần đây, khái niệm Digital Banking được nhắc đến rất nhiều và có thể hiểu đơn giản đây là quá trình số hóa ngân hàng truyền thống (traditional bank) thành ngân hàng số (digital bank). Digital bank là xu thế tất yếu với mục tiêu số hóa tối đa toàn bộ quy trình vận hành và phát triển, để cung cấp cho khách hàng những tiện ích và trải nghiệm sản phẩm dịch vụ ngân hàng hiện đại.
                            </p>
                       </article>
                       <article class="footer-contnet">
                           <a href="/blogdetail-1">> Xem thêm</a>
                       </article>
                   </div>

               </div>
           </div>
           </section>
           <section class="box-deal">
               <div class=" grid-container-deal">
                   <div class=" item-grid">
                       <div class="img-deal">
                           <img src="/images/blog/bai2_anh1.jpg" class="img-responsive" />
                       </div>
                       <div class="head-deal">
                           <h4><strong>Gửi tiết kiệm online và những lưu ý bạn cần biết </strong>
                             </h4>
                       </div>
                       <div class="body-deal">
                           <p>Bạn có biết, ở nhiều nước trên thế giới, tỷ trọng giao dịch tiết kiệm online lên đến 70%, trong khi ở Việt Nam hiện mới khoảng 10%? Bạn còn e ngại tiết kiệm online không an toàn hay lãi suất không tốt? Bài viết này sẽ giúp bạn giải đáp những khúc mắc đó.</p>
                       </div>
                       <div class="footer-deal">
                           <a href="/blogdetail-2">> Xem thêm</a>
                       </div>
                   </div>
                   <div class="  item-grid">
                       <div class="img-deal">
                           <img src="/images/blog/bai3anh1.jpg" class="img-responsive" />
                       </div>
                       <div class="head-deal">
                           <h4><strong>Thông minh lựa chọn đúng ngân hàng - tiết kiệm hàng triệu tiền phí <br />
                              </strong>
                             </h4>
                       </div>
                       <div class="body-deal">
                           <p>Bạn thường xuyên mua sắm online? Bạn thích thanh hóa các hóa đơn điện, nước, internet cùng hàng loạt hóa đơn khác bằng tài khoản trực tuyến vì tiện lợi và dễ dàng? Nhưng bạn đau đầu vì “hàng ngàn” loại phí? Bài viết này sẽ cho bạn một giải pháp. </p>
                       </div>
                       <div class="footer-deal">
                           <a href="/blogdetail-3">> Xem thêm</a>
                       </div>
                   </div>
                   <div class=" item-grid">
                       <div class="img-deal">
                           <img src="/images/blog/new2.jpg" class="img-responsive" />
                       </div>
                       <div class="head-deal">
                           <h4><strong>Giao dịch nhanh không lo gián đoạn. <br />
                               Mọi lúc. Mọi nơi</strong>
                             </h4>
                       </div>
                       <div class="body-deal">
                           <p>Trải nghiệm ngay Ngân hàng số hợp kênh đầu tiên tại Việt Nam để cảm nhận. Trải nghiệm ngay Ngân hàng số hợp kênh đầu tiên tại Việt Nam để cảm nhận. Trải nghiệm ngay Ngân hàng số hợp kênh đầu tiên tại Việt Nam để cảm nhận. </p>
                       </div>
                       <div class="footer-deal">
                           <a href="#">> Xem thêm</a>
                       </div>
                   </div>
               </div>
           </section>
           <div class="box-blog-tow">
                 <div class="grid-container">
                   <div class="item-grid">
                       <div class="img-deal">
                           <img src="/images/blog/video2.jpg" class="img-responsive" />
                       </div>
                       <div class="head-deal">
                           <h4><strong>Giao dịch nhanh không lo gián đoạn. <br />
                               Mọi lúc. Mọi nơi</strong>
                             </h4>
                       </div>
                       <div class="body-deal">
                           <p>Trải nghiệm ngay Ngân hàng số hợp kênh đầu tiên tại Việt Nam để cảm nhận. Trải nghiệm ngay Ngân hàng số hợp kênh đầu tiên tại Việt Nam để cảm nhận. Trải nghiệm ngay Ngân hàng số hợp kênh đầu tiên tại Việt Nam để cảm nhận. </p>
                       </div>
                       <div class="footer-deal">
                           <a href="#">> Xem thêm</a>
                       </div>
                   </div>
                 <div class=" item-grid">
                       <div class="img-deal">
                           <img src="/images/blog/new2.jpg" class="img-responsive" />
                       </div>
                       <div class="head-deal">
                           <h4><strong>Giao dịch nhanh không lo gián đoạn. <br />
                               Mọi lúc. Mọi nơi</strong>
                             </h4>
                       </div>
                       <div class="body-deal">
                           <p>Trải nghiệm ngay Ngân hàng số hợp kênh đầu tiên tại Việt Nam để cảm nhận. Trải nghiệm ngay Ngân hàng số hợp kênh đầu tiên tại Việt Nam để cảm nhận. Trải nghiệm ngay Ngân hàng số hợp kênh đầu tiên tại Việt Nam để cảm nhận. </p>
                       </div>
                       <div class="footer-deal">
                           <a href="#">> Xem thêm</a>
                       </div>
                   </div>
                   
               </div>
           </div>
       </div>
   </section>
</asp:Content>