﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace OCB
{
    public class Global : HttpApplication
    {
        void URL(System.Web.Routing.RouteCollection routes)
        {

            //Frontend
            System.Web.Routing.Route pageRoute;
            pageRoute = new Route("{cardname}-{id}/step1", new PageRouteHandler("~/step1.aspx"));
            pageRoute.DataTokens = new System.Web.Routing.RouteValueDictionary { { "step", "1" } };
            routes.Add(pageRoute);

            pageRoute = new System.Web.Routing.Route("{cardname}-{id}/step2a", new System.Web.Routing.PageRouteHandler("~/step2.aspx"));
            pageRoute.DataTokens = new System.Web.Routing.RouteValueDictionary { { "step", "2" } };
            routes.Add(pageRoute);

            pageRoute = new System.Web.Routing.Route("{cardname}-{id}/step2b", new System.Web.Routing.PageRouteHandler("~/step3.aspx"));
            pageRoute.DataTokens = new System.Web.Routing.RouteValueDictionary { { "step", "3" } };
            routes.Add(pageRoute);

            pageRoute = new System.Web.Routing.Route("{cardname}-{id}/step3", new System.Web.Routing.PageRouteHandler("~/step4.aspx"));
            pageRoute.DataTokens = new System.Web.Routing.RouteValueDictionary { { "step", "4" } };
            routes.Add(pageRoute);

            pageRoute = new System.Web.Routing.Route("{cardname}-{id}/step4", new System.Web.Routing.PageRouteHandler("~/UploadImageUserRegister.aspx"));
            pageRoute.DataTokens = new System.Web.Routing.RouteValueDictionary { { "step", "5" } };
            routes.Add(pageRoute);

            pageRoute = new System.Web.Routing.Route("{cardname}-{id}/step3a", new System.Web.Routing.PageRouteHandler("~/OTP.aspx"));
            pageRoute.DataTokens = new System.Web.Routing.RouteValueDictionary { { "step", "5" } };
            routes.Add(pageRoute);


            // blog
             routes.MapPageRoute("", "blogdetail-{id}", "~/BlogDetail.aspx");

            // ============================================================
            // ============================================================
            // Control Panel
            routes.MapPageRoute("", "cp-login", "~/cp/Login.aspx");
            routes.MapPageRoute("", "cp-default", "~/cp/Default.aspx");
            routes.MapPageRoute("", "cp-demo", "~/cp/page/demo/Demo.aspx");

            // user page
            routes.MapPageRoute("", "cp-user", "~/cp/page/user/User.aspx");
            routes.MapPageRoute("", "cp-add-user", "~/cp/page/user/AddUser.aspx");
            routes.MapPageRoute("", "cp-edit-user/id-{id}", "~/cp/page/user/EditUser.aspx");
            routes.MapPageRoute("", "cp-user-survey/userID-{userID}", "~/cp/page/user/UserSurvey.aspx");
            // profile image
            routes.MapPageRoute("", "cp-user-profile-image/userID-{userID}", "~/cp/page/user/profile-image/UserProfileImage.aspx");
            routes.MapPageRoute("", "cp-add-user-profile-image/userID-{userID}", "~/cp/page/user/profile-image/AddUserProfileImage.aspx");
            //routes.MapPageRoute("", "cp-edit-user-profile-image/userID-{userID}", "~/cp/page/user/profile-image/UserProfileImage.aspx");

            // card
            routes.MapPageRoute("", "cp-card", "~/cp/page/card/Card.aspx");
            routes.MapPageRoute("", "cp-add-card", "~/cp/page/card/AddCard.aspx");
            routes.MapPageRoute("", "cp-edit-card/cardID-{cardID}", "~/cp/page/card/EditCard.aspx");
            routes.MapPageRoute("", "cp-card-image/cardID-{cardID}", "~/cp/page/card/card-image/CardImage.aspx");
            routes.MapPageRoute("", "cp-edit-card-image/cardImageID-{cardImageID}", "~/cp/page/card/card-image/EditCardImage.aspx");

            // survey
            routes.MapPageRoute("", "cp-survey", "~/cp/page/survey/Survey.aspx");
            routes.MapPageRoute("", "cp-add-survey", "~/cp/page/survey/AddSurvey.aspx");
            routes.MapPageRoute("", "cp-edit-survey/id-{id}", "~/cp/page/survey/EditSurvey.aspx");

            // survey item value
            routes.MapPageRoute("", "cp-survey-item-value/surveyID-{surveyID}", "~/cp/page/survey/survey-item-value/SurveyItemValue.aspx");
            routes.MapPageRoute("", "cp-add-survey-item-value/surveyID-{surveyID}", "~/cp/page/survey/survey-item-value/AddSurveyItemValue.aspx");
            routes.MapPageRoute("", "cp-edit-survey-item-value/surveyItemValueID-{surveyItemValueID}", "~/cp/page/survey/survey-item-value/EditSurveyItemValue.aspx");

            //country
            routes.MapPageRoute("", "cp-country", "~/cp/page/country/Country.aspx");
            routes.MapPageRoute("", "cp-add-country", "~/cp/page/country/AddCountry.aspx");
            routes.MapPageRoute("", "cp-edit-country-countryID-{countryID}", "~/cp/page/country/EditCountry.aspx");

            // province
            routes.MapPageRoute("", "cp-province", "~/cp/page/province/Province.aspx");
            routes.MapPageRoute("", "cp-add-province", "~/cp/page/province/AddProvince.aspx");
            routes.MapPageRoute("", "cp-edit-province-provinceID-{provinceID}", "~/cp/page/province/EditProvince.aspx");

            // town
            routes.MapPageRoute("", "cp-town", "~/cp/page/town/Town.aspx");
            routes.MapPageRoute("", "cp-add-town", "~/cp/page/town/AddTown.aspx");
            routes.MapPageRoute("", "cp-edit-town-townID-{townID}", "~/cp/page/town/EditTown.aspx");

            // branch
            routes.MapPageRoute("", "cp-branch", "~/cp/page/branch/Branch.aspx");
            routes.MapPageRoute("", "cp-add-branch", "~/cp/page/branch/AddBranch.aspx");
            routes.MapPageRoute("", "cp-edit-branch-branchID-{branchID}", "~/cp/page/branch/EditBranch.aspx");

            // page
            routes.MapPageRoute("", "cp-email-page", "~/cp/page/page/email/EmailPage.aspx");
            routes.MapPageRoute("", "cp-email-customize/pageID-{pageID}", "~/cp/page/page/email/EmailCustomizer.aspx");
            routes.MapPageRoute("", "cp-page", "~/cp/page/page/Page.aspx");
            routes.MapPageRoute("", "cp-add-page", "~/cp/page/page/AddPage.aspx");
            routes.MapPageRoute("", "cp-edit-page/pageID-{pageID}", "~/cp/page/page/EditPage.aspx");

            // slider
            routes.MapPageRoute("", "cp-slider", "~/cp/page/slider/Slider.aspx");
            routes.MapPageRoute("", "cp-add-slider", "~/cp/page/slider/AddSlider.aspx");
            routes.MapPageRoute("", "cp-edit-slider/sliderID-{sliderID}", "~/cp/page/slider/EditSlider.aspx");
            routes.MapPageRoute("", "cp-slider-image/sliderID-{sliderID}", "~/cp/page/slider/SliderImage.aspx");

            // ============================================================
        }
        void Application_Start(object sender, EventArgs e)
        {
            URL(System.Web.Routing.RouteTable.Routes);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}