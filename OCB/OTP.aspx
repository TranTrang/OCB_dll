﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="OTP.aspx.cs" Inherits="OCB.OTP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header class="header-page bg-green-dark">

        <div class="container-fluid">
            <div class="row  box-logo-pass-menu">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="row m-row-pass">
                        <div class="col-xs-2 hidden-sm hidden-md hidden-lg">
                            <a href="#">
                                <div class="icon-bar-mobile">
                                    <i class="fas fa-bars"></i>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            <a href="#">
                                <div class="img-logo">
                                    <img src="/images/OCB/logo.png" class="img-responsive" />
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-5 col-md-5 col-lg-">
                            <a href="#">
                                <div class="img-pass">
                                    <img src="/images/OCB/pass.png" class="img-responsive" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-8 col-md-7 col-lg-9 list-menu">
                    <nav>
                        <ul class="list-menu-parent">
                            <li><a href="#">OCB OMMI</a></li>
                            <li><a href="#">Thẻ tín dụng</a></li>
                            <li><a href="#">Vay</a></li>
                            <li><a href="#">Ưu đãi</a></li>
                            <li><a href="#">Mr.Tài chính</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">My account</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

    </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="boxOTP" style="margin-top: 100px">
        <div class="form-inline text-center">
            <div class="col-xs-12 form-group mx-sm-3 mb-2">
                <label for="codeOTP" class="sr-only">Nhập mã OTP</label>
                <input type="text" class="form-control" id="codeOTP" placeholder="Nhập mã OTP" />
            </div>
            <div class="col-xs-12 form-group" style="margin-top: 10px;">
                <button type="submit" class="btn btn-primary mb-2" onclick="CheckOTP()">GỬI</button>
            </div>
        </div>
    </div>
</asp:Content>
