﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="step4.aspx.cs" Inherits="OCB.step4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
     <input id="ipstep" value="<%=step %>" hidden="hidden" />
    <form class="row" runat="server">

        <section class="box-banner">
            <div class="box-form  " data-wow-duration="2s" data-wow-delay="0.01s">
                <div class="container">
                    <div class="row item-form">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="title-form text-center">
                                <h3 class="color-green-dark">Yeah! Thẻ đến với bạn đây!</h3>
                                <h4>Bạn xác nhận thông tin nhé</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row dl-margin-form">
                        <div class="col-xs-12 col-sm-12 col-md-12 box-number-form  box-number-form-cs ">
                            <div class="box-number">
                                <span class="number-form" id="number1">1</span>
                                <p class="hidden-xs">Thông tin liên hệ</p>
                            </div>

                            <div class="box-number">
                                <span class="number-form" id="number2">2</span>
                                <p class="hidden-xs">Hoàn thành đơn đăng ký</p>
                            </div>

                            <div class="box-number">
                                <span class="number-form" id="number3">3</span>
                                <p class="hidden-xs">Xác nhận thông tin</p>

                            </div>

                            <div class="box-number">
                                <span class="number-form" id="number4">4</span>
                                <p class="hidden-xs">Tải hồ sơ</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 box-question">
                            <div class="question<%=step %> question" data-tab="number1">
                                <input id="ipstep" value="<%=step %>" hidden="hidden" />
                                <div class="row item-form">
                                    <%for (int j = 0; j < listSurveyStep.Count(); j++)
                                        { %>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <%if (listSurveyStep[j].Type == "T")
                                            {

                                        %>
                                        <div class="input-text">
                                            <span class="input input-jiro">

                                                <%
                                                    if (listSurveyStep[j].ID == 6)
                                                    {%>
                                                <input type="text" placeholder="<%=listSurveyStep[j].Code%>" id="dtt_<%=listSurveyStep[j].ID%>" class="input-field input-field-jiro email_<%=listSurveyStep[j].Required %>  survey require_<%=listSurveyStep[j].Required %>  type_<%=listSurveyStep[j].Number %>" data-error="<%=listSurveyStep[j].Code %>" data-email="Email không đúng định dạng" />
                                                <%}
                                                    else if (listSurveyStep[j].ID == 80 || listSurveyStep[j].ID == 5)
                                                    { %>
                                                <input type="text" placeholder="<%=listSurveyStep[j].Code%>" id="dtt_<%=listSurveyStep[j].ID%>" class="input-field input-field-jiro phone_<%=listSurveyStep[j].Required %>  survey require_<%=listSurveyStep[j].Required %>" data-error="<%=listSurveyStep[j].Code %>" data-phone="Số điện thoại không đúng định dạng" />
                                                <%}
                                                    else if (listSurveyStep[j].ID == 14 || listSurveyStep[j].ID == 94 || listSurveyStep[j].ID == 73 || listSurveyStep[j].ID == 92)
                                                    {
                                                %>
                                                <input placeholder="<%=listSurveyStep[j].Code%>" class="currency survey require_<%=listSurveyStep[j].Required %>" id="dtt_<%=listSurveyStep[j].ID%>" data-error="<%=listSurveyStep[j].Code %>" />
                                                <script>

</script>
                                                <%
                                                    }
                                                    else
                                                    {%>
                                                <input type="text" placeholder="<%=listSurveyStep[j].Code%>" id="dtt_<%=listSurveyStep[j].ID%>" class="input-field input-field-jiro survey require_<%=listSurveyStep[j].Required %>" data-error="<%=listSurveyStep[j].Code %>" />
                                                <%}%>
                                                <label class="input-label input-label-jiro" for="name">

                                                    <span class="input-label-content input-label-content-jiro"><%=listSurveyStep[j].Code%>

                                                        <%if (listSurveyStep[j].Required == true)
                                                            { %>(*) <%} %></span>
                                                </label>
                                            </span>
                                        </div>
                                        <%
                                            }
                                            else if (listSurveyStep[j].Type == "S")
                                            {
                                                listOption = SIV.GetListOption(listSurveyStep[j].ID);
                                        %>
                                        <input type="text" value="<%=listSurveyStep[j].Code%>" class=" sl-item survey slx_<%=listSurveyStep[j].ID%>  require_<%=listSurveyStep[j].Required %>" onfocus="OpenSelect(this)" onblur="CloseSelect(this)" readonly data-error="<%=listSurveyStep[j].Code %>" id=" " />
                                        <div class="select-item" id="slx_<%=listSurveyStep[j].ID%>">
                                            <ul>
                                                <li data-id="" onclick="Select(this)"><%=listSurveyStep[j].Code%></li>
                                                <% if (listSurveyStep[j].ID == 19 || listSurveyStep[j].ID == 7 || listSurveyStep[j].ID == 52 || listSurveyStep[j].ID == 58 || listSurveyStep[j].ID == 63 || listSurveyStep[j].ID == 104 || listSurveyStep[j].ID == 105 || listSurveyStep[j].ID == 84)
                                                    {
                                                        for (int l = 0; l < listProvince.Count; l++)
                                                        {
                                                %>
                                                <li data-id="<%=listProvince[l].ID%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=listProvince[l].Name%></li>
                                                <%}
                                                    }
                                                    else if (listSurveyStep[j].ID == 48 || listSurveyStep[j].ID == 53 || listSurveyStep[j].ID == 51 || listSurveyStep[j].ID == 17 || listSurveyStep[j].ID == 25)
                                                    {
                                                        for (int date = 1; date <= 31; date++)
                                                        {
                                                %>
                                                <li data-id="<%=date%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=date%></li>
                                                <%
                                                        }
                                                    }
                                                    else if (listSurveyStep[j].ID == 49 || listSurveyStep[j].ID == 54)
                                                    {
                                                        for (int month = 1; month <= 12; month++)
                                                        {
                                                %>
                                                <li data-id="<%=month%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=month%></li>
                                                <%
                                                        }
                                                    }
                                                    else if (listSurveyStep[j].ID == 50 || listSurveyStep[j].ID == 55 || listSurveyStep[j].ID == 16)
                                                    {
                                                        for (int year = 1945; year <= 2018; year++)
                                                        {
                                                %>
                                                <li data-id="<%=year%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=year%></li>

                                                <%
                                                        }
                                                    }
                                                    else
                                                    {
                                                        for (int k = 0; k < listOption.Count; k++)
                                                        {%>
                                                <li data-id="<%=listOption[k].ID%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=listOption[k].Value%></li>
                                                <% }
                                                    }
                                                %>
                                            </ul>
                                        </div>
                                        <%}
                                            else if (listSurveyStep[j].Type == "C")
                                            { %>


                                        <div>
                                            <div class="outer-but">
                                                <a id="chk_<%=listSurveyStep[j].ID%>" class="orange confirm-address" href="javascript:void(0);" data-name="consult"><span>Có</span></a>
                                                <input type="radio" name="name" id="rdo_<%=listSurveyStep[j].ID%>" value="Yes" disabled="disabled" />
                                                <a id="chkn_<%=listSurveyStep[j].ID%>" class="green confirm-address" href="javascript:void(0);" data-name="consult"><span>Không</span></a><input type="radio" name="name" id="rdon_<%=listSurveyStep[j].ID%>" value="No" disabled="disabled" />
                                            </div>
                                        </div>

                                    </div>
                                    <%} %>
                                </div>
                                <%} %>
                            </div>
                           

                        </div>
                    </div>
                    <div class="container-fluid table-info-resgiter">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="box-button">
                                     <a href="step2b" class="btn btn-default btn-update">QUAY LẠI</a>
                                    <a class="btn btn-success btn-open" onclick="Open()">XÁC NHẬN</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </form>
</asp:Content>
