﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="step5.aspx.cs" Inherits="OCB.step5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
     <input id="ipstep" value="<%=step %>" hidden="hidden" />
    <div class="boxOTP" style="margin-top: 100px">
        <div class="form-inline text-center">
            <div class="col-xs-12 form-group mx-sm-3 mb-2">
                <label for="codeOTP" class="sr-only">Nhập mã OTP</label>
                <input type="text" class="form-control" id="codeOTP" placeholder="Nhập mã OTP" />
            </div>
            <div class="col-xs-12 form-group" style="margin-top: 10px;">
                <button type="submit" class="btn btn-primary mb-2" onclick="CheckOTP()">GỬI</button>
            </div>
        </div>
    </div>
</asp:Content>
