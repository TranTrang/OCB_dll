﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="card2.aspx.cs" Inherits="OCB.card2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <header class="header-page bg-green-dark">
        <input id="ipstep" value="<%=step %>" hidden="hidden" />
        <div class="container-fluid">
            <div class="row  box-logo-pass-menu">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="row m-row-pass">
                        <div class="col-xs-2 hidden-sm hidden-md hidden-lg">
                            <a href="#">
                                <div class="icon-bar-mobile">
                                    <i class="fas fa-bars"></i>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            <a href="#">
                                <div class="img-logo">
                                    <img src="/images/OCB/logo.png" class="img-responsive" />
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-5 col-md-5 col-lg-">
                            <a href="#">
                                <div class="img-pass">
                                    <img src="/images/OCB/pass.png" class="img-responsive" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-8 col-md-7 col-lg-9 list-menu">
                    <nav>
                        <ul class="list-menu-parent">
                            <li><a href="#">OCB OMMI</a></li>
                            <li><a href="#">Thẻ tín dụng</a></li>
                            <li><a href="#">Vay</a></li>
                            <li><a href="#">Ưu đãi</a></li>
                            <li><a href="#">Mr.Tài chính</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">My account</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <form class="row" runat="server">

        <section class="box-banner">
            <div class="box-form  " data-wow-duration="2s" data-wow-delay="0.01s">
                <div class="container">
                    <div class="row item-form">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="title-form text-center">
                                <%if (step == 2)
                                    { %>
                                <h3 class="color-green-dark">Ồ! Tên bạn thú vị quá! Thế còn thông tin cá nhân của bạn?</h3>
                                <%}
                                    else if (step == 3)
                                    {%>
                                <h3 class="color-green-dark">Thật tuyệt! Thêm thông tin công việc là thẻ sớm gặp được bạn rồi</h3>
                                <%}
                                    else
                                    {%>
                                <h3 class="color-green-dark">Yeah! Thẻ đến với bạn đây ! Bạn xác nhận thông tin nhé</h3>
                                <%} %>
                            </div>
                        </div>
                    </div>
                    <div class="row dl-margin-form">
                        <div class="col-xs-12 col-sm-12 col-md-12 box-number-form  box-number-form-cs ">
                            <div class="box-number">
                                <span class="number-form" id="number1">1</span>
                                <p class="hidden-xs">Thông tin liên hệ</p>
                            </div>

                            <div class="box-number">
                                <span class="number-form" id="number2">2</span>
                                <p class="hidden-xs">Hoàn thành đơn đăng ký</p>
                            </div>

                            <div class="box-number">
                                <span class="number-form" id="number3">3</span>
                                <p class="hidden-xs">Xác nhận thông tin</p>

                            </div>

                            <div class="box-number">
                                <span class="number-form" id="number4">4</span>
                                <p class="hidden-xs">Tải hồ sơ</p>
                            </div>
                        </div>
                        <%if (step < 4 && step > 1)
                            { %>
                        <div class="col-xs-12 col-sm-12 col-md-12 box-question">
                            <div class="question<%=step %> question" data-tab="number1">
                                <input id="ipstep" value="<%=step %>" hidden="hidden" />
                                <div class="row item-form">
                                    <%for (int j = 0; j < listSurveyStep.Count(); j++)
                                        { %>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <%if (listSurveyStep[j].Type == "T")
                                            {

                                        %>
                                        <%--<label for="dtt_<%=listSurveyStep[j].ID%>" id="lb_<%=listSurveyStep[j].ID%>"><%=listSurveyStep[j].Code%>
                                                                         <%if (listSurveyStep[j].Required == true)
                                                                             { %>
                                                                         <span class="color-red">(*)</span>
                                                                    <%} %>
                                                                    </label>--%>
                                        <div class="input-text">
                                            <span class="input input-jiro">

                                                <%
                                                    if (listSurveyStep[j].ID == 6)
                                                    {%>
                                                <input type="text" placeholder="<%=listSurveyStep[j].Code%>" id="dtt_<%=listSurveyStep[j].ID%>" class="input-field input-field-jiro email_<%=listSurveyStep[j].Required %>  survey require_<%=listSurveyStep[j].Required %>  type_<%=listSurveyStep[j].Number %>" data-error="<%=listSurveyStep[j].Code %>" data-email="Email không đúng định dạng" />
                                                <%}
                                                    else if (listSurveyStep[j].ID == 80 || listSurveyStep[j].ID == 5)
                                                    { %>
                                                <input type="text" placeholder="<%=listSurveyStep[j].Code%>" id="dtt_<%=listSurveyStep[j].ID%>" class="input-field input-field-jiro phone_<%=listSurveyStep[j].Required %>  survey require_<%=listSurveyStep[j].Required %>" data-error="<%=listSurveyStep[j].Code %>" data-phone="Số điện thoại không đúng định dạng" />
                                                <%}
                                                    else if (listSurveyStep[j].ID == 14 || listSurveyStep[j].ID == 94 || listSurveyStep[j].ID == 73 || listSurveyStep[j].ID == 92)
                                                    {
                                                %>
                                                <input placeholder="<%=listSurveyStep[j].Code%>" class="currency survey require_<%=listSurveyStep[j].Required %>" id="dtt_<%=listSurveyStep[j].ID%>" data-error="<%=listSurveyStep[j].Code %>" />
                                                <script>

</script>
                                                <%
                                                    }
                                                    else
                                                    {%>
                                                <input type="text" placeholder="<%=listSurveyStep[j].Code%>" id="dtt_<%=listSurveyStep[j].ID%>" class="input-field input-field-jiro survey require_<%=listSurveyStep[j].Required %>" data-error="<%=listSurveyStep[j].Code %>" />
                                                <%}%>
                                                <label class="input-label input-label-jiro" for="name">

                                                    <span class="input-label-content input-label-content-jiro"><%=listSurveyStep[j].Code%>

                                                        <%if (listSurveyStep[j].Required == true)
                                                            { %>(*) <%} %></span>
                                                </label>
                                            </span>
                                        </div>
                                        <%
                                            }
                                            else if (listSurveyStep[j].Type == "S")
                                            {
                                                listOption = SIV.GetListOption(listSurveyStep[j].ID);
                                        %>
                                        <%--  <label for="slx_<%=listSurveyStep[j].ID%>" id="lb_<%=listSurveyStep[j].ID%>"><%=listSurveyStep[j].Code%>

                                                                     <%if (listSurveyStep[j].Required == true)
                                                                         { %>
                                                                         <span class="color-red">(*)</span>
                                                                    <%} %>
                                                             </label>--%>

                                        <%-- <select class="sl-item survey require_<%=listSurveyStep[j].Required %>" id="slx_<%=listSurveyStep[j].ID%>" data-error="<%=listSurveyStep[j].Code %>" >--%>
                                        <%-- <option value=""><%=listSurveyStep[j].Code%></option>--%>
                                        <input type="text" value="<%=listSurveyStep[j].Code%>" class=" sl-item survey slx_<%=listSurveyStep[j].ID%>  require_<%=listSurveyStep[j].Required %>" onfocus="OpenSelect(this)" onblur="CloseSelect(this)" readonly data-error="<%=listSurveyStep[j].Code %>" id=" " />
                                        <div class="select-item" id="slx_<%=listSurveyStep[j].ID%>">
                                            <ul>
                                                <li data-id="" onclick="Select(this)"><%=listSurveyStep[j].Code%></li>
                                                <% if (listSurveyStep[j].ID == 19 || listSurveyStep[j].ID == 7 || listSurveyStep[j].ID == 52 || listSurveyStep[j].ID == 58 || listSurveyStep[j].ID == 63 || listSurveyStep[j].ID == 104 || listSurveyStep[j].ID == 105 || listSurveyStep[j].ID == 84)
                                                    {
                                                        for (int l = 0; l < listProvince.Count; l++)
                                                        {
                                                %>
                                                <li data-id="<%=listProvince[l].ID%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=listProvince[l].Name%></li>
                                                <%}
                                                    }
                                                    else if (listSurveyStep[j].ID == 48 || listSurveyStep[j].ID == 53 || listSurveyStep[j].ID == 51 || listSurveyStep[j].ID == 17 || listSurveyStep[j].ID == 25)
                                                    {
                                                        for (int date = 1; date <= 31; date++)
                                                        {
                                                %>
                                                <li data-id="<%=date%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=date%></li>
                                                <%
                                                        }
                                                    }
                                                    else if (listSurveyStep[j].ID == 49 || listSurveyStep[j].ID == 54)
                                                    {
                                                        for (int month = 1; month <= 12; month++)
                                                        {
                                                %>
                                                <li data-id="<%=month%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=month%></li>
                                                <%
                                                        }
                                                    }
                                                    else if (listSurveyStep[j].ID == 50 || listSurveyStep[j].ID == 55 || listSurveyStep[j].ID == 16)
                                                    {
                                                        for (int year = 1945; year <= 2018; year++)
                                                        {
                                                %>
                                                <li data-id="<%=year%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=year%></li>

                                                <%
                                                        }
                                                    }
                                                    else
                                                    {
                                                        for (int k = 0; k < listOption.Count; k++)
                                                        {%>
                                                <li data-id="<%=listOption[k].ID%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=listOption[k].Value%></li>
                                                <% }
                                                    }
                                                %>
                                            </ul>
                                        </div>
                                        <%-- </select>--%>
                                        <%}
                                            else if (listSurveyStep[j].Type == "C")
                                            { %>

                                        <%-- <label style="margin-top: 20px">
                                            <%=listSurveyStep[j].Code%>
                                            <span class="color-red">

                                                <%if (listSurveyStep[j].Required == true)
                                                    { %>(*) <%} %></span>
                                        </label>--%>
                                        <div>
                                            <div class="outer-but">
                                                <a id="chk_<%=listSurveyStep[j].ID%>" class="orange confirm-address" href="javascript:void(0);" data-name="consult"><span>Có</span></a>
                                                <input type="radio" name="name" id="rdo_<%=listSurveyStep[j].ID%>" value="Yes" disabled="disabled" />
                                                <a id="chkn_<%=listSurveyStep[j].ID%>" class="green confirm-address" href="javascript:void(0);" data-name="consult"><span>Không</span></a><input type="radio" name="name" id="rdon_<%=listSurveyStep[j].ID%>" value="No" disabled="disabled" />
                                            </div>
                                        </div>

                                    </div>
                                    <%} %>
                                </div>
                                <%} %>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 footer-form">
                                    <div class="box-group">
                                        <div class="img-group hidden-xs">
                                        </div>

                                    </div>
                                    <div class="box-button">


                                        <%-- <%if (step == 1) { %>
                                                    <a href="step2a" class="btn btn-default btn-update">TIẾP TỤC</a>--%>
                                        <% if (step > 1)
                                            {
                                                if (step == 2)
                                                {%>
                                        <a href="step1" class="btn btn-default btn-update">QUAY LẠI</a>
                                        <%}
                                            else if (step == 3)
                                            {%>
                                        <a href="step2a" class="btn btn-default btn-update">QUAY LẠI</a>
                                        <%}
                                            else if (step == 4)
                                            {%>
                                        <a href="step2b" class="btn btn-default btn-update">QUAY LẠI</a>
                                        <%}
                                            else
                                            {%>
                                        <a href="step<%=step - 1 %>" class="btn btn-default btn-update">QUAY LẠI</a>
                                        <%}%>
                                        <%
                                            }
                                            if (step <= 4)
                                            {%>

                                        <a class="btn btn-success btn-open" onclick="Open()">TIẾP TỤC ></a>
                                        <%}%>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <%}
                        else if (step == 5)
                        { %>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-inline form-otp">

                                <div class="form-group mx-sm-3 mb-2">
                                    <label for="inputPassword2" class="sr-only">Mã OTP</label>
                                    <input type="password" class="form-control" id="txtCodeOTP" placeholder="Vui lòng nhập OTP">
                                </div>
                                <button type="submit" class="btn btnotp mb-2">Xác nhận OTP</button>
                            </div>
                        </div>
                    </div>
                    <%}
                        else if (step == 4)
                        {
                    %>
                    <div class="container-fluid table-info-resgiter">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="box-button">


                                    <%-- <%if (step == 1) { %>
                                                    <a href="step2a" class="btn btn-default btn-update">TIẾP TỤC</a>--%>
                                    <% if (step > 1)
                                        {
                                            if (step == 2)
                                            {%>
                                    <a href="step1" class="btn btn-default btn-update">QUAY LẠI</a>
                                    <%}
                                        else if (step == 3)
                                        {%>
                                    <a href="step2a" class="btn btn-default btn-update">QUAY LẠI</a>
                                    <%}
                                        else if (step == 4)
                                        {%>
                                    <a href="step2b" class="btn btn-default btn-update">QUAY LẠI</a>
                                    <%}
                                        else
                                        {%>
                                    <a href="step<%=step - 1 %>" class="btn btn-default btn-update">QUAY LẠI</a>
                                    <%}%>
                                    <%
                                        }
                                        if (step <= 6)
                                        {%>

                                    <a class="btn btn-success btn-open" onclick="Open()">TIẾP TỤC ></a>
                                    <%}%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%} %>
                </div>
            </div>
            </div>
        </section>
        <script>
            function OpenSelect(input) {
                $(input).parent().find(".select-item").show();
            }

            function CloseSelect(input) {
                setTimeout(() => {
                    $(input).parent().find(".select-item").hide();
                }, 150);
            }

            //function Select(input) {
            //    $(input).parent().parent().parent().find("input").val($(input).text());
            //    $(input).parent().find("li").removeClass("selected");
            //    $(input).addClass("selected");
            //}

            function Submit() {
                console.log($("#txtDate .selected").data("id"));
                console.log($("#txtMonth .selected").data("id"));
            }

        </script>
        <script>
            $("#slx_87").change(function () {

                GetTypeFileUser();
            });
            function GetTypeFileUser() {

            }
        </script>
        <script>

</script>
    </form>

    <style>
        .sl-item {
            font-weight: normal !important;
        }
    </style>
</asp:Content>


