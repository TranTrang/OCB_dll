﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SurveyItemValueManager
/// </summary>
namespace OCB.AppCode.Manager
{
    public class SurveyItemValueManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();

        public SurveyItemValueManager()
        {
        }

        // ===============================================
        // Basic Methods

        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(SurveyItemValueTBx element)
        {
            DB.SurveyItemValueTBxes.InsertOnSubmit(element);
            Save();
        }

        public SurveyItemValueTBx GetByID(int ID)
        {
            try
            {
                return DB.SurveyItemValueTBxes.Where(e => e.ID == ID && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return new SurveyItemValueTBx();
            }
        }

        public List<SurveyItemValueTBx> GetList()
        {
            try
            {
                return DB.SurveyItemValueTBxes.Where(e => e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<SurveyItemValueTBx>();
            }
        }
        public List<SurveyItemValueTBx> GetListOption(int surveyID)
        {
            try
            {
                return DB.SurveyItemValueTBxes.Where(e => e.IsDeleted == false && e.SurveyID == surveyID).OrderBy(e => e.OrderBy).ToList();
            }
            catch (Exception)
            {
                return new List<SurveyItemValueTBx>();
            }
        }

        // ===============================================================================================
        // This Manager Methods
        // 

        public List<SurveyItemValueTBx> GetListBySurveyID(int surveyID)
        {
            try
            {
                return DB.SurveyItemValueTBxes.Where(e => e.SurveyID == surveyID && e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<SurveyItemValueTBx>();
            }
        }
        public SurveyItemValueTBx GetBySurveyIDAndValueID(int surveyID, int ID)
        {
            return DB.SurveyItemValueTBxes.Where(e => e.ID == ID && e.SurveyID == surveyID).FirstOrDefault();
        }
    }
}