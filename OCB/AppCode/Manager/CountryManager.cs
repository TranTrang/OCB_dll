﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CountryManager
/// </summary>
namespace OCB.AppCode.Manager
{
    public class CountryManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();


        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(CountryTBx element)
        {
            DB.CountryTBxes.InsertOnSubmit(element);
            Save();
        }

        public bool IsExist(string name, string code)
        {
            return DB.CountryTBxes.Any(e => e.IsDeleted == false && e.Name == name && e.CountryCode == code);
        }

        public void AddRange(List<CountryTBx> element)
        {
            DB.CountryTBxes.InsertAllOnSubmit(element);
            Save();
        }

        public CountryTBx GetByID(int ID)
        {
            try
            {
                return DB.CountryTBxes.Where(e => e.ID == ID && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return new CountryTBx();
            }
        }

        public List<CountryTBx> GetList()
        {
            try
            {
                return DB.CountryTBxes.Where(e => e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<CountryTBx>();
            }
        }


        // ===============================================================================================
        // This Manager Methods
        // 
        public List<CountryTBx> SearchCountry(string strSearch)
        {
            try
            {
                return DB.CountryTBxes.Where(e => e.Name.ToLower().Contains(strSearch.ToLower()) && e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<CountryTBx>();
            }
        }

    }
}