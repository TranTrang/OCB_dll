﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserProfileImageManager
/// </summary>
namespace OCB.AppCode.Manager
{
    public class UserProfileImageManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();

        public UserProfileImageManager()
        {
        }

        // ===============================================
        // Basic Methods

        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(UserProfileImageTBx element)
        {
            DB.UserProfileImageTBxes.InsertOnSubmit(element);
            Save();
        }

        public UserProfileImageTBx GetByID(int ID)
        {
            try
            {
                return DB.UserProfileImageTBxes.Where(e => e.ID == ID && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return new UserProfileImageTBx();
            }
        }

        public List<UserProfileImageTBx> GetList()
        {
            try
            {
                return DB.UserProfileImageTBxes.Where(e => e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<UserProfileImageTBx>();
            }
        }

        // ===============================================================================================
        // This Manager Methods
        // 

        public List<UserProfileImageTBx> GetListByPhoneNumber(string phoneNumber)
        {
            try
            {
                return DB.UserProfileImageTBxes.Where(e => e.UserPhoneNumber == phoneNumber && e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<UserProfileImageTBx>();
            }
        }
        public List<UserProfileImageTBx> GetListByPhoneNumberAndType(string phoneNumber, string type)
        {
            try
            {
                return DB.UserProfileImageTBxes.Where(e => e.UserPhoneNumber == phoneNumber && e.IsDeleted == false && e.Type == type).ToList();
            }
            catch (Exception)
            {
                return new List<UserProfileImageTBx>();
            }
        }
        public void RemoveFileImageUser(int pid)
        {
            try
            {

                DB.UserProfileImageTBxes.DeleteOnSubmit(GetByID(pid));
                Save();
            }
            catch
            {

            }
        }

    }
}