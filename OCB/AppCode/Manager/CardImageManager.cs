﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CardImageManager
/// </summary>
namespace OCB.AppCode.Manager
{
    public class CardImageManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();

        public CardImageManager()
        {
        }

        // ===============================================
        // Basic Methods

        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(CardImageTBx element)
        {
            DB.CardImageTBxes.InsertOnSubmit(element);
            Save();
        }

        public CardImageTBx GetByID(int ID)
        {
            try
            {
                return DB.CardImageTBxes.Where(e => e.ID == ID && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return new CardImageTBx();
            }
        }

        public List<CardImageTBx> GetList()
        {
            try
            {
                return DB.CardImageTBxes.Where(e => e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<CardImageTBx>();
            }
        }

        // ===============================================================================================
        // This Manager Methods
        // 

        public List<CardImageTBx> GetListByCardID(int cardID)
        {
            try
            {
                return DB.CardImageTBxes.Where(e => e.CardID == cardID && e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<CardImageTBx>();
            }
        }

    }
}