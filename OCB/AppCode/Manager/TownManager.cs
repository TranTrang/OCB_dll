﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TownManager
/// </summary>
namespace OCB.AppCode.Manager
{
    public class TownManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();
        public void Add(TownTBx town)
        {
            DB.TownTBxes.InsertOnSubmit(town);
            DB.SubmitChanges();
        }


        public void Save()
        {
            DB.SubmitChanges();
        }

        public TownTBx GetByID(int id)
        {
            try
            {
                return DB.TownTBxes.Where(e => e.IsDeleted == false && e.ID == id).First();
            }
            catch (Exception)
            {
                return new TownTBx();
            }
        }
        public List<TownTBx> GetList()
        {
            try
            {
                return DB.TownTBxes.Where(e => e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<TownTBx>();
            }
        }

        public List<TownTBx> SearchProvince(string strSearch)
        {
            try
            {
                return DB.TownTBxes.Where(e => e.Name.ToLower().Contains(strSearch.ToLower()) && e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<TownTBx>();
            }
        }

        public TownTBx GetByTownID(string id)
        {
            try
            {
                return DB.TownTBxes.Where(e => e.IsDeleted == false && e.TownID == id).First();
            }
            catch (Exception)
            {
                return new TownTBx();
            }
        }

        public TownTBx GetByNameAndTownID(string name, string townID)
        {
            try
            {
                return DB.TownTBxes.Where(e => e.IsDeleted == false && e.TownID == townID && e.Name == name).First();
            }
            catch (Exception)
            {
                return new TownTBx();
            }
        }
        public List<TownTBx> GetByProvinceID(int id)
        {
            try
            {
                return DB.TownTBxes.Where(e => e.IsDeleted == false && e.ProvinceID == id).ToList();
            }
            catch (Exception)
            {
                return new List<TownTBx>();
            }
        }

    }
}