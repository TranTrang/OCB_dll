﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CardSurveyValueManager
/// </summary>
namespace OCB.AppCode.Manager
{
    public class CardSurveyValueManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();

        public CardSurveyValueManager()
        {
        }

        // ===============================================
        // Basic Methods

        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(CardSurveyValueTBx element)
        {
            DB.CardSurveyValueTBxes.InsertOnSubmit(element);
            Save();
        }
        public void AddList(List<CardSurveyValueTBx> list)
        {
            DB.CardSurveyValueTBxes.InsertAllOnSubmit(list);
            Save();
        }
        public CardSurveyValueTBx GetByID(int UCSID)
        {
            try
            {
                return DB.CardSurveyValueTBxes.Where(e => e.UCSID == UCSID && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return new CardSurveyValueTBx();
            }
        }

        public List<CardSurveyValueTBx> GetListByUserID(int userID)
        {
            try
            {
                return DB.CardSurveyValueTBxes.Where(e => e.UserTBx.ID == userID && e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<CardSurveyValueTBx>();
            }
        }

        // ===============================================================================================
        // This Manager Methods
        // 
        public void Add(List<CardSurveyValueTBx> listElement)
        {
            DB.CardSurveyValueTBxes.InsertAllOnSubmit(listElement);
            Save();
        }
    }
}