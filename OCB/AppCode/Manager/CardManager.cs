﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CardManager
/// </summary>
namespace OCB.AppCode.Manager
{
    public class CardManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();

        public CardManager()
        {
        }

        // ===============================================
        // Basic Methods

        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(CardTBx element)
        {
            DB.CardTBxes.InsertOnSubmit(element);
            Save();
        }

        public CardTBx GetByID(int ID)
        {
            try
            {
                return DB.CardTBxes.Where(e => e.ID == ID && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return new CardTBx();
            }
        }

        public List<CardTBx> GetList()
        {
            try
            {
                return DB.CardTBxes.Where(e => e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<CardTBx>();
            }
        }

        // ===============================================================================================
        // This Manager Methods
        // 

    }
}