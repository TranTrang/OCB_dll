﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OCB.AppCode.Manager
{
    public class BranchManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();

        public BranchManager()
        {
        }

        // ===============================================
        // Basic Methods

        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(BranchTBx element)
        {
            DB.BranchTBxes.InsertOnSubmit(element);
            Save();
        }

        public BranchTBx GetByID(int ID)
        {
            try
            {
                return DB.BranchTBxes.Where(e => e.ID == ID && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return new BranchTBx();
            }
        }

        public List<BranchTBx> GetList()
        {
            try
            {
                return DB.BranchTBxes.Where(e => e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<BranchTBx>();
            }
        }

        // ===============================================================================================
        // This Manager Methods
        // 

        public List<BranchTBx> GetListByProvinceID(int provinceID)
        {
            try
            {
                return DB.BranchTBxes.Where(e => e.ProvinceID == provinceID && e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<BranchTBx>();
            }
        }


        public List<BranchTBx> SearchBranch(string strSearch)
        {
            try
            {
                return DB.BranchTBxes.Where(e => e.NameVN.ToLower().Contains(strSearch.ToLower()) && e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<BranchTBx>();
            }
        }

        public BranchTBx GetByProvinceIDAndCode(string code, int? provinceID)
        {
            try
            {
                return DB.BranchTBxes.Where(e => e.ItemCode == code && e.ProvinceID == provinceID && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return new BranchTBx();
            }
        }
    }
}