﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MetadataManager
/// </summary>
namespace OCB.AppCode.Manager
{
    public class MetadataManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();

        public MetadataManager()
        {
        }

        // ===============================================
        // Basic Methods

        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(MetadataTBx element)
        {
            DB.MetadataTBxes.InsertOnSubmit(element);
            Save();
        }

        public MetadataTBx GetByID(int ID)
        {
            try
            {
                return DB.MetadataTBxes.Where(e => e.ID == ID && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return new MetadataTBx();
            }
        }

        public List<MetadataTBx> GetList()
        {
            try
            {
                return DB.MetadataTBxes.Where(e => e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<MetadataTBx>();
            }
        }

        // ===============================================================================================
        // This Manager Methods
        // 


    }
}