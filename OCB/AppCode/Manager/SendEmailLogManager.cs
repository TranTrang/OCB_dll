﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SendEmailLogManager
/// </summary>
namespace OCB.AppCode.Manager
{
    public class SendEmailLogManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();

        public SendEmailLogManager()
        {
        }

        // ===============================================
        // Basic Methods

        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(SendEmailLogTBx element)
        {
            DB.SendEmailLogTBxes.InsertOnSubmit(element);
            Save();
        }

        public SendEmailLogTBx GetByID(int ID)
        {
            try
            {
                return DB.SendEmailLogTBxes.Where(e => e.ID == ID).First();
            }
            catch (Exception)
            {
                return new SendEmailLogTBx();
            }
        }

        public List<SendEmailLogTBx> GetList()
        {
            try
            {
                return DB.SendEmailLogTBxes.ToList();
            }
            catch (Exception)
            {
                return new List<SendEmailLogTBx>();
            }
        }

        // ===============================================================================================
        // This Manager Methods
        // 
    }
}