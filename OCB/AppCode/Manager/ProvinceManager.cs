﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProvinceManager
/// </summary>
namespace OCB.AppCode.Manager
{
    public class ProvinceManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();

        public void Add(ProvinceTBx province)
        {
            DB.ProvinceTBxes.InsertOnSubmit(province);
            DB.SubmitChanges();
        }

        public void Save()
        {
            DB.SubmitChanges();
        }

        public ProvinceTBx GetByID(int id)
        {
            try
            {
                return DB.ProvinceTBxes.Where(e => e.IsDeleted == false && e.ID == id).Single();
            }
            catch (Exception)
            {

                return new ProvinceTBx();
            }
        }


        public ProvinceTBx GetByNameAndT24ID(string name, string t24ID)
        {
            try
            {
                return DB.ProvinceTBxes.Where(e => e.IsDeleted == false && e.Name == name && e.T24ID == t24ID).Single();
            }
            catch (Exception)
            {

                return new ProvinceTBx();
            }
        }
        public List<ProvinceTBx> GetList()
        {
            try
            {
                return DB.ProvinceTBxes.Where(e => e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<ProvinceTBx>();
            }
        }
        public List<ProvinceTBx> SearchProvince(string strSearch)
        {
            try
            {
                return DB.ProvinceTBxes.Where(e => e.Name.ToLower().Contains(strSearch.ToLower()) && e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<ProvinceTBx>();
            }
        }


        public ProvinceTBx GetByT24ID(string t24)
        {
            try
            {
                return DB.ProvinceTBxes.Where(e => e.IsDeleted == false && e.T24ID == t24).First();
            }
            catch (Exception)
            {
                return new ProvinceTBx();
            }
        }
    }
}