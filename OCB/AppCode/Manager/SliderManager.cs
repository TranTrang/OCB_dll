﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SliderManager
/// </summary>
namespace OCB.AppCode.Manager
{
    public class SliderManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();

        public SliderManager()
        {
        }

        // ===============================================
        // Basic Methods

        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(SliderTBx element)
        {
            DB.SliderTBxes.InsertOnSubmit(element);
            Save();
        }

        public SliderTBx GetByID(int ID)
        {
            try
            {
                return DB.SliderTBxes.Where(e => e.ID == ID && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return new SliderTBx();
            }
        }

        public List<SliderTBx> GetList()
        {
            try
            {
                return DB.SliderTBxes.Where(e => e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<SliderTBx>();
            }
        }

        // ===============================================================================================
        // This Manager Methods
        // 
    }
}