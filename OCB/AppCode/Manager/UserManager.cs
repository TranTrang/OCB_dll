﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserManager
/// </summary>
namespace OCB.AppCode.Manager
{
    public class UserManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();
        public UserManager()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        // ===============================================
        // Basic Methods

        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(UserTBx element)
        {
            DB.UserTBxes.InsertOnSubmit(element);
            Save();
        }

        public UserTBx GetByID(int ID)
        {
            try
            {
                return DB.UserTBxes.Where(e => e.ID == ID && e.Status != -1 && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return new UserTBx();
            }
        }

        public List<UserTBx> GetList()
        {
            try
            {
                return DB.UserTBxes.Where(e => e.Status != -1 && e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<UserTBx>();
            }
        }

        public List<UserTBx> SearchEmail(string strSearch)
        {
            try
            {
                return DB.UserTBxes.Where(e => e.Email.ToLower().Contains(strSearch.ToLower()) && e.Status != -1 && e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<UserTBx>();
            }
        }

        public List<UserTBx> SearchPhone(string strSearch)
        {
            try
            {
                return DB.UserTBxes.Where(e => e.Phone.ToLower().Contains(strSearch.ToLower()) && e.Status != -1 && e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<UserTBx>();
            }
        }

        // ===============================================================================================
        // This Manager Methods
        //

        public bool CheckEmailExisted(string email)
        {
            UserTBx user = GetByEmail(email);

            if (user == null) return false;
            return true;
        }

        public UserTBx GetByEmail(string email)
        {
            try
            {
                return DB.UserTBxes.Where(e => e.Email == email && e.Status != -1 && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool CheckPhoneExisted(string phone)
        {
            UserTBx user = GetByPhone(phone);

            if (user == null) return false;
            return true;
        }

        public bool CheckCardExisted(int card)
        {
            UserTBx user = GetCarID(card);

            if (user == null) return false;
            return true;
        }

        public UserTBx GetByPhone(string phone)
        {
            try
            {
                return DB.UserTBxes.Where(e => e.Phone == phone && e.Status != -1 && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public UserTBx GetCarID(int carid)
        {
            try
            {
                return (from user in DB.UserTBxes
                        join cardSurveyValue in DB.CardSurveyValueTBxes on user.ID equals cardSurveyValue.UserID
                        join cardSurvey in DB.CardSurveyTBxes on cardSurveyValue.CardSurveyID equals cardSurvey.ID
                        join card in DB.CardTBxes on cardSurvey.CardID equals card.ID
                        where cardSurveyValue.IsDeleted == false && cardSurvey.IsDeleted == false && card.IsDeleted == false
                            && user.IsDeleted == false && user.Status != -1
                            && card.ID == carid

                        select user).SingleOrDefault();
            }
            catch (Exception)
            {
                return new UserTBx();
            }
        }

        //public UserTBx GetByToken(string token)
        //{
        //    try
        //    {
        //        return DB.UserTBxes.Where(u => u.Token == token && u.Status != -1).First();
        //    }
        //    catch (Exception)
        //    {
        //        return new UserTBx();
        //    }
        //}
    }
}