﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PageManager
/// </summary>
namespace OCB.AppCode.Manager
{
    public class PageManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();

        public PageManager()
        {
        }

        // ===============================================
        // Basic Methods

        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(PageTBx element)
        {
            DB.PageTBxes.InsertOnSubmit(element);
            Save();
        }

        /// <summary>
        /// Get element by ID with status != -1
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public PageTBx GetByID(int ID)
        {
            try
            {
                return DB.PageTBxes.Where(e => e.ID == ID).First();
            }
            catch (Exception)
            {
                return new PageTBx();
            }
        }

        /// <summary>
        /// Get list all with status != -1
        /// </summary>
        /// <returns></returns>
        public List<PageTBx> GetList()
        {
            try
            {
                return DB.PageTBxes.ToList();
            }
            catch (Exception)
            {
                return new List<PageTBx>();
            }
        }

        // ===============================================================================================
        // This Manager Methods
        // 

        public List<PageTBx> GetListEmailPage()
        {
            try
            {
                return DB.PageTBxes.Where(e => e.ID <= 3).ToList();
            }
            catch (Exception)
            {
                return new List<PageTBx>();
            }
        }

        public List<PageTBx> GetListEditPage()
        {
            try
            {
                return DB.PageTBxes.Where(e => e.ID > 3 && e.Isdelete == false).ToList();
            }
            catch (Exception)
            {
                return new List<PageTBx>();
            }
        }
    }
}