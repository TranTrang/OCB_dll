﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OCB.AppCode.Manager
{
    public class BlogManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();

        public BlogManager()
        {
        }

        // ===============================================
        // Basic Methods

        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(BlogTBx element)
        {
            DB.BlogTBxes.InsertOnSubmit(element);
            Save();
        }

        public BlogTBx GetByID(int ID)
        {
            try
            {
                return DB.BlogTBxes.Where(e => e.ID == ID && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return new BlogTBx();
            }
        }

        public List<BlogTBx> GetList()
        {
            try
            {
                return DB.BlogTBxes.Where(e => e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<BlogTBx>();
            }
        }

        // ===============================================================================================
        // This Manager Methods
        // 



    }
}