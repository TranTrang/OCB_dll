﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SurveyManager
/// </summary>
namespace OCB.AppCode.Manager
{
    public class SurveyManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();

        public SurveyManager()
        {
        }

        // ===============================================
        // Basic Methods

        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(SurveyTBx element)
        {
            DB.SurveyTBxes.InsertOnSubmit(element);
            Save();
        }

        public SurveyTBx GetByID(int ID)
        {
            try
            {
                return DB.SurveyTBxes.Where(e => e.ID == ID && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return new SurveyTBx();
            }
        }

        public List<SurveyTBx> GetList()
        {
            try
            {
                return DB.SurveyTBxes.Where(e => e.IsDeleted == false).OrderBy(e => e.Order).ToList();
            }
            catch (Exception)
            {
                return new List<SurveyTBx>();
            }
        }
        public List<SurveyTBx> GetListStep(int step)
        {
            try
            {
                return DB.SurveyTBxes.Where(e => e.IsDeleted == false && e.step == step && e.IsPublish == true).OrderBy(e => e.Order).ToList();
            }
            catch (Exception)
            {
                return new List<SurveyTBx>();
            }
        }
        public List<SurveyTBx> SearchText(string strSearch)
        {
            try
            {
                return DB.SurveyTBxes.Where(e => e.Text.ToLower().Contains(strSearch.ToLower()) && e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<SurveyTBx>();
            }
        }

        // ===============================================================================================
        // This Manager Methods
        // 
        public List<SurveyTBx> GetListSurveysUploadImage()
        {
            try
            {
                return DB.SurveyTBxes.Where(e => e.ID == 87 || e.ID == 88 || e.ID == 89 && e.IsDeleted == false).OrderBy(e => e.Order).ToList();
            }
            catch (Exception)
            {
                return new List<SurveyTBx>();
            }
        }
    }
}