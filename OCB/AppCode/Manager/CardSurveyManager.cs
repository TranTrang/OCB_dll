﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CardSurveyManager
/// </summary>
/// 
namespace OCB.AppCode.Manager
{
    public class CardSurveyManager
    {
        DataClassesDataContext DB = new DataClassesDataContext();

        public CardSurveyManager()
        {
        }

        // ===============================================
        // Basic Methods

        public void Save()
        {
            DB.SubmitChanges();
        }

        public void Add(CardSurveyTBx element)
        {
            DB.CardSurveyTBxes.InsertOnSubmit(element);
            Save();
        }

        /// <summary>
        /// Get element by ID with status != -1
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public CardSurveyTBx GetByID(int ID)
        {
            try
            {
                return DB.CardSurveyTBxes.Where(e => e.ID == ID && e.IsDeleted == false).First();
            }
            catch (Exception)
            {
                return new CardSurveyTBx();
            }
        }

        /// <summary>
        /// Get list all with status != -1
        /// </summary>
        /// <returns></returns>
        public List<CardSurveyTBx> GetListByCardID(int cardID)
        {
            try
            {
                return DB.CardSurveyTBxes.Where(e => e.CardID == cardID && e.IsDeleted == false).ToList();
            }
            catch (Exception)
            {
                return new List<CardSurveyTBx>();
            }
        }

        // ===============================================================================================
        // This Manager Methods
        // 

        public void Delete(List<CardSurveyTBx> listElement)
        {
            DB.CardSurveyTBxes.DeleteAllOnSubmit(listElement);
            Save();
        }

        public void Add(List<CardSurveyTBx> listElement)
        {
            DB.CardSurveyTBxes.InsertAllOnSubmit(listElement);
            Save();
        }
    }
}