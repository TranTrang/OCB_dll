﻿using OCB.AppCode;

public class CPSession
{
    // "cp-session"
    public SessionType sessionType;
    public Admin admin;
    public int userID;
}

public enum SessionType
{
    admin = 0,
    user = 1,
}