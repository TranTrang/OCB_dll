﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BaseAdminPage
/// </summary>
public abstract class BaseAdminPage : System.Web.UI.Page
{
    public bool isAdmin = false;
    private CPSession cpSession = new CPSession();

    protected void InitPage()
    {
        try
        {
            cpSession = ((CPSession)Session["cp-session"]);
            if (cpSession.sessionType == SessionType.admin)
            {
                isAdmin = true;
                ActionAdminPage();
            }
            else if (cpSession.sessionType == SessionType.user)
            {
                //ActionUserPage();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }

    protected CPSession GetCPSession()
    {
        return cpSession;
    }

    // ===========================================================
    // abstract methods

    protected abstract void ActionAdminPage();
    //protected abstract void ActionUserPage();
}