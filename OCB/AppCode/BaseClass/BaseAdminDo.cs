﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BaseAdminDo
/// </summary>
public abstract class BaseAdminDo : System.Web.UI.Page
{
    public bool isAdmin = false;
    private CPSession cpSession = new CPSession();

    protected void InitDo()
    {
        try
        {
            cpSession = ((CPSession)Session["cp-session"]);
            if (cpSession == null)
            {
                ShowWhenMissingSession();
            }
            else
            {
                if (cpSession.sessionType == SessionType.admin)
                {
                    isAdmin = true;
                    ActionAdminDo();
                }
                else if (cpSession.sessionType == SessionType.user)
                {
                    //ActionUserDo();
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }

    public void ShowWhenMissingSession()
    {
        Response.Redirect("/cp-login");
    }

    protected CPSession GetCPSession()
    {
        return cpSession;
    }

    // ===========================================================
    // abstract methods

    protected abstract void ActionAdminDo();
    //protected abstract void ActionUserDo();
}