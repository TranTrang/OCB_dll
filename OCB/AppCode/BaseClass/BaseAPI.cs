﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

/// <summary>
/// Summary description for BaseAPI
/// </summary>
public abstract class BaseAPI : IHttpHandler
{
    protected string token = "";
    protected HttpContext context;

    public BaseAPI()
    {
    }

    protected void InitAPI(HttpContext context)
    {
        this.context = context;
        this.context.Response.AppendHeader("Access-Control-Allow-Origin", "*");
        this.context.Response.ContentType = "text/plain";

        try
        {
            // request token
            token = this.context.Request["token"];
            if (string.IsNullOrEmpty(token) || string.IsNullOrWhiteSpace(token))
            {
                MissingToken();
                return;
            }

            //// get user
            //UserManager UM = new UserManager();
            //UserTBx user = UM.GetByToken(token);

            //if (user.ID == 0)
            //{
            //    IncorrectToken();
            //    return;
            //}
            //else // ID != 0 --> got user
            //{
            //    Action(user.ID);
            //}
        }
        catch (Exception ex)
        {
            ReturnException(ex);
        }
    }

    protected void APIResponse(int status, string data, int errorStatus, string error)
    {
        context.Response.Write(JsonConvert.SerializeObject
            (
                new
                {
                    status = status,
                    data = data,
                    errorStatus = errorStatus,
                    error = error,
                }
            )
        );
    }

    // =======================================================
    // Exception methods

    protected void IncorrectToken()
    {
        APIResponse(0, "User not exist!", 0, "");
    }

    protected void MissingToken()
    {
        APIResponse(0, "Missing token!", 0, "");
    }

    protected void ReturnException(Exception ex)
    {
        APIResponse(-1, ex.ToString(), 0, "");
    }

    // =======================================================
    // Abtract methods

    public bool IsReusable { get{return false; }}
    public abstract void ProcessRequest(HttpContext context);

    protected abstract void Action(int userID);
}