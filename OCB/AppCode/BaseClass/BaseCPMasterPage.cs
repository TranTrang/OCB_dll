﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BaseCPMasterPage
/// </summary>
public abstract class BaseCPMasterPage : System.Web.UI.MasterPage
{
    public bool isAdmin = false;
    private CPSession cpSession = new CPSession();

    protected void InitMasterPage()
    {
        try
        {
            cpSession = (CPSession)Session["cp-session"];
            if (cpSession == null)
            {
                ShowWhenMissingSession();
            }
            else // got session
            {
                if (cpSession.sessionType == SessionType.admin)
                {
                    isAdmin = true;
                    ActionAdminMasterPage();
                }
                else if (cpSession.sessionType == SessionType.user)
                {
                    // action user master-page
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }

    public void ShowWhenMissingSession()
    {
        Response.Redirect("/cp-login");
    }

    public CPSession GetCPSession()
    {
        return cpSession;
    }
    // ===========================================================
    // abstract methods

    protected abstract void ActionAdminMasterPage();
    //protected abstract void ActionUserMasterPage();
}