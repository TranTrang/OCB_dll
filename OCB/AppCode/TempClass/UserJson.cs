﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserJson
/// </summary>
public class UserJSON
{
    string key;
    string value;
 
    public UserJSON()
    {

    }

    public string Key
    {
        get
        {
            return key;
        }

        set
        {
            key = value;
        }
    }

    public string Value
    {
        get
        {
            return value;
        }

        set
        {
            this.value = value;
        }
    }

    
}