﻿using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.api
{
    public partial class deleteImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                UserProfileImageManager UPI = new UserProfileImageManager();
                UPI.RemoveFileImageUser(Convert.ToInt32(Request["id"]));

                string script = "alert(\"Xóa ảnh thành công!\");";
                ScriptManager.RegisterStartupScript(this, GetType(),
                                      "ServerControlScript", script, true);
            }
            catch
            {
                string script = "alert(\"Xóa ảnh thất bại!\");";
                ScriptManager.RegisterStartupScript(this, GetType(),
                                      "ServerControlScript", script, true);
            }
        }
    }
}