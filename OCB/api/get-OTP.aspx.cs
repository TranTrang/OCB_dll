﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace OCB.api
{
    public partial class get_OTP : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string phone = Request["phone"];
            string branchCode = Request["branchCode"];
            string cRefNumIn = "123";
            string userID = Request["userID"];

            string url = "http://10.96.23.111:8800/transactionauthorization/v1/ws?wsdl";
            StringBuilder requestXML = new StringBuilder();
            requestXML.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tran='http://bian.com.vn/TransactionAuthorization/'>");
            requestXML.Append("<soapenv:Header/>");
            requestXML.Append("<soapenv:Body>");
            requestXML.Append("<tran:initOTPNoAccountCustomer_in>");
            requestXML.Append("<transactionInfo>");
            requestXML.AppendFormat("<cRefNum>{0}</cRefNum>", cRefNumIn);
            requestXML.AppendFormat("<userId>{0}</userId>", userID);
            requestXML.Append("<clientCode>LandingPage</clientCode>");
            requestXML.Append("<branchInfo>");
            requestXML.AppendFormat("<branchCode>{0}</branchCode>", branchCode);
            requestXML.Append("</branchInfo>");
            requestXML.Append("</transactionInfo>");
            requestXML.Append("<OTPData>");
            requestXML.AppendFormat("<phoneNumber>{0}</phoneNumber>", phone);
            requestXML.Append("</OTPData>");
            requestXML.Append("</tran:initOTPNoAccountCustomer_in>");
            requestXML.Append("</soapenv:Body>");
            requestXML.Append("</soapenv:Envelope>");
            string responseXML = postXMLData(url, requestXML.ToString());

            XDocument xd = XDocument.Parse(responseXML.ToString());

            var result = new
            {
                //Mã giao dịch phát sinh bởi Client
                cRefNumOut = xd.Descendants("cRefNum").FirstOrDefault().Value,
                //Mã giao dịch phát sinh bởi hệ thống ESB
                pRefNum = xd.Descendants("pRefNum").FirstOrDefault().Value,
                //Thời gian giao dịch hoàn tất thực hiện
                transactionCompletedTime = xd.Descendants("transactionCompletedTime").FirstOrDefault().Value,
                //Mã trả về sử dụng để xác định kết quả thực hiện giao dịch
                //90: Thành công
                //Khác: Lỗi
                transactionErrorCode = xd.Descendants("transactionErrorCode").FirstOrDefault().Value,
                //Tình trạng của giao dịch
                //EXECUTED: Đã gửi OTP
                operationStatus = xd.Descendants("response").FirstOrDefault().Descendants("operationStatus").FirstOrDefault().Value,
                //Id giao dịch xác thực
                authId = xd.Descendants("response").FirstOrDefault().Descendants("authId").FirstOrDefault().Value,
                //Ngày phát sinh giao dịch
                operationDate = xd.Descendants("response").FirstOrDefault().Descendants("operationDate").FirstOrDefault().Value
            };

            Response.Write(JsonConvert.SerializeObject(result));
        }
        public string postXMLData(string destinationUrl, string requestXml)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            byte[] bytes = Encoding.UTF8.GetBytes(requestXml);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Timeout = 50000;

            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                return responseStr;
            }
            return null;
        }
    }
}