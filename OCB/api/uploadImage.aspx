﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="uploadImage.aspx.cs" Inherits="OCB.api.uploadImage" %>

<!-- Simple Upload Image v1.0 -->
<script src="../cp/plugins/jquery/jquery-2.2.4.min.js"></script>
<div style="float: left">
    <form id="form1" runat="server">

        <%-- <img runat="server" id="img" width="300" height="200" />--%>
        <p></p>

        <asp:fileupload id="FileUploadControl" runat="server" />

        <asp:button runat="server" id="UploadButton" text="Upload" onclick="UploadButton_Click" />
        <br />
        <br />
        <asp:label runat="server" id="StatusLabel" text="Upload status: " />


        <!-- display all image of the products -->
        <br />



    </form>
</div>
<div style="float: left; margin-left: 30px">
    <% if (proImgList.Count > 0)
        {
            for (int i = 0; i < proImgList.Count; i++)
            {
                pi = proImgList[i];
    %>
    <div style="margin-bottom: 5px" id="img-pr-<%= pi.ID %>">
        <img src="/upload/userInfo/<%=pi.LinkURL%>?date<%=DateTime.Now %>" style="width: 200px" />

        <button onclick="RemoveImage(<%= pi.ID %>)">Remove</button>

    </div>
    <% }
        } %>
</div>

<script>
    function RemoveImage(id) {
        location.href = "/api/deleteImage.aspx?id=" + id;

    }
    $(function () {
        var css = { "background": "green", "color": "#fff", "padding": "10px 20px", "border": "none" };

        for (var prop in css) {
            document.getElementById("<%=UploadButton.ClientID %>").style[prop] = css[prop];
        }
    })



</script>
