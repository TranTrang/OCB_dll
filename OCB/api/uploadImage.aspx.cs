﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB.api
{
    public partial class uploadImage : System.Web.UI.Page
    {
        public List<UserProfileImageTBx> proImgList = new List<UserProfileImageTBx>();
        public UserProfileImageTBx pi;
        string phone = "";
        string type = "";
        UserProfileImageManager UIM = new UserProfileImageManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                phone = Request["phone"];
                type = Request["type"];
                proImgList = UIM.GetListByPhoneNumberAndType(phone, type);
            }

            catch
            {

            }
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (FileUploadControl.HasFile)// check if the file seleced
            {
                try
                {
                    if (FileUploadControl.PostedFile.ContentType == "image/jpeg")// check jpg alowed only
                    {
                        if (FileUploadControl.PostedFile.ContentLength < 5424000) // check size limit
                        {
                            string filename = FileUploadControl.FileName; // create a new file name for the image - Default name of the image
                                                                          //int nextd = proImgList.Count+1;
                                                                          //string filename = FileUploadControl.FileName; // customize the image name

                            /* new code lines 29/06/17 */
                            string ext = System.IO.Path.GetExtension(FileUploadControl.PostedFile.FileName); //get extension name of filename
                            filename = System.IO.Path.GetFileNameWithoutExtension(FileUploadControl.PostedFile.FileName);

                            filename = phone + UTIL.FormatLink(type) + "-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ext;

                            FileUploadControl.SaveAs(Server.MapPath("~/upload/userInfo/") + filename); // SAVE IMAGE!
                            StatusLabel.Text = "Upload status: File uploaded!";
                            //img.Src = "/upload/userInfo/" + filename; // update avatar image


                            UserProfileImageTBx pi = new UserProfileImageTBx();
                            pi.UserPhoneNumber = phone;
                            pi.Type = type;
                            pi.LinkURL = filename;
                            pi.IsDeleted = false;
                            UIM.Add(pi);
                            UIM.Save();
                            proImgList = UIM.GetListByPhoneNumberAndType(phone, type);
                            FileUploadControl.Attributes.Clear();
                        }
                        else
                            StatusLabel.Text = "Upload status: The file has to be less than 5 MB!";
                    }
                    else
                        StatusLabel.Text = "Upload status: Only JPEG files are accepted!";
                }
                catch (Exception ex)
                {
                    // display errors to the label
                    StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                }
            }
        }
    }
}