﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace OCB.api
{
    public partial class check_OTP : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string phone = Request["phone"];
            string branchCode = Request["branchCode"];
            string authID = Request["authID"];
            string valueOTP = Request["valueOTP"];
            string userID = Request["userID"];
            string cRefNumIn = "";
            string url = "http://10.96.23.111:8800/transactionauthorization/v1/ws?wsdl";
            StringBuilder requestXML = new StringBuilder();
            requestXML.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tran='http://bian.com.vn/TransactionAuthorization/'>");
            requestXML.Append("<soapenv:Header/>");
            requestXML.Append("<soapenv:Body>");
            requestXML.Append("<tran:authorizeTransactionOTPNoAccountCustomer_in>");
            requestXML.Append("<transactionInfo>");
            requestXML.AppendFormat("<cRefNum>{0}</cRefNum>", cRefNumIn);
            requestXML.Append("<clientCode>LandingPage</clientCode>");
            requestXML.Append("<branchInfo>");
            requestXML.AppendFormat("<branchCode>{0}</branchCode>", branchCode);
            requestXML.Append("</branchInfo>");
            requestXML.Append("</transactionInfo>");
            requestXML.Append("<OTPData>");
            requestXML.AppendFormat("<phoneNumber>{0}</phoneNumber>", phone);
            requestXML.AppendFormat("<authId>{0}</authId>", authID);
            requestXML.AppendFormat("<value>{0}</value>", valueOTP);
            requestXML.Append("</OTPData>");
            requestXML.Append("</tran:authorizeTransactionOTPNoAccountCustomer_in>");
            requestXML.Append("</soapenv:Body>");
            requestXML.Append("</soapenv:Envelope>");

            string responseXML = postXMLData(url, requestXML.ToString());
            XDocument xd = XDocument.Parse(responseXML);
            var result = new
            {
                //Mã giao dịch phát sinh bởi Client
                cRefNum = xd.Descendants("cRefNum").FirstOrDefault().Value,
                //Mã giao dịch phát sinh bởi hệ thống ESB
                pRefNum = xd.Descendants("pRefNum").FirstOrDefault().Value,
                //Thời gian giao dịch hoàn tất thực hiện
                transactionCompletedTime = xd.Descendants("transactionCompletedTime").FirstOrDefault().Value,
                //Mã trả về sử dụng để xác định kết quả thực hiện giao dịch
                //Lưu ý: Không dùng mã này để xác định OTP hợp lệ hay không
                transactionErrorCode = xd.Descendants("transactionErrorCode").FirstOrDefault().Value,
                //Tình trạng của giao dịch
                //INCORRECT: OTP không đúng
                //CORRECT: OTP hợp lệ
                operationStatus = xd.Descendants("authorizationResponse").FirstOrDefault().Descendants("operationStatus").FirstOrDefault().Value
            };
            Response.Write(JsonConvert.SerializeObject(result));
        }

        public string postXMLData(string destinationUrl, string requestXml)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            byte[] bytes = Encoding.ASCII.GetBytes(requestXml);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                return responseStr;
            }
            return null;
        }
    }
}