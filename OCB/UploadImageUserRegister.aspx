﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="UploadImageUserRegister.aspx.cs" Inherits="OCB.UploadImageUserRegister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="box-form" data-wow-duration="2s" data-wow-delay="0.01s" id="box-form">
        <div class="container">
            <div class="row item-form">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="title-form text-center">
                        <h3 class="color-green-dark">Bạn thật cừ! Thẻ đã là của bạn rồi !</h3>
                        <h4>Chỉ bước cuối cùng này thôi, gửi hồ sơ cho Thẻ nào</h4>
                    </div>
                </div>
            </div>
            <div class="row dl-margin-form">
                <div class="col-xs-12 col-sm-12 col-md-12 box-number-form  box-number-form-cs ">
                    <div class="box-number">
                        <span class="number-form" id="number1">1</span>
                        <p class="hidden-xs">Thông tin liên hệ</p>
                    </div>
                    <span class="line hidden-md hidden-sm " id=""></span>
                    <br />
                    <div class="box-number">
                        <span class="number-form" id="number2">2</span>
                        <p class="hidden-xs">Thông tin cá nhân</p>
                    </div>

                    <div class="box-number">
                        <span class="number-form" id="number3">3</span>
                        <p class="hidden-xs">Thông tin nghề nghiệp</p>

                    </div>

                    <div class="box-number">
                        <span class="number-form" id="number4">4</span>
                        <p class="hidden-xs">Xác nhận thông tin</p>
                    </div>
                </div>

            </div>
            <div class="grid-container-upload text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="box-upload">
                                <h6><strong>HỒ SƠ CÁ NHÂN</strong> <span class="color-orange"><i class="fas fa-info-circle"></i>Hướng dẫn </span></h6>
                                <% if (surveyUserFile.Type == "S")
                                    {
                                        listOption = SIM.GetListOption(surveyUserFile.ID);
                                %>

                                <select class="sl-item survey  require_<%=surveyUserFile.Required %>" id="slx_<%=surveyUserFile.ID%>" data-error="<%=surveyUserFile.Code %>">
                                    <option value=""><%=surveyUserFile.Code%></option>
                                    <%

                                        for (int k = 0; k < listOption.Count; k++)
                                        {%>
                                    <option value="<%=listOption[k].Value%>"><%=listOption[k].Value%></option>
                                    <% }
                                        }
                                    %>
                                </select>
                                <iframe class="iframeFileUser" src="" frameborder="0" style="width: 100%; max-height: 500px"></iframe>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="box-upload">
                                <h6><strong>HỒ SƠ NƠI Ở</strong> <span class="color-orange"><i class="fas fa-info-circle"></i>Hướng dẫn </span></h6>
                                <% if (surveyAddressFile.Type == "S")
                                    {
                                        listOption = SIM.GetListOption(surveyAddressFile.ID);
                                %>

                                <select class="sl-item survey require_<%=surveyUserFile.Required %>" id="slx_<%=surveyAddressFile.ID%>" data-error="<%=surveyAddressFile.Code %>">
                                    <option value=""><%=surveyAddressFile.Code%></option>
                                    <%

                                        for (int k = 0; k < listOption.Count; k++)
                                        {%>
                                    <option value="<%=listOption[k].Value%>"><%=listOption[k].Value%></option>
                                    <% }
                                        }
                                    %>
                                </select>
                                <iframe class="iframeFileAddress" src="" frameborder="0" style="width: 100%; max-height: 500px"></iframe>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="box-upload">
                                <h6><strong>HỒ SƠ TÀI CHÍNH</strong> <span class="color-orange"><i class="fas fa-info-circle"></i>Hướng dẫn </span></h6>
                                <% if (surveyUserFile.Type == "S")
                                    {
                                        listOption = SIM.GetListOption(surveyAccount.ID);
                                %>

                                <select class="sl-item survey require_<%=surveyUserFile.Required %>" id="slx_<%=surveyAccount.ID%>" data-error="<%=surveyAccount.Code %>">
                                    <option value=""><%=surveyAccount.Code%></option>
                                    <%

                                        for (int k = 0; k < listOption.Count; k++)
                                        {%>
                                    <option value="<%=listOption[k].Value%>"><%=listOption[k].Value%></option>
                                    <% }
                                        }
                                    %>
                                </select>
                                <iframe class="iframeFileAccount" src="" frameborder="0" style="width: 100%; max-height: 500px"></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="box-button">
                                <a href="#" class="color-orange">Tôi sẽ hoàn thiện sau</a>
                                <a class="btn btn-success btn-open" onclick="">HOÀN TẤT ></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        $(function () {
            LoadImageDoc();

            $("#slx_87").change(function () {

                fileUser = $("#slx_87 option:selected").text();

                localStorage.setItem("iframeFileUser", fileUser);

                var linkImage = "/api/uploadImage.aspx?phone=" + localStorage.getItem("phoneuser") + "&type=" + localStorage.getItem("iframeFileUser");
                $(".iframeFileUser").attr("src", linkImage);

            });
            $("#slx_88").change(function () {
                fileAddress = $("#slx_88 option:selected").text();
                localStorage.setItem("iframeFileAddress", fileAddress);
                var linkImage = "/api/uploadImage.aspx?phone=" + localStorage.getItem("phoneuser") + "&type=" + localStorage.getItem("iframeFileAddress");
                $(".iframeFileAddress").attr("src", linkImage);
            });
            $("#slx_89").change(function () {
                fileAccount = $("#slx_89 option:selected").text();
                localStorage.setItem("iframeFileAccount", fileAccount);
                var linkImage = "/api/uploadImage.aspx?phone=" + localStorage.getItem("phoneuser") + "&type=" + localStorage.getItem("iframeFileAccount");
                $(".iframeFileAccount").attr("src", linkImage);
            });

        });

        function LoadImageDoc() {
            fileUser = $("#slx_87 option:selected").text();

            localStorage.setItem("iframeFileUser", fileUser);

            var linkImage = "/api/uploadImage.aspx?phone=" + localStorage.getItem("phoneuser") + "&type=" + fileUser;
            $(".iframeFileUser").attr("src", linkImage);




            fileAddress = $("#slx_88 option:selected").text();
            localStorage.setItem("iframeFileAddress", fileAddress);
            var linkImage = "/api/uploadImage.aspx?phone=" + localStorage.getItem("phoneuser") + "&type=" + localStorage.getItem("fileAddress");
            $(".iframeFileAddress").attr("src", linkImage);

            fileAccount = $("#slx_89 option:selected").text();
            localStorage.setItem("iframeFileAccount", fileAccount);
            var linkImage = "/api/uploadImage.aspx?phone=" + localStorage.getItem("phoneuser") + "&type=" + localStorage.getItem("fileAccount");
            $(".iframeFileAccount").attr("src", linkImage);
        }


    </script>
</asp:Content>
