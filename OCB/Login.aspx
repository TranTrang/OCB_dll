﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="OCB.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="box-thank text-center">
                    <h3 class="color-green-dark">Đăng nhập</h3>
                    <div class="form-login">
                        <input type="text" id="txtphone" placeholder="Nhập số điện thoại" />
                        <a href="/comfirm-info">
                            <div class="box-login">
                                Lấy mã xác thực OTP
                            <i class="fas fa-arrow-right"></i>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
        // active button
        $("#txtphone").blur(function () {
            $(".box-login").css("background", "#ffdb4d");
        });
    </script>
</asp:Content>


