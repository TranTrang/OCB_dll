﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="OCB.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="/plugin/range-slider/asRange.css" rel="stylesheet" />
    <script src="/plugin/range-slider/jquery-asRange.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <header class="header-page">
        <div class="container-fluid">
            <div class="row  box-logo-pass-menu">
                <div class="col-xs-12 col-sm-5 col-md-5">
                    <div class="row m-row-pass">
                        <div class="col-xs-2 hidden-sm hidden-md hidden-lg">
                            <a href="#">
                                <div class="icon-bar-mobile">
                                    <i class="fas fa-bars"></i>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-7 col-sm-5 col-md-5 col-lg-5">
                            <a href="#">
                                <div class="img-logo">
                                    <img src="/images/OCB/logo.png" class="img-responsive" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 list-menu">
                    <nav>
                        <ul class="list-menu-parent">
                            <li><a href="#" class="nunito-bold">OCB OMNI</a></li>
                            <li><a href="#" class="nunito-bold">Tín dụng</a></li>
                            <li><a href="#" class="nunito-bold">Vay</a></li>
                            <li><a href="#" class="nunito-bold">Ưu đãi</a></li>
                            <li><a href="#" class="nunito-bold">Mr.Tài chính</a></li>
                            <li><a href="#" class="nunito-bold">FAQ</a></li>
                            <li><a href="/my-account" class="nunito-bold">My Account</a></li>

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <section class="box-banner">
        <div class="img-banner">
            <a href="#">
                <img src="/images/index_v2/top-banner.png" class="img-responsive " data-wow-duration="10s" data-wow-delay="0.1s" />
            </a>
        </div>
        <div class="container">
            <div class="row mt-30">
                <h3 class="gotham-book text-center text-green h3__edited">Giao dịch nhanh không lo gián đoạn. Mọi lúc. Mọi nơi.
                </h3>
                <h4 class="san-francisco text-center text-light-gray h4__edited">Trải nghiệm ngay Ngân hàng số hợp kênh đầu tiên tại Việt Nam để cảm nhận
                </h4>
            </div>
            <div class="row mt-20">
                <img src="/images/index_v2/img-1.png" class="img-responsive center-block w-80" />
            </div>
            <div class="row mt-20">
                <div class="col-md-3 col-lg-3 col-xs-12 col-md-offset-3 col-lg-offset-3">
                    <ul class="check-list san-francisco">
                        <li>Không phí mở tài khoản</li>
                        <li>Không phí chuyển tiền</li>
                        <li>Không phí thường niên</li>
                    </ul>
                </div>
                <div class="col-md-6 col-lg-6 col-xs-12">
                    <ul class="check-list san-francisco">
                        <li>Chỉ 1 cú chạm quản lý toàn bộ tình hình tài chính</li>
                        <li>Chỉ 1 OTP cho hàng ngàn giao dịch</li>
                    </ul>
                </div>
            </div>
            <div class="row mt-20">
                <h2 class="text-center san-francisco"><span class="text-green">30s</span> <span class="text-light-gray">CÓ NGAY TÀI KHOẢN</span></h2>
                <button class="button__yellow button--outline center-block mt-10 san-francisco">Mở tài khoản ngay</button>
            </div>
            <div class="row mt-30">
                <img src="/images/index_v2/card.png" class="img-responsive center-block mt-20" />
            </div>
            <div class="row mt-30">
                <h2 class="text-right san-francisco">
                    <span class="text-light-gray">CHỈ</span> <span class="text-green">2 NGÀY</span> <span class="text-light-gray">CÓ NGAY THẺ TRONG TAY</span>
                    <button class="button__yellow button--outline button--inline-block button--short center-block mt-10 san-francisco">Mở thẻ ngay</button>
                </h2>
            </div>
            <div class="row mt-30">
                <h3 class="gotham-book text-center text-green h3__edited">Thực hiện ước mơ không còn là mơ ước
                </h3>
                <h4 class="san-francisco text-center text-light-gray h4__edited">Vay tiêu dùng dễ dàng với lãi suất cạnh tranh nhất
                </h4>
            </div>
            <div class="row mt-30">
                <img src="/images/index_v2/lai-suat.png" class="img-responsive center-block" />
            </div>
            <div class="row mt-50">
                <div class="col-md-4 col-lg-4 col-xs-5 col-md-offset-2 col-lg-offset-2">
                    <h4 class="text-light-gray san-francisco">Tiền vay</h4>
                    <h3 class="text-green gotham-book" id="txtLoan">10,000,000 vnd</h3>
                    <input class="range-input mt-10" type="text" value="10000000" id="loan" />
                </div>
                <div class="col-md-4 col-lg-4 col-xs-5">
                    <h4 class="text-light-gray san-francisco">Thời gian vay</h4>
                    <h3 class="text-green gotham-book" id="txtPeriod">1 tháng</h3>
                    <input class="range-input mt-10" type="text" value="1" id="period" />
                </div>
            </div>
            <div class="row mt-30">
                <div class="col-md-4 col-lg-4 col-xs-5 col-md-offset-2 col-lg-offset-2">
                    <h4 class="text-light-gray san-francisco">Tổng số tiền trả</h4>
                    <h3 class="text-green gotham-book">10,015,000 vnd</h3>
                </div>
                <div class="col-md-4 col-lg-4 col-xs-5">
                    <h4 class="text-light-gray san-francisco">Trả hàng tháng</h4>
                    <h3 class="text-green gotham-book">10,015,000 vnd</h3>
                </div>
                <div class="col-md-2 col-lg-2 col-xs-2">
                    <h4 class="text-light-gray san-francisco">Lãi năm</h4>
                    <h3 class="text-green gotham-book">10%</h3>
                </div>
            </div>
            <div class="row mt-30">
                <h3 class="text-center text-light-gray h3__edited san-francisco">THỦ TỤC ĐƠN GIẢN NHANH CHÓNG<br />
                    GIẢI NGÂN CẤP TỐC <span class="text-green">TRONG 2 GIỜ</span>
                </h3>
                <button class="button__yellow button--outline button--short center-block mt-20 san-francisco">Vay ngay</button>
            </div>
            <div class="row mt-30">
                <h3 class="gotham-book text-center text-green h3__edited">Tích lũy để sẵn sàng cho cả một hành trình
                </h3>
                <h4 class="san-francisco text-center text-light-gray h4__edited">Mở sổ tiết kiệm online dễ dàng, nhanh chóng. Lãi suất tiền gửi cao lên đến 8%
                </h4>
            </div>
            <div class="row mt-50 relative">
                <h3 class="text-center text-light-gray h3__edited san-francisco text--absolute">ĐẶT MỤC TIÊU TIẾT KIỆM <span class="text-green">NGAY HÔM NAY</span>
                </h3>
                <button class="button__yellow button--outline center-block mt-10 san-francisco absolute button-absolute">Mở sổ tiết kiệm</button>
                <img src="/images/index_v2/map.png" class="img-responsive center-block mt-50" />
            </div>
            <div class="row mt-50">
                <img src="/images/index_v2/ocb-omni-360.png" class="center-block" />
                <h4 class="text-light-gray text-center">Cẩm nang hướng dẫn sử dụng</h4>
            </div>
            <div class="row mt-50">
                <div class="col-md-12 col-sm-12 col-xs 12">
                    <section class="slider">
                        <div class="item">
                            <img src="/images/index_v2/clip-demo.png" class="img-responsive center-block" />
                            <h4 class="text-center mt-20"><b>Clip hướng dẫn sử dụng</b></h4>
                        </div>
                        <div class="item">
                            <img src="/images/index_v2/clip-demo.png" class="img-responsive center-block" />
                            <h4 class="text-center mt-20"><b>Clip 3S</b></h4>
                        </div>
                        <div class="item">
                            <img src="/images/index_v2/clip-demo.png" class="img-responsive center-block" />
                            <h4 class="text-center mt-20"><b>Clip tính năng</b></h4>
                        </div>
                        <div class="item">
                            <img src="/images/index_v2/clip-demo.png" class="img-responsive center-block" />
                            <h4 class="text-center mt-20"><b>Clip hướng dẫn sử dụng</b></h4>
                        </div>
                        <div class="item">
                            <img src="/images/index_v2/clip-demo.png" class="img-responsive center-block" />
                            <h4 class="text-center mt-20"><b>Clip 3S</b></h4>
                        </div>
                        <div class="item">
                            <img src="/images/index_v2/clip-demo.png" class="img-responsive center-block" />
                            <h4 class="text-center mt-20"><b>Clip tính năng</b></h4>
                        </div>
                    </section>
                </div>
            </div>
            <div class="divider__green center-block mt-50"></div>
            <div class="row mt-30">
                <h3 class="text-center text-green h3__edited gotham-book">Chương trình ưu đãi khác</h3>
            </div>
            <div class="row mt-50">
                <div class="col-md-12 col-sm-12 col-xs 12">
                    <section class="slider">
                        <div class="item">
                            <img src="/images/index_v2/promotion-1.png" class="img-responsive center-block" />
                            <h4 class="text-center mt-20 text-light-gray">Tiết kiệm thông minh cùng OCB OMNI: Hưởng mức lãi suất cộng thêm 0,4%, lãi suất tối đa đến 8%</h4>
                            <div class="text-center">
                                <a class="link__yellow">Xem ngay</a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/images/index_v2/promotion-2.png" class="img-responsive center-block" />
                            <h4 class="text-center mt-20 text-light-gray">Tặng 100k cho 10.000 khách hàng OCB mở tài khoản OCB Omni trên online</h4>
                            <div class="text-center">
                                <a class="link__yellow">Xem ngay</a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/images/index_v2/promotion-3.png" class="img-responsive center-block" />
                            <h4 class="text-center mt-20 text-light-gray">Vui lướt Shopee nhận voucher mua sắm từ OCB Omni trị giá 88k</h4>
                            <div class="text-center">
                                <a class="link__yellow">Xem ngay</a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/images/index_v2/promotion-1.png" class="img-responsive center-block" />
                            <h4 class="text-center mt-20 text-light-gray">Tiết kiệm thông minh cùng OCB OMNI: Hưởng mức lãi suất cộng thêm 0,4%, lãi suất tối đa đến 8%</h4>
                            <div class="text-center">
                                <a class="link__yellow">Xem ngay</a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/images/index_v2/promotion-2.png" class="img-responsive center-block" />
                            <h4 class="text-center mt-20 text-light-gray">Tặng 100k cho 10.000 khách hàng OCB mở tài khoản OCB Omni trên online</h4>
                            <div class="text-center">
                                <a class="link__yellow">Xem ngay</a>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/images/index_v2/promotion-3.png" class="img-responsive center-block" />
                            <h4 class="text-center mt-20 text-light-gray">Vui lướt Shopee nhận voucher mua sắm từ OCB Omni trị giá 88k</h4>
                            <div class="text-center">
                                <a class="link__yellow">Xem ngay</a>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(function () {
            $('.slider').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            arrows: true,
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: true,
                            slidesToShow: 1
                        }
                    }
                ]
            });

            $("#loan").asRange({
                step: 10000000,
                range: false,
                min: 0,
                max: 100000000,
                tip: false
            });

            $("#period").asRange({
                step: 1,
                range: false,
                min: 0,
                max: 12,
                tip: false
            });
        });

        // range slider change function
        $("#loan").on('asRange::change', function (e) {
            var value = $('#loan').asRange('get');
            var result = value.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,');
            result = result.replace('.0', '');

            $('#txtLoan').html(result + " vnd");
        });

        $("#period").on('asRange::change', function (e) {
            var value = $('#period').asRange('get');

            $('#txtPeriod').html(value + " tháng");
        });
    </script>
</asp:Content>

