﻿using OCB.AppCode;
using OCB.AppCode.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OCB
{
    public partial class UploadImageUserRegister : System.Web.UI.Page
    {
        public SurveyTBx surveyUserFile = new SurveyTBx();
        public SurveyTBx surveyAddressFile = new SurveyTBx();
        public SurveyTBx surveyAccount = new SurveyTBx();
        public List<SurveyItemValueTBx> listOption = new List<SurveyItemValueTBx>();
        //public List<SurveyItemValueTBx> listOptionAddress = new List<SurveyItemValueTBx>();
        //public List<SurveyItemValueTBx> listOptionAccount = new List<SurveyItemValueTBx>();
        public SurveyItemValueManager SIM = new SurveyItemValueManager();
        int id = 0;
        UserProfileImageManager UPIM = new UserProfileImageManager();
        UserProfileImageTBx userProfile = new UserProfileImageTBx();
        public List<UserProfileImageTBx> proImgList = new List<UserProfileImageTBx>();
        protected void Page_Load(object sender, EventArgs e)
        {
            SurveyManager SM = new SurveyManager();
            surveyUserFile = SM.GetByID(87);
            surveyAddressFile = SM.GetByID(88);
            surveyAccount = SM.GetByID(89);
        }
    }
}