﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BlogDetail.aspx.cs" Inherits="OCB.BlogDetail" MasterPageFile="~/MasterPageBlog.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="box-blog">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <header class="box-share">
                        <h4 class="title-share">Chia sẻ </h4>
                        <div class="img-circle-fb">
                            <img src="/images/blog/fb_orange.jpg" class="img-responsive" />
                        </div>
                    </header>
                </div>
            </div>
        </div>
        <%if (blogdetailID == 1)
            { %>
        <div class="container">
            
            <div class="head-blog text-center">
                <span class="color-gray-dark text-center">29/07/2018</span>
                <h3>Bạn biết gì về OMNI Channel
                </h3>
            </div>
            <section class="blog-detail-new">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="img-new-blog">
                            <img src="/images/blog/bai1anh2.jpg" class="img-responsive" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <section class="content-blog-detail">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <p>
                                        Bạn biết gì về ngân hàng hợp kênh OMNI? Bạn được gì khi sử dụng OMNI?
                                        Bạn đã từng nghe và thắc mắc về Omni Channel và ngân hàng hợp kênh? Bạn được gì khi sử dụng Omni? Bài viết này sẽ mang đến cho bạn cái nhìn cơ bản về sự khác nhau giữa ngân hàng đa kênh và hợp kênh, cùng những đặc điểm nổi bật chỉ có tại Omni.
                                        Omni Channel là gì?
                                        Những năm gần đây, khái niệm Digital Banking được nhắc đến rất nhiều và có thể hiểu đơn giản đây là quá trình số hóa ngân hàng truyền thống (traditional bank) thành ngân hàng số (digital bank). Digital bank là xu thế tất yếu với mục tiêu số hóa tối đa toàn bộ quy trình vận hành và phát triển, để cung cấp cho khách hàng những tiện ích và trải nghiệm sản phẩm dịch vụ ngân hàng hiện đại.

                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <div class="head-blog text-center">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <div class="head-blog text-center">
                                        <div class="img-content"> <!-- BLOG DETAIL KO CO KIA--->
                                            <img src="/images/blog/bai1_anh1.jpg" class="img-responsive" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <p>
                                      Đã có nhiều ngân hàng tại Việt Nam ứng dụng các nền tảng công nghệ để triển khai digital banking với nhiều các tiếp cận khác nhau, nhưng tất cả chỉ dừng lại ở công nghệ đa kênh (Multi-Channel). Theo đó cung cấp cho khách hàng nhiều nhất có thể các kênh tương tác và giao dịch như trên internet banking, mobile banking…
                                      Công nghệ hợp kênh (Omni-Channel) chính là bước phát triển cao hơn của công nghệ đa kênh và là công nghệ tiên tiến nhất hiện nay. Điểm khác biệt và nổi trội của Omni-Channel là cung cấp những trải nghiệm liền mạch cho người dùng, bất kể đang giao dịch ở kênh hay thiết bị nào.

                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <div class="head-blog text-center">
                                        <h3>Bạn được gì khi sử dụng OMNI?</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <ul class="list-omni">
                                        <li>	Giao dịch liền mạch, không bị ngắt quãng </li>
                                        <li>	Tự do lựa chọn kênh giao dịch, dễ dàng trải nghiệm và quản lý tài chính mọi lúc mọi nơi </li>
                                        <li>	Thao tác đơn giản, an toàn tuyệt đối </li>
                                    </ul>
                                    <p>Bạn có thể bắt đầu giao dịch từ kênh thứ nhất (vị dụ trên internet), tiếp tục ở kênh khác (ví dụ trên ứng dụng điện thoại) và kết thúc tại một kênh khác bất kỳ (ví dụ chi nhánh ngân hàng). Do đó, giao dịch với ngân hàng không bị giới hạn trong một kênh duy nhất hay trong một thời gian nhất định, bạn thoải mái lựa chọn kênh và thời gian giao dịch phù hợp nhất với mình. </p>   
                                         
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <div class="head-blog text-center">
                                        <div class="img-content"> <!-- BLOG DETAIL KO CO KIA--->
                                            <img src="/images/blog/bai1anh2.jpg" class="img-responsive" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>Bạn dễ dàng chủ động kiểm soát được tất cả các giao dịch, từ tiết kiệm, vay, thẻ ghi nợ đến thẻ tín dụng, chuyển tiền… trên mọi các kênh giao dịch vì tất cả sản phẩm dịch vụ của ngân hàng đều được phát triển và tích hợp trên tất cả các kênh.
                                    Do Omni Channel là công nghệ hiện đại, tiên tiến nhất hiện nay nên bạn hoàn toàn có thể yên tâm sử dụng vì tính bảo mật đa lớp.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <div class="head-blog text-center">
                                        <div class="img-content"> <!-- BLOG DETAIL KO CO KIA--->
                                            <img src="/images/blog/bai2anh3.jpg" class="img-responsive" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>Tại Việt Nam, Ngân hàng Phương Đông là đơn vị đi đầu trong cuộc cách mạng ngân hàng số, với việc phát triển và mang đến cho người dùng ngân hàng hợp kênh đầu tiên – OCB OMNI. Thừa hưởng những thành tựu của công nghệ hợp kênh tiên tiến trên thế giới, OCB OMNI khẳng định tính tiên phong với 3 S nổi trội: Security (Bảo mật), Simplicity (Tiện nghi) và Seamlessly (Dễ dàng).</p>    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p class="color-red"> Hiện tại, OCB đang miễn phí mở tài khoản, đồng thời tặng 100.000 VND cho khách hàng mới đăng ký mở tài khoản OCB OMNI qua kênh online và phát sinh giao dịch bất kỳ. Nhiều ưu đãi hấp dẫn khác mà bạn sẽ nhận được khi trở thành “người nhà” của OCB OMNI như: miễn phí thường niên, miễn phí chuyển tiền (bao gồm nội bộ, liên ngân hàng và 24/7), tiết kiệm online lãi suất cộng thêm 0.4% so với tại quầy, cùng kỳ hạn gửi và mức lãi suất đa dạng lên đến 8%. 
                                        Bạn có thể đăng ký mở tài khoản ngay tại đây: <a href="https://khachhang.ocb.com.vn/">https://khachhang.ocb.com.vn/ </a>
                                        Nếu bạn đang là khách hàng của OCB, đăng nhập và trải nghiệm ngay tại:<a href=" https://omni.ocb.com.vn"> https://omni.ocb.com.vn</a></p>    
                                </div>
                            </div>

                           

                        </section>  
                    </div>
                </div>
            </section>
        </div>
        <%}
            else if (blogdetailID == 2)
            { %>
                 <div class="container">
            
            <div class="head-blog text-center">
                <span class="color-gray-dark text-center">29/07/2018</span>
                <h3>Gửi tiết kiệm online và những lưu ý bạn cần biết
                </h3>
            </div>
            <section class="blog-detail-new">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="img-new-blog">
                            <img src="/images/blog/bai2_anh1.jpg" class="img-responsive" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <section class="content-blog-detail">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <p>
                                       Bạn có biết, ở nhiều nước trên thế giới, tỷ trọng giao dịch tiết kiệm online lên đến 70%, trong khi ở Việt Nam hiện mới khoảng 10%? Bạn còn e ngại tiết kiệm online không an toàn hay lãi suất không tốt? Bài viết này sẽ giúp bạn giải đáp những khúc mắc đó.
                                    Gửi tiết kiệm online có an toàn?
                                    Vừa gửi tiền vừa run khi gửi tiết kiệm online - nếu bạn có cảm giác như vậy thì cũng không có gì lạ. Bởi khi gửi tiết kiệm trực tuyến, bạn không cầm trong tay bất kỳ một loạt chứng từ nào. Nhưng gửi tiết kiệm trực tuyến an toàn và có nhiều ưu điểm nổi trội hơn hẳn so với tiết kiệm thông thường mà bạn nên biết.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <div class="head-blog text-center">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <div class="head-blog text-center">
                                        <div class="img-content"> 
                                            <img src="/images/blog/bai2_anh1.jpg" class="img-responsive" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <p>
                                        <span>Cụ thể, gửi tiết kiệm online được xem là an toàn và bảo mật vì:</span> <br />
                                       <i class="fas fa-check"></i>  Được xác nhận gửi tiết kiệm qua email, SMS và danh mục tài khoản trên Internet banking.  <br />
                                       <i class="fas fa-check"></i>  Kiểm soát số dư và lãi trên Internet banking mọi lúc, mọi nơi.  <br />
                                       <i class="fas fa-check"></i>  Rút tiết kiệm, phải có xác nhận bằng mã xác thực (OTP) chỉ riêng khách hàng có. <br />
                                       <i class="fas fa-check"></i>	 Không lo mất sổ <br />
                                       <i class="fas fa-check"></i>	 Không sợ rủi ro khi mang tiền tới quầy giao dịch. <br />


                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <p>Điều bạn cần làm để giữ an toàn tuyệt đối khi giao dịch trực tuyến là bảo quản điện thoại di động, email và các mật khẩu đăng nhập một cách chặt chẽ. Trường hợp thất lạc điện thoại hay bị rò rỉ thông tin..., cần thực hiện thay đổi hoặc báo ngân hàng khẩn cấp.</p>
                               </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <h3>Những lợi ích khi gửi tiết kiệm trực tuyến
                                    </h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>Bên cạnh tính bảo mật đã được nói ở trên, tiết kiệm trực tuyến đang trở thành xu hướng bởi sự dễ dàng và tiện lợi cùng nhiều ưu đãi được các ngân hàng tung ra.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <div class="head-blog text-center">
                                        <div class="img-content"> 
                                            <img src="/images/blog/bai2anh2.jpg" class="img-responsive" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:20px 50px">
                                        <strong> Dễ dàng và tiện lợi</strong> <br />
                                        <i class="fas fa-check"></i>	Mở sổ nhanh chóng, chỉ cần một cú chạm hay một cú nhấp chuột <br />
                                        <i class="fas fa-check"></i>	Gửi tiền mọi lúc mọi nơi <br />
                                        <i class="fas fa-check"></i>	Kỳ hạn gửi linh hoạt, hình thức đa dạng <br />
                                        <i class="fas fa-check"></i>	Tất toán sổ nhanh gọn, không mất thời gian ra ngân hàng <br />
                                        Lựa chọn ngân hàng có lãi suất tiết kiệm online tốt nhất  <br />
                                        Hiện nay, rất nhiều ngân hàng cho phép khách hàng tiết kiệm online. Tuy nhiên, để thu lợi tốt nhất, bạn nên lựa chọn những ngân hàng có lãi suất online hấp dẫn.<br />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p >
                                        Hiện tại, Ngân hàng Phương Đông đang áp dụng mức lãi suất cho tiết kiệm online lên đến 8%, rất cao so với thị trường. Đồng thời, khác hàng gửi tiết kiệm online cũng hưởng mức lãi suất cộng thêm 0.4% so với gửi tại quầy.
                                    </p>  
                                    
                                </div>
                            </div>
                            <div class="box-table">
                                <div class="table-responsive">          
                                  <table class="table">
                                    <thead>
                                      <tr>
                                        <th>NỘI DUNG</th>
                                        <th>ESAVING CÙNG OCB</th>
                                        <th>ESAVING NGÂN HÀNG KHÁC</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>Lãi suất</td>
                                        <td>Đến 8%. Cộng thêm 0.4% so với quầy</td>
                                        <td>6.5% - 7.5%</td>
                                      </tr>
                                       <tr>
                                        <td>Mở sổ</td>
                                        <td>Cùng lúc 4 sổ chỉ 1 lần OTP</td>
                                        <td>Mở từng sổ</td>
                                      </tr>
                                        <tr>
                                        <td>Tất toán</td>
                                        <td>Nhanh chóng, tự động qua OCB OMNI</td>
                                        <td>Tất toán qua ebanking</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12" style="padding:20px 50px">
                                    <strong> Những lợi ích khác dành cho bạn khi tiết kiệm onine cùng OCB OMNI:</strong> <br />
                                   
                                     <i class="fas fa-check"></i>	Chia nhỏ sổ tiết kiệm chỉ với một xác thực OTP, giúp hoạch định tài chính thông minh và linh hoạt <br />
                                     <i class="fas fa-check"></i>	Chủ động mở và quản lý tiền gửi tiết kiệm mọi lúc mọi nơi <br />
                                     <i class="fas fa-check"></i>	Đa dạng kỳ hạn gửi như tuần/tháng/năm <br />
                                     <i class="fas fa-check"></i>	Tất toán nhanh chóng, hoàn toàn tự động qua OCB OMNI <br />
                                     <i class="fas fa-check"></i>	An toàn và bảo mật với tin nhắn + email thông báo khi giao dịch thành công <br />

                                </div>
                            </div>
                           

                        </section>  
                    </div>
                </div>
            </section>
        </div>
                
        <%}
            else if (blogdetailID == 3)
            { %>

              <div class="container">
            
            <div class="head-blog text-center">
                <span class="color-gray-dark text-center">29/07/2018</span>
                <h3>Thông minh lựa chọn đúng ngân hàng - tiết kiệm hàng triệu tiền phí
                </h3>
            </div>
            <section class="blog-detail-new">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="img-new-blog">
                            <img src="/images/blog/bai3anh1.jpg" class="img-responsive" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <section class="content-blog-detail">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <h3>Thông minh lựa chọn đúng ngân hàng - tiết kiệm hàng triệu tiền phí
                                    </h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <p>
                                    Bạn thường xuyên mua sắm online? Bạn thích thanh hóa các hóa đơn điện, nước, internet cùng hàng loạt hóa đơn khác bằng tài khoản trực tuyến vì tiện lợi và dễ dàng? Nhưng bạn đau đầu vì “hàng ngàn” loại phí? Bài viết này sẽ cho bạn một giải pháp. 
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <h3>“Điên đầu” vì phí dịch vụ ngân hàng
                                    </h3>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <p>
                                        Trong thời đại công nghệ số hiện nay, việc giảm sử dụng tiền mặt trong các giao dịch mua bán ngày càng trở nên phổ biến nhờ sự phát triển của nhiều loại hình thẻ ngân hàng, hay các ứng dụng hỗ trợ thanh toán trực tuyến. Bạn dễ dàng để tiền trong một tài khoản ngân hàng và sử dụng cho các giao dịch online như mua sắm, thanh toán hóa đơn, vay, trả nợ, tiết kiệm… Nhưng vấn đề khiến nhiều người “đau đầu” cũng nảy sinh từ đây, khi phải trả quá nhiều tiên cho phí dịch vụ ngân hàng.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <div class="head-blog text-center">
                                        <div class="img-content"> 
                                            <img src="/images/blog/bai3anh1.jpg" class="img-responsive" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <p>
                                       Hiện tại, hầu hết các ngân hàng đều áp mức phí quản lý tài khoản, khoảng 10.000 đến 22.000 đồng/tháng. Cùng với đó là hàng loạt các loại phí như: phí chuyển khoản nội bộ (khoảng 5.000 đồng/lần), phí chuyển tiền ngoài hệ thống (khoảng 11.000 đồng/lần), phí SMS (khoảng 15.000 đồng/tháng), phí eBanking (khoảng 15.000 đồng/tháng).
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <p>Một người ít giao dịch trực tuyến, một tháng cũng mất khoảng vài chục ngàn đồng để trả các loại phí. Số tiền này có thể lên đến vài trăm ngàn, thậm chí cả triệu đồng nếu thường xuyên giao dịch trên mạng. Nếu nhân số tiền phí này với 12 tháng trong một năm, đây sẽ là một con số khá “khủng”. Với những bạn “nghiện” mua sắm online, chắc chắn sẽ phải “điên đầu” nếu tính ra số tiền phí này.</p>
                               </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <h3>“Chọn mặt gửi vàng” cho ngân hàng không phí
                                    </h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <p>Giải pháp tốt nhất cho bạn trong việc hạn chế “mất tiền” oan cho các loại phí chính là lựa chọn một ngân hàng có nhiều ưu đãi cho khách hàng. Nếu bạn chọn được một ngân hàng áp dụng mức phí 0 đồng cho nhiều dịch vụ thì càng tốt. Bạn có thể dùng số tiền tiết kiệm được từ việc trả phí để mua sắm, gửi tiết kiệm hoặc đầu tư để “tiền lại đẻ ra tiền”.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                    <div class="head-blog text-center">
                                        <div class="img-content"> 
                                            <img src="/images/blog/bai3anh2.jpg" class="img-responsive" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:20px 50px">
                                    <p>Thời gian gần đây, Ngân hàng Phương Đông (OCB) đang được nhiều người tìm đến không chỉ bởi dịch vụ tận tâm, mà còn có nhiều ưu đãi hấp dẫn cho khách hàng. Bạn sẽ không tìm thấy ở đâu một ngân hàng cho khách hàng trả phí 0 đồng với tất cả các dịch vụ qua OCB OMNI: Quản lý tài khoản, chuyển khoản nội bộ, chuyển khoản liên ngân hàng, chuyển khoản nhanh 24/7. Bên cạnh đó, khi gửi tiết kiệm online qua kênh OCB OMNI, bạn còn được hưởng mức lãi suất cộng thêm 0.4% so với gửi tại quầy, lãi suất tối đa lên đến 8%.</p>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:20px 50px">
                                    <p>Đồng thời, OCB OMNI được nhiều người lựa chọn khi là ngân hàng hợp kênh đầu tiên tại Việt Nam với 3S nổi trội: Security (an toàn), Simplicity (tiện nghi) và Seamlessly (đồng nhất).Ứng dụng quản lý và giao dịch tài chính online này sẽ dần được thay thế cho eBanking khi có nhiều tính năng và tiện ích nổi trội hơn hẳn, đáp ứng nhu cầu đa dạng ngày càng cao của người dùng.</p>
                                </div>
                            </div>
                            
                          
                        </section>  
                    </div>
                </div>
            </section>
        </div>
        <%} %>
    </section>
</asp:Content>
