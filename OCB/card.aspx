﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="card.aspx.cs" Inherits="OCB.card" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #dtt_43 {
            padding-left: 0px;
        }

        @media screen and (max-width: 768px) {
            #dtt_43 {
                padding-left: 10px;
            }
        }
    </style>

    <script>
        $(function () {
            $(".box-check").parent().closest("div").removeClass("col-xs-12 col-sm-6 col-md-6");
            $(".box-check").parent().closest("div").addClass("col-xs-12 col-sm-12 col-md-12");
            $(".box-check").parent().closest("div").attr("style", "margin-top:20px");
            $("#chk_12").hide();
            $("#slx_82").parent().closest("div").attr("style", "margin-top:24px");
            $(".slx_82").parent().hide();
        })
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <header class="header-page bg-green-dark">
        <div class="container-fluid">
            <div class="row  box-logo-pass-menu">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="row m-row-pass">
                        <div class="col-xs-2 hidden-sm hidden-md hidden-lg">
                            <a href="#">
                                <div class="icon-bar-mobile">
                                    <i class="fas fa-bars"></i>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            <a href="#">
                                <div class="img-logo">
                                    <img src="/images/OCB/logo.png" class="img-responsive" />
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-5 col-md-5 col-lg-">
                            <a href="#">
                                <div class="img-pass">
                                    <img src="/images/OCB/pass.png" class="img-responsive" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-8 col-md-7 col-lg-9 list-menu">
                    <nav>
                        <ul class="list-menu-parent">
                            <li><a href="#">OCB OMMI</a></li>
                            <li><a href="#">Thẻ tín dụng</a></li>
                            <li><a href="#">Vay</a></li>
                            <li><a href="#">Ưu đãi</a></li>
                            <li><a href="#">Mr.Tài chính</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="/my-account">My account</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <section class="box-banner">
        <div class="img-banner">
            <a href="#">
                <img src="/images/OCB/bannercard.jpg" class="img-responsive " data-wow-duration="10s" data-wow-delay="0.1s" />
            </a>
        </div>
    </section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="two-second">
                    <div class="container">
                        <div class="row">
                            <div class="content-two-second text-center " data-wow-delay="0.21s">
                                <h3><strong><span class="color-green-dark">Chỉ 2 phút đăng ký online - Nhận ngay thẻ tại nhà</span></strong></h3>
                                <h4>Có ngay thẻ OCB PASSPORT với 3 bước đơn giản</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="step">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 dl-m-col">
                        <div class="grid-container-step">
                            <div class="item-step-1" data-wow-duration="2s" data-wow-delay="0.01s">
                                <div class="img-step-write">
                                    <picture>
                                                <source media="(min-width: 700px)" srcset="/images/v2/stepwrite.png">
                                            <%--  <source media="(min-width: 100px)" srcset="/images/OCB/icon-write-mobile.png">--%>
                                                <img src="/images/v2/stepwrite.png" class="img-responsive step-img"/>
                                            </picture>


                                </div>
                                <div class="content-step content-step1">
                                    <h3 class="color-green-dark"><strong>2 PHÚT ĐĂNG KÝ</strong></h3>
                                    <p>Điền thông tin online theo mẫu</p>
                                </div>
                            </div>
                            <div class="arrow">
                                <img src="/images/v2/arrow.png" />
                            </div>
                            <div class="item-step-2" data-wow-duration="2s" data-wow-delay="0.02s">
                                <div class="img-step-check">
                                    <picture>
                                                <source media="(min-width: 700px)" srcset="/images/v2/stepcheck.png">
                                                <%-- <source media="(min-width: 100px)" srcset="/images/OCB/ico-check-mobile.png">--%>
                                                <img src="/images/v2/stepcheck.png" class="img-responsive step-img"/>
                                            </picture>


                                </div>
                                <div class="content-step content-step2">
                                    <h3 class="color-green-dark"><strong>PHÊ DUYỆT NHANH</strong></h3>
                                    <p>Trong vòng 24h</p>
                                </div>
                            </div>
                            <div class="arrow">
                                <img src="/images/v2/arrow.png" />
                            </div>
                            <div class="item-step-3 " data-wow-duration="2s" data-wow-delay="0.03s">
                                <div class="img-step-card">
                                    <picture>
                                            <source media="(min-width: 700px)" srcset="/images/v2/stepcard.png">
                                            <%-- <source media="(min-width: 100px)" srcset="/images/OCB/ico-card-mobile.png">--%>
                                            <img src="/images/v2/stepcard.png" class="img-responsive step-img-leter" />
                                        </picture>

                                </div>
                                <div class="content-step">
                                    <h3 class="color-green-dark"><strong>NHẬN THẺ NGAY</strong></h3>
                                    <p>Trao thẻ tận tay trong 5 ngày</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="ocb-passport">
        <div class="title-passport text-center">
            <h3 class="color-green-dark"><span>Đặc quyền OCB Passport</span></h3>
        </div>
        <div class="grid-container-passport">
            <div class="item-passport">
                <div class="img-passport">

                    <picture>
                        <source media="(min-width: 700px)" srcset="/images/OCB/passport1.jpg">
                            <source media="(min-width: 100px)" srcset="/images/OCB/passport_mb1.jpg">
                            <img src="/images/OCB/passport1.jpg" class="img-responsive " data-wow-duration="10s" data-wow-delay="0.1s"  />
                    </picture>
                </div>
                <div class="content-passport">
                    <span class="title-content-passport">
                        <span class="size-lg">Phí 0%</span><br />
                        <span class="size-mall">Chuyển đổi ngoại tệ</span>
                    </span>

                    <p>Thỏa sức mua sắm trên các website thương mại quốc tế, du lịch toàn cầu hoàn toàn không mất phí chuyển đổi ngoại tệ. Tiết kiệm từ 2% - 4% mức phí so với các ngân hàng khác. </p>
                    <div class="more">
                        <a href="#" class="color-yellow" data-toggle="modal" data-target="#myModal">CHI TIẾT</a>
                    </div>
                </div>
            </div>
            <div class="item-passport">
                <div class="img-passport">
                    <picture>
                        <source media="(min-width: 700px)" srcset="/images/OCB/passport2.jpg">
                            <source media="(min-width: 100px)" srcset="/images/OCB/passport_mb2.jpg">
                            <img src="/images/OCB/passport2.jpg" class="img-responsive " data-wow-duration="10s" data-wow-delay="0.1s"  />
                    </picture>
                </div>
                <div class="content-passport">
                    <span class="title-content-passport">
                        <span class="size-lg">5 tỷ đồng</span><br />
                        <span class="size-mall text-right">Bảo hiểm du lịch toàn cầu</span>
                    </span>

                    <p>An tâm tận hưởng chuyến đi khi được tặng kèm bảo hiểm du lịch của MSIG với số tiền được bảo hiểm lên đến 5 tỷ đồng gồm bảo hiểm thất lạc hành lý, trễ chuyến bay…</p>
                    <div class="more">
                        <a href="#" class="color-yellow text-right" data-toggle="modal" data-target="#myModal2">CHI TIẾT</a>
                    </div>
                </div>
            </div>
            <div class="item-passport">
                <div class="img-passport">
                    <picture>
                        <source media="(min-width: 700px)" srcset="/images/OCB/passport3.jpg">
                            <source media="(min-width: 100px)" srcset="/images/OCB/passport3_mb.jpg">
                            <img src="/images/OCB/passport3.jpg" class="img-responsive " data-wow-duration="10s" data-wow-delay="0.1s"  />
                    </picture>
                </div>
                <div class="content-passport">
                    <span class="title-content-passport">
                        <span class="size-lg">Hàng triệu</span><br />
                        <span class="size-mall">Dịch vụ tiện ích đẳng cấp</span>
                    </span>

                    <p>Trải nghiệm cuộc sống đẳng cấp của một cư dân toàn cầu với ưu đãi phí tại hơn 40 sân golf quốc tế, nhà hàng, khu mua sắm sang trọng, ... cho chuyến đi của bạn thêm ý  nghĩa</p>
                    <div class="more">
                        <a href="#" class="color-yellow text-right" data-toggle="modal" data-target="#myModal">CHI TIẾT</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="box-event">
        <div class="title-passport title-event text-center">
            <h3 class="color-green-dark"><span>Tiện ích và điều kiện mở thẻ</span></h3>
        </div>
        <div id="outer-wrap">
            <div class="container">
                <!-- Navigation -->
                <nav class="mainNav">
                    <ul>
                        <li class="selected"><a><strong>Tiện ích nổi bật</strong></a>
                            <ul>
                                <li><a href="#">0% phí chuyển đổi ngoại tệ khi chủ thẻ thanh toán tại nước ngoài</a></li>
                                <li><a href="#">Tặng bảo hiểm du lịch của MSIG với số tiền được bảo hiểm lên đến 5 tỷ đồng</a></li>
                                <li><a href="#">Rút tiền mặt đến 80% hạn mức thẻ tín dụng.</a></li>
                                <li><a href="#">Chi tiêu trước, thanh toán sau, miễn lãi lên đến 55 ngày.</a></li>
                                <li><a href="#">Quản lý thẻ (kích hoạt, đóng thẻ) trích tiền tự động hoặc thanh toán Thẻ tín dụng qua OCB OMNI.</a></li>

                            </ul>
                        </li>
                        <li><a>Ưu đãi thẻ</a>
                            <ul>
                                <li><a href="#">Khách hàng được ưu đãi phí hơn 40 sân Goft nổi tiếng tại Việt Nam và thế giớ.</a></li>
                                <li><a href="#">Trên 1000 điểm ưu đãi mua sắm, nhà hàng, khách sạn quốc tế.</a></li>
                                <li><a href="#">Hỗ trợ chủ thẻ toàn cầu 24/7 của Mastercard và OCB </a></li>
                                <li><a href="#">Hơn 1 triệu ATM/POS rút tiền mặt trên toàn thế giới</a></li>
                                <li><a href="#">Hơn 4.500 điểm ưu đãi trả góp lãi suất 0% tại các đại lý liên kết với OCB trên toàn quốc</a></li>

                            </ul>
                        </li>
                        <li><a>Điều kiện mở thẻ</a>
                            <ul>
                                <li><a href="#">CMND, Hộ khẩu, chứng minh nguồn thu nhập ổn định.</a></li>

                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <section class="box-why">
        <div class="title-why text-center">
            <h3 class="color-green-dark"><span>Lý do chọn OCB PASSPORT</span></h3>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="table  table-hover" id="table-why">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">
                                    <div class="img-title-passport">
                                        <center><img src="/images/v2/title_passport.png" alt="title-passport" class="img-responsive" style="max-width:110px" /></center>
                                    </div>
                                </th>
                                <th scope="col">
                                    <center><strong>NGÂN HÀNG KHÁC</strong></center>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="width-td">Phí phát hành thẻ</td>
                                <td>0 đ</td>
                                <td>200.000 đ -835.000 đ</td>

                            </tr>
                            <tr>
                                <td>Phí thường niên</td>
                                <td>0đ
                                   
                                </td>
                                <td>400.000 đ -800.000 đ/năm
                              
                                  

                                </td>
                            </tr>
                            <tr>
                                <td>Phí chuyển đổi ngoại tệ</td>
                                <td>0%</td>
                                <td>2.5 - 4%
                             
                                </td>
                            </tr>
                            <tr>
                                <td>Bảo hiểm du lịch</td>
                                <td>đến 5 tỷ đồng</td>
                                <td>Không có
                                </td>
                            </tr>
                            <tr>
                                <td>Thời gian miễn lãi</td>
                                <td>55 ngày</td>
                                <td>45-55 ngày
                                </td>
                            </tr>
                            <tr>
                                <td>Hạn mức rút tiền mặt</td>
                                <td>80%</td>
                                <td>50% -70%
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="box-form" data-wow-duration="2s" data-wow-delay="0.01s" id="box-form">
        <div class="container">
            <div class="row item-form">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="title-form text-center">
                        <h3 class="color-green-dark">Mở thẻ ngay chỉ 2 phút điền thông tin</h3>
                        <h4 class="link-register"><a href="/my-account" class="color-orange">Bạn đã từng đăng ký?</a></h4>
                    </div>
                </div>
            </div>
            <div class="row dl-margin-form">
                <div class="col-xs-12 col-sm-12 col-md-12 box-number-form  box-number-form-cs ">
                    <div class="box-number">
                        <span class="number-form" id="number1">1</span>
                        <p class="hidden-xs">Thông tin liên hệ</p>
                    </div>

                    <div class="box-number">
                        <span class="number-form" id="number2">2</span>
                        <p class="hidden-xs">Hoàn thành đơn đăng ký</p>
                    </div>

                    <div class="box-number">
                        <span class="number-form" id="number3">3</span>
                        <p class="hidden-xs">Xác nhận thông tin</p>

                    </div>

                    <div class="box-number">
                        <span class="number-form" id="number4">4</span>
                        <p class="hidden-xs">Tải hồ sơ</p>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 box-question">
                    <div class="question<%=step %> question" data-tab="number1">
                        <input id="ipstep" value="<%=step %>" hidden="hidden" />
                        <div class="row item-form">
                            <%for (int j = 0; j < listSurveyStep.Count(); j++)
                                { %>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <%if (listSurveyStep[j].Type == "T")
                                    {
                                %>
                                <div class="input-text">
                                    <span class="input input-jiro">

                                        <%
                                            if (listSurveyStep[j].ID == 6)
                                            {%>
                                        <input type="text" placeholder="<%=listSurveyStep[j].Code%>" id="dtt_<%=listSurveyStep[j].ID%>" class="input-field input-field-jiro email_<%=listSurveyStep[j].Required %>  survey require_<%=listSurveyStep[j].Required %>  type_<%=listSurveyStep[j].Number %>" data-error="<%=listSurveyStep[j].Code %>" data-email="Email không đúng định dạng" />
                                        <%}
                                            else if (listSurveyStep[j].ID == 5 || listSurveyStep[j].ID == 80)
                                            { %>
                                        <input type="text" placeholder="<%=listSurveyStep[j].Code%>" id="dtt_<%=listSurveyStep[j].ID%>" class="input-field input-field-jiro phone_<%=listSurveyStep[j].Required %>  survey require_<%=listSurveyStep[j].Required %>" data-error="<%=listSurveyStep[j].Code %>" data-phone="Số điện thoại không đúng định dạng" />
                                        <%}
                                            else if (listSurveyStep[j].ID == 14 || listSurveyStep[j].ID == 94 || listSurveyStep[j].ID == 73 || listSurveyStep[j].ID == 11)
                                            {%>


                                        <input placeholder="<%=listSurveyStep[j].Code%>" class="input-field input-field-jiro input-field input-field-jiro currency survey require_<%=listSurveyStep[j].Required %>" id="dtt_<%=listSurveyStep[j].ID%>" data-error="<%=listSurveyStep[j].Code %>" />

                                        <%}
                                            else
                                            { %>
                                        <input type="text" placeholder="<%=listSurveyStep[j].Code%>" id="dtt_<%=listSurveyStep[j].ID%>" class="input-field input-field-jiro survey require_<%=listSurveyStep[j].Required %>" data-error="<%=listSurveyStep[j].Code %>" />
                                        <%} %>
                                        <label class="input-label input-label-jiro" for="name">

                                            <span class="input-label-content input-label-content-jiro"><%=listSurveyStep[j].Code%>

                                                <%if (listSurveyStep[j].Required == true)
                                                    { %>(*) <%} %></span>
                                        </label>
                                    </span>
                                </div>
                                <%}
                                    else if (listSurveyStep[j].Type == "S")
                                    {
                                        listOption = SIV.GetListOption(listSurveyStep[j].ID);
                                %>
                                <%--   <label for="slx_<%=listSurveyStep[j].ID%>" id="lb_<%=listSurveyStep[j].ID%>">
                                    <%=listSurveyStep[j].Code%>

                                    <%if (listSurveyStep[j].Required == true)
                                        { %>
                                    <span class="color-red">(*)</span>
                                    <%} %>
                                </label>--%>
                                <input type="text" value="<%=listSurveyStep[j].Code%>" class=" sl-item survey slx_<%=listSurveyStep[j].ID%>  require_<%=listSurveyStep[j].Required %>" onfocus="OpenSelect(this)" onblur="CloseSelect(this)" readonly data-error="<%=listSurveyStep[j].Code %>" id=" " />
                                <%--                                <select class="sl-item survey require_<%=listSurveyStep[j].Required %>" id="slx_<%=listSurveyStep[j].ID%>" data-error="<%=listSurveyStep[j].Code %>">--%>
                                <%--<option value=""><%=listSurveyStep[j].Code%></option>--%>

                                <div class="select-item" id="slx_<%=listSurveyStep[j].ID%>">
                                    <ul>
                                        <li data-id="" onclick="Select(this)"><%=listSurveyStep[j].Code%></li>
                                        <% if (listSurveyStep[j].ID == 19 || listSurveyStep[j].ID == 7 || listSurveyStep[j].ID == 52 || listSurveyStep[j].ID == 58 || listSurveyStep[j].ID == 63)
                                            {
                                                for (int l = 0; l < listProvince.Count; l++)
                                                {
                                        %>
                                        <%-- <option value="<%=listProvince[l].ID%>"><%=listProvince[l].Name%></option>--%>
                                        <li data-id="<%=listProvince[l].ID%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=listProvince[l].Name%></li>
                                        <%}
                                            }
                                            else if (listSurveyStep[j].ID == 48 || listSurveyStep[j].ID == 53 || listSurveyStep[j].ID == 51 || listSurveyStep[j].ID == 17)
                                            {
                                                for (int date = 1; date <= 31; date++)
                                                {
                                        %>
                                        <li data-id="<%=date%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=date%></li>
                                        <%-- <option value="<%=date%>"><%=date%></option>--%>
                                        <%
                                                }
                                            }
                                            else if (listSurveyStep[j].ID == 49 || listSurveyStep[j].ID == 54)
                                            {
                                                for (int month = 1; month <= 12; month++)
                                                {
                                        %>
                                        <li data-id="<%=month%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=month%></li>
                                        <%--   <option value="<%=month%>"><%=month%></option>--%>
                                        <%
                                                }
                                            }
                                            else if (listSurveyStep[j].ID == 50 || listSurveyStep[j].ID == 55)
                                            {
                                                for (int year = 1945; year <= 2050; year++)
                                                {
                                        %>


                                        <%
                                                }
                                            }
                                            else if (listSurveyStep[j].ID == 16)
                                            {
                                                for (int year = 1945; year <= 2001; year++)
                                                {
                                        %>
                                        <li data-id="<%=year%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=year%></li>
                                        <%-- <option value="<%=year%>"><%=year%></option>--%>
                                        <%}
                                            }
                                            else
                                            {
                                                for (int k = 0; k < listOption.Count; k++)
                                                {%>
                                        <li data-id="<%=listOption[k].ID%>" onclick="Select(this,<%=listSurveyStep[j].ID%>)"><%=listOption[k].Value%></li>
                                        <%-- <option value="<%=listOption[k].Value%>"><%=listOption[k].Value%></option>--%>
                                        <%
                                                }
                                            }

                                        %>
                                    </ul>
                                </div>
                                <%}
                                    else if (listSurveyStep[j].Type == "C")
                                    {%>
                                <div class="box-check">
                                    <label style="margin-top: 20px; color: #0e8c45">
                                        <%=listSurveyStep[j].Code%>
                                        <span class="color-red">

                                            <%if (listSurveyStep[j].Required == true)
                                                { %>(*) <%} %></span>
                                    </label>
                                    <div class="input-check">
                                        <%--<div class="outer-but">
                                                <a id="chk_<%=listSurveyStep[j].ID%>" class="orange confirm-address" href="javascript:void(0);" data-name="consult"><span>Có</span></a> <input type="radio" name="name" id="rdo_<%=listSurveyStep[j].ID%>" value="Yes" disabled="disabled" />
                                                <a id="chkn_<%=listSurveyStep[j].ID%>" class="green confirm-address" href="javascript:void(0);" data-name="consult"><span>Không</span></a><input type="radio" name="name" id="rdon_<%=listSurveyStep[j].ID%>" value="No" disabled="disabled" />
                                            </div>--%>
                                        <div class="input-checkbox col-50-left">
                                            <input type="checkbox" class="nhucaukhachhang survey" id="chk_<%=listSurveyStep[j].ID%>" name="" value="YES"><span class="checkmark" id="span_<%=listSurveyStep[j].ID%>"></span><label for=" nhucau-2"></label>
                                        </div>
                                    </div>
                                </div>

                                <%} %>
                            </div>
                            <%} %>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 footer-form">
                                <div class="box-group">

                                    <p class="hidden-xs"></p>
                                </div>
                                <div class="box-button">
                                    <% if (step > 1)
                                        {
                                            if (step == 2)
                                            {%>
                                    <a href="step1" class="btn btn-default btn-update">QUAY LẠI</a>
                                    <%}
                                        else if (step == 3)
                                        {%>
                                    <a href="step2a" class="btn btn-default btn-update">QUAY LẠI</a>
                                    <%}
                                        else if (step == 4)
                                        {%>
                                    <a href="step2b" class="btn btn-default btn-update">QUAY LẠI</a>
                                    <%}
                                        else
                                        {%>
                                    <a href="step<%=step - 1 %>" class="btn btn-default btn-update">QUAY LẠI</a>
                                    <%}%>
                                    <%
                                        }
                                        if (step <= 4)
                                        {%>
                                    <a class="btn btn-success btn-open" onclick="Open()">TIẾP TỤC ></a>
                                    <%}%>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>

    <section class="box-partner">
        <div class="container">
            <div class="title-parner text-center">
                <h3 class="color-green-dark"><span>Đối tác</span></h3>
            </div>
            <div class="row">
                <section class="logo hidden-sm hidden-md hidden-lg" style="width: 100%">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-new-mobile">
                                <div class="sliderlogo" style="width: 100%">
                                    <section class="autoplay logoPatner">
                                        <div class=" box-item-logo">
                                            <div class="img-new img-logo-cs">
                                                <img src="/images/v2/logo1.png" class="img-responsive" />

                                            </div>
                                        </div>
                                        <div class=" box-item-logo">
                                            <div class="img-new img-logo-cs">
                                                <img src="/images/v2/logo1.png" class="img-responsive" />

                                            </div>
                                        </div>
                                        <div class=" box-item-logo">
                                            <div class="img-new img-logo-cs">
                                                <img src="/images/v2/logo6.png" class="img-responsive" />

                                            </div>
                                        </div>
                                        <div class=" box-item-logo">
                                            <div class="img-new img-logo-cs">
                                                <img src="/images/v2/logo5.png" class="img-responsive" />

                                            </div>
                                        </div>
                                        <div class=" box-item-logo">
                                            <div class="img-new img-logo-cs">
                                                <img src="/images/v2/logo2.png" class="img-responsive" />

                                            </div>
                                        </div>
                                        <div class=" box-item-logo">
                                            <div class="img-new img-logo-cs">
                                                <img src="/images/v2/logo3.png" class="img-responsive" />

                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
                <section class="box-partner hidden-xs">

                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <section class="grid-container-partner">
                                    <div class="item-partner">
                                        <div class="img-partner">
                                            <img src="/images/v2/logo3.png" class="img-responsive" />
                                        </div>
                                    </div>
                                    <div class="item-partner">
                                        <div class="img-partner">
                                            <img src="/images/v2/logo2.png" class="img-responsive" />
                                        </div>
                                    </div>
                                    <div class="item-partner">
                                        <div class="img-partner">
                                            <img src="/images/v2/logo5.png" class="img-responsive" />
                                        </div>
                                    </div>
                                    <div class="item-partner">
                                        <div class="img-partner">
                                            <img src="/images/v2/logo1.png" class="img-responsive" />
                                        </div>
                                    </div>
                                    <div class="item-partner">
                                        <div class="img-partner">
                                            <img src="/images/v2/logo4.png" class="img-responsive" />
                                        </div>
                                    </div>
                                    <div class="item-partner">
                                        <div class="img-partner">
                                            <img src="/images/v2/logo6.png" class="img-responsive" />
                                        </div>
                                    </div>
                                    <div class="item-partner">
                                        <div class="img-partner">
                                            <img src="/images/v2/logo1.png" class="img-responsive" />
                                        </div>
                                    </div>
                                    <div class="item-partner">
                                        <div class="img-partner">
                                            <img src="/images/v2/logo4.png" class="img-responsive" />
                                        </div>
                                    </div>
                                    <div class="item-partner">
                                        <div class="img-partner">
                                            <img src="/images/v2/logo3.png" class="img-responsive" />
                                        </div>
                                    </div>

                                </section>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    </section>

    <section class="new">
        <div class="container">
            <div class="row ">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="title-endow text-center" data-wow-duration="4s" data-wow-delay="0.3s">
                        <h3 class="color-green-dark">Chương trình ưu đãi khác</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-new-mobile">
                    <div class="slider-new">
                        <section class="regular slider">
                            <div class="box-item">
                                <div class="img-new">
                                    <img src="/images/OCB/new1.jpg" class="img-responsive" />
                                    <div class="content-new text-center">
                                        <p>Dành cho khách hàng gửi tiết kiệm online trên OCB OMNI </p>
                                    </div>
                                    <div class="link-read-more text-center">
                                        <a href="#" class="color-orange">Xem ngay</a>
                                    </div>
                                </div>
                            </div>
                            <div class="box-item">
                                <div class="img-new">
                                    <img src="/images/OCB/new2.jpg" class="img-responsive" />
                                    <div class="content-new text-center">
                                        <p>Dành cho khách hàng gửi tiết kiệm online trên OCB OMNI </p>
                                    </div>
                                    <div class="link-read-more text-center">
                                        <a href="#" class="color-orange">Xem ngay</a>
                                    </div>
                                </div>
                            </div>
                            <div class="box-item">
                                <div class="img-new">
                                    <img src="/images/OCB/tin-1.jpg" class="img-responsive" />
                                    <div class="content-new text-center">
                                        <p>Dành cho khách hàng gửi tiết kiệm online trên OCB OMNI </p>
                                    </div>
                                    <div class="link-read-more text-center">
                                        <a href="#" class="color-orange">Xem ngay</a>
                                    </div>
                                </div>
                            </div>
                            <div class="box-item">
                                <div class="img-new">
                                    <img src="/images/v2/new1.png" class="img-responsive" />
                                    <div class="content-new text-center">
                                        <p>Dành cho khách hàng gửi tiết kiệm online trên OCB OMNI</p>
                                    </div>
                                    <div class="link-read-more text-center">
                                        <a href="#" class="color-orange">Xem ngay</a>
                                    </div>
                                </div>
                            </div>
                            <div class="box-item">
                                <div class="img-new">
                                    <img src="/images/v2/new2.png" class="img-responsive" />
                                    <div class="content-new text-center">
                                        <p>Dành cho khách hàng gửi tiết kiệm online trên OCB OMNI</p>
                                    </div>
                                    <div class="link-read-more text-center">
                                        <a href="#" class="color-orange">Xem ngay</a>
                                    </div>
                                </div>
                            </div>
                            <div class="box-item">
                                <div class="img-new">
                                    <img src="/images/OCB/tin-1.jpg" class="img-responsive" />
                                    <div class="content-new text-center">
                                        <p>Dành cho khách hàng gửi tiết kiệm online trên OCB OMNI</p>
                                    </div>
                                    <div class="link-read-more text-center">
                                        <a href="#" class="color-orange">Xem ngay</a>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--Nội dung 3-->
    <div class="modal fade" id="myModal3" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    <h4 class="modal-title">Hàng triệu -  Dịch vụ tiện ích đẳng cấp</h4>
                </div>
                <div class="modal-body">
                    <p>Trải nghiệm cuộc sống đẳng cấp của một cư dân toàn cầu với ưu đãi phí tại hơn 40 sân golf quốc tế, nhà hàng, khu mua sắm sang trọng, ... cho chuyến đi của bạn thêm ý  nghĩa</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>

    <!--Nội dung 2-->
    <div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    <h4 class="modal-title">5 tỷ đồng - Bảo hiểm du lịch toàn cầu</h4>
                </div>
                <div class="modal-body">
                    <p>An tâm tận hưởng chuyến đi khi được tặng kèm bảo hiểm du lịch của MSIG với số tiền được bảo hiểm lên đến 5 tỷ đồng gồm bảo hiểm thất lạc hành lý, trễ chuyến bay…</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>

    <!--Nội dung 1-->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    <h4 class="modal-title">Phí 0đ - Chuyển đổi ngoại tệ</h4>
                </div>
                <div class="modal-body">
                    <p>Thỏa sức mua sắm trên các website thương mại quốc tế, du lịch toàn cầu hoàn toàn không mất phí chuyển đổi ngoại tệ. Tiết kiệm từ 2% - 4% mức phí so với các ngân hàng khác. </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {

            $("#dtt_14").parent().hide();
            $("#slx_82").parent().hide();
            $("#dtt_14").removeClass("require_True");
            $("#slx_82").removeClass("require_True");

            $(".input-checkbox").click(function () {
                if ($("#chk_12").is(":checked")) {
                    $("#chk_12").prop("checked", false);
                    $("#dtt_14").removeClass("require_True");
                    $("#slx_82").removeClass("require_True");
                    $("#dtt_14").parent().hide();
                    $("#slx_82").parent().hide();
                }
                else {
                    $("#chk_12").prop("checked", true);
                    $('#slx_82').parent("").show();
                    $('#dtt_14').parent().show();
                    $("#dtt_14").addClass("require_True");
                    $("#slx_82").addClass("require_True");

                }
            });
        });


        function OpenSelect(input) {
            $(input).parent().find(".select-item").show();
        }

        function CloseSelect(input) {
            setTimeout(() => {
                $(input).parent().find(".select-item").hide();
            }, 150);
        }
        function CheckAvailableEmail(email) {
            $.post("/do/register/CheckEmail", {
                email: email,
            }, function (data) {
                data = JSON.parse(data);
                if (data.success == -1) {
                    console.log(data.error);
                } else if (data.success == 0) {
                    $("#dtt_6").css("border", "unset");
                } else if (data.success == 1) {
                    alertify.alert("Vui lòng nhập lại", "<li>Email đã được đăng ký</li>");
                    $("#dtt_6").css("border", "1px solid red");
                }
            });
        }

        function CheckAvailablePhone(phone) {
            $.post("/do/register/CheckPhone", {
                email: email,
            }, function (data) {
                if (data == 2) {
                    HideLoading();
                    alertify.alert("Vui lòng nhập lại", "<li>Số điện thoại hoặc email đã được đăng ký</li>");
                }
            });
        }
        function Open() {
            ShowLoading();
            AddressInitLoad();
            var step = $("#ipstep").val();
            var length = $(".survey").length;
            var value = "";
            var lasterr = 0;

            ValidationCardStep(cardid, cardstep);
            if (step == "step1") {
                var dateBirthday = $("#slx_17 .selected").data("id");
                var monthBirtday = $("#slx_49 .selected").data("id");
                var yearBirtday = $("#slx_16 .selected").data("id");
                var strDay1 = monthBirtday + "/" + dateBirthday + "/" + yearBirtday;
                var d = new Date();
                var yearNow = d.getMonth() + "/" + d.getDate() + "/" + d.getFullYear();
                localStorage.setItem("birdthday", strDay1);

                if (HowManyYear(strDay1, d) < 18) {
                    errRegister += "<li> Khách hàng phải trên 18 tuổi</li>";
                    lasterr = i;
                }
                var email = localStorage.getItem("email");
                var phoneuser = localStorage.getItem("phoneuser");
                $.post("/do/register/CheckUser", { email: email, phoneuser: phoneuser }, function (data) {
                    if (data == 2) {
                        HideLoading();
                        alertify.alert("Vui lòng nhập lại", "<li>Số điện thoại hoặc email đã được đăng ký</li>");
                        lasterr = i;
                    }
                    else {
                        CallFunctionRegister();
                    }
                });

            }
        }


    </script>
</asp:Content>

